package br.com.seinfra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_par", schema = "bdscj")
public class Parecer extends GenericBaseModel<Long>{
	
	@Column(name = "prc_mft_jur")
	private Boolean manifestacaoJuridica;

	@JsonBackReference("pareceres")
	@ManyToOne
	@JoinColumn(name = "doc_id")
	private DocumentoJuridico documento;
	
	public Parecer() {
		super();
	}

	public Parecer(DocumentoJuridico documento) {
		this.documento = documento;
	}

	public Boolean getManifestacaoJuridica() {
		return manifestacaoJuridica;
	}

	public void setManifestacaoJuridica(Boolean manifestacaoJuridica) {
		this.manifestacaoJuridica = manifestacaoJuridica;
	}

	public DocumentoJuridico getDocumento() {
		return documento;
	}

	public void setDocumento(DocumentoJuridico documento) {
		this.documento = documento;
	}
	
}
