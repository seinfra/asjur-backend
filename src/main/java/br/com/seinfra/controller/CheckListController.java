package br.com.seinfra.controller;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.CheckList;
import br.com.seinfra.model.CheckListDocumento;
import br.com.seinfra.model.CheckListSub;
import br.com.seinfra.model.CheckListTipo;
import br.com.seinfra.model.filter.CheckListFilter;
import br.com.seinfra.service.CheckListService;

@RestController
@RequestMapping("/asjur-api/checklist")
public class CheckListController extends GenericBaseController<CheckListService, CheckList, Long> {
	
	@GetMapping	
	public Page<CheckList> pequisar(CheckListFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
	@GetMapping("/buscarPorDescricao")
	public CheckList buscarPorDescricao(String descricao){
		return servico.buscarPorDescricao(descricao);
	}
	
	@GetMapping("/buscarTipo/{id}")
	public List<CheckListTipo> buscarPorCheckListTipo(@PathVariable Long id){
		return servico.checkListTipo(id);
	}
	
	@GetMapping("/buscarSub/{id}")
	public List<CheckListSub> buscarPorCheckListSub(@PathVariable Long id){
		return servico.checkListSub(id);
	}
	
	@GetMapping("/buscarSubSelecionado/{id}")
	public CheckListSub checkListSubSelecionado(@PathVariable Long id){
		return servico.checkListSubSelecionado(id);
	}
	
	@GetMapping("/buscarDocumentacao")
	public List<CheckListDocumento> buscarPorCheckListDocs(@RequestParam String id,@RequestParam String documento){
		return servico.checkListDocs(id, documento);
	}
	
}