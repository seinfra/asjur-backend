package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.EmitenteDestinatario;
import br.com.seinfra.model.filter.EmitenteDestinatarioFilter;

public interface EmitenteDestinatarioRepositoryQuery {

	public Page<EmitenteDestinatario> pequisar(EmitenteDestinatarioFilter filtro, Pageable pageable);
	
	public EmitenteDestinatario findByNome(String nome);
	
}
