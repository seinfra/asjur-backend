package br.com.seinfra.repository.query;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.CheckList;
import br.com.seinfra.model.CheckListDocumento;
import br.com.seinfra.model.CheckListSub;
import br.com.seinfra.model.CheckListTipo;
import br.com.seinfra.model.filter.CheckListFilter;

public interface CheckListRepositoryQuery {

	public Page<CheckList> pequisar(CheckListFilter filtro, Pageable pageable);
	
	public List<CheckListTipo> checkListTipo(Long id);
	
	public List<CheckListSub> checkListSub(Long id);
	
	public CheckListSub checkListSubSelecionado(Long id);
	
	public List<CheckListDocumento> checkListDocs(Long id, Long documento);
	
}
