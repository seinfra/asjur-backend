-------------TABELA PERMISSÃO
-------PADRÃO

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (1, current_date, 'Menu - cadastrar', 'CADASTRO_MENU');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (2, current_date, 'Segurança - menu', 'SEGURANCA_MENU');



---------PERFIL
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (7, current_date, 'Perfil - consultar', 'PERFIL_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (8, current_date, 'Perfil - cadastrar', 'PERFIL_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (9, current_date, 'Perfil - alterar', 'PERFIL_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (10, current_date, 'Perfil - excluir', 'PERFIL_EXCLUIR');

---------PERMISSAO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (11, current_date, 'Permissão - consultar', 'PERMISSAO_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (12, current_date, 'Permissão - casdastrar', 'PERMISSAO_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (13, current_date, 'Permissão - alterar', 'PERMISSAO_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (14, current_date, 'Permissão - excluir', 'PERMISSAO_EXCLUIR');

---------USUÁRIO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (15, current_date, 'Usuário - consultar', 'USUARIO_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (16, current_date, 'Usuário - cadastrar', 'USUARIO_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (17, current_date, 'Usuário - alterar', 'USUARIO_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (18, current_date, 'Usuário - excluir', 'USUARIO_EXCLUIR');

---------CIDADE
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (19, current_date, 'Cidade - consultar', 'CIDADE_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (20, current_date, 'Cidade - cadastrar', 'CIDADE_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (21, current_date, 'Cidade - alterar', 'CIDADE_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (22, current_date, 'Cidade - excluir', 'CIDADE_EXCLUIR');

---------ENTIDADE
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (23, current_date, 'Entidade - consultar', 'ENTIDADE_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (24, current_date, 'Entidade - cadastrar', 'ENTIDADE_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (25, current_date, 'Entidade - alterar', 'ENTIDADE_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (26, current_date, 'Entidade - excluir', 'ENTIDADE_EXCLUIR');

---------ESTADO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (27, current_date, 'Estado - consultar', 'ESTADO_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (28, current_date, 'Estado - cadastrar', 'ESTADO_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (29, current_date, 'Estado - alterar', 'ESTADO_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (30, current_date, 'Estado - excluir', 'ESTADO_EXCLUIR');

---------REGIÃO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (31, current_date, 'Região - consultar', 'REGIAO_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (32, current_date, 'Região - cadastrar', 'REGIAO_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (33, current_date, 'Região - alterar', 'REGIAO_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (34, current_date, 'Região - excluir', 'REGIAO_EXCLUIR');

---------UNIDADE ADMINISTRATIVA
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (35, current_date, 'Unidade administrativa - consultar', 'UNIDADE_ADMINISTRATIVA_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (36, current_date, 'Unidade administrativa - cadastrar', 'UNIDADE_ADMINISTRATIVA_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (37, current_date, 'Unidade administrativa - alterar', 'UNIDADE_ADMINISTRATIVA_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (38, current_date, 'Unidade administrativa - excluir', 'UNIDADE_ADMINISTRATIVA_EXCLUIR');

---------PAÍS
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (39, current_date, 'País - consultar', 'PAIS_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (40, current_date, 'País - cadastrar', 'PAIS_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (41, current_date, 'País - alterar', 'PAIS_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (42, current_date, 'País - excluir', 'PAIS_EXCLUIR');


--------DASHBOARD
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (43, current_date, 'Paínel - menu', 'PAINEL_MENU');


---------TIPO DOCUMENTO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (44, current_date, 'Tipo documento - consultar', 'TIPO_DOCUMENTO_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (45, current_date, 'Tipo documento - cadastrar', 'TIPO_DOCUMENTO_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (46, current_date, 'Tipo documento - alterar', 'TIPO_DOCUMENTO_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (47, current_date, 'Tipo documento - excluir', 'TIPO_DOCUMENTO_EXCLUIR');


---------ASSUNTO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (48, current_date, 'Assunto - consultar', 'ASSUNTO_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (49, current_date, 'Assunto - cadastrar', 'ASSUNTO_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (50, current_date, 'Assunto - alterar', 'ASSUNTO_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (51, current_date, 'Assunto - excluir', 'ASSUNTO_EXCLUIR');

---------REFERENCIA
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (52, current_date, 'Referência - consultar', 'REFERENCIA_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (53, current_date, 'Referência - cadastrar', 'REFERENCIA_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (54, current_date, 'Referência - alterar', 'REFERENCIA_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (55, current_date, 'Referência - excluir', 'REFERENCIA_EXCLUIR');


---------CAIXA
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (56, current_date, 'Caixa - consultar', 'CAIXA_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (57, current_date, 'Caixa - cadastrar', 'CAIXA_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (58, current_date, 'Caixa - alterar', 'CAIXA_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (59, current_date, 'Caixa - excluir', 'CAIXA_EXCLUIR');


---------ARMARIO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (60, current_date, 'Armário - consultar', 'ARMARIO_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (61, current_date, 'Armário - cadastrar', 'ARMARIO_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (62, current_date, 'Armário - alterar', 'ARMARIO_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (63, current_date, 'Armário - excluir', 'ARMARIO_EXCLUIR');


---------PASTA
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (64, current_date, 'Pasta - consultar', 'PASTA_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (65, current_date, 'Pasta - cadastrar', 'PASTA_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (66, current_date, 'Pasta - alterar', 'PASTA_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (67, current_date, 'Pasta - excluir', 'PASTA_EXCLUIR');


---------DOCUMENTO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (68, current_date, 'Documento - consultar', 'DOCUMENTO_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (69, current_date, 'Documento - cadastrar', 'DOCUMENTO_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (70, current_date, 'Documento - alterar', 'DOCUMENTO_ALTERAR');


---------EMITENTE DESTINATÁRIO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (72, current_date, 'Emitente Destinatário - consultar', 'EMITENTE_DESTINATARIO_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (73, current_date, 'Emitente Destinatário - cadastrar', 'EMITENTE_DESTINATARIO_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (74, current_date, 'Emitente Destinatário - alterar', 'EMITENTE_DESTINATARIO_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (75, current_date, 'Emitente Destinatário - excluir', 'EMITENTE_DESTINATARIO_EXCLUIR');

---------FUNCIONÁRIO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (76, current_date, 'Funcionário - consultar', 'FUNCIONARIO_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (77, current_date, 'Funcionário - cadastrar', 'FUNCIONARIO_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (78, current_date, 'Funcionário - alterar', 'FUNCIONARIO_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (79, current_date, 'Funcionário - excluir', 'FUNCIONARIO_EXCLUIR');

---------CARGO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (80, current_date, 'Cargo - consultar', 'CARGO_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (81, current_date, 'Cargo - cadastrar', 'CARGO_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (82, current_date, 'Cargo - alterar', 'CARGO_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (83, current_date, 'Cargo - excluir', 'CARGO_EXCLUIR');


---------ORGÃO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (84, current_date, 'Orgão - consultar', 'ORGAO_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (85, current_date, 'Orgão - cadastrar', 'ORGAO_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (86, current_date, 'Orgão - alterar', 'ORGAO_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (87, current_date, 'Orgão - excluir', 'ORGAO_EXCLUIR');


---------CHECKLIST
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (92, current_date, 'Checklist - consultar', 'CHECKLIST_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (93, current_date, 'Checklist - cadastrar', 'CHECKLIST_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (94, current_date, 'Checklist - alterar', 'CHECKLIST_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (95, current_date, 'Checklist - excluir', 'CHECKLIST_EXCLUIR');

---------DOCUMENTO-JURIDICO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (96, current_date, 'Documento jurídico - consultar', 'DOCUMENTO_JURIDICO_CONSULTAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (97, current_date, 'Documento jurídico - cadastrar', 'DOCUMENTO_JURIDICO_CADASTRAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (98, current_date, 'Documento jurídico - alterar', 'DOCUMENTO_JURIDICO_ALTERAR');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (99, current_date, 'Documento jurídico - excluir', 'DOCUMENTO_JURIDICO_EXCLUIR');



---------MENU RECURSOS HUMANOS
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (100, current_date, 'Recursos humanos - menu', 'RECURSOS_HUMANOS_MENU');

---------MENU RELATÓRIO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (101, current_date, 'Relatório - menu', 'RELATORIO_MENU');

---------MENU GRÁFICO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (102, current_date, 'Gráfico - menu', 'GRAFICO_MENU');

---------MENU DOCUMENTO
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (103, current_date, 'Documento - menu', 'DOCUMENTO_MENU');


insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (104, current_date, 'Funcionário - imprimir', 'FUNCIONARIO_IMPRIMIR');


insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (105, current_date, 'Plano de férias - relatório', 'RELATORIO_PLANO_FERIAS');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (106, current_date, 'Controle de prazos - relatório', 'RELATORIO_CONTROLE_PRAZO');


---------MENU GRÁFICOS
insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (107, current_date, 'Gráfico - menu', 'GRAFICO_MENU');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (108, current_date, 'Controle de prazos - gráfico', 'GRAFICO_CONTROLE_PRAZO');


insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (109, current_date, 'Controle de jurídico - relatório', 'RELATORIO_CONTROLE_JURIDICO');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (110, current_date, 'Controle de jurídico resumo - relatório', 'RELATORIO_CONTROLE_JURIDICO_RESUMO');

insert into bdadm.tb_permissao (id, data_cadastro, descricao, "role")
values (111, current_date, 'Controle de entrega - relatório', 'RELATORIO_CONTROLE_ENTREGA');


