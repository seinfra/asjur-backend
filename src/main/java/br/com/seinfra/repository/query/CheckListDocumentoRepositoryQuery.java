package br.com.seinfra.repository.query;

import java.util.List;

import br.com.seinfra.model.CheckListDocumentoPendente;

public interface CheckListDocumentoRepositoryQuery {
	
	public List<CheckListDocumentoPendente> checkListDocs(Long id, Long documento);

}
