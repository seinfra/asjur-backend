package br.com.seinfra.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Municipio;
import br.com.seinfra.model.filter.MunicipioFilter;
import br.com.seinfra.repository.MunicipioRepository;

@Service
public class MunicipioService  extends GenericBaseService<MunicipioRepository, Municipio, Long>{
	
	public Page<Municipio> pequisar(MunicipioFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public Municipio buscarPorNome(String nome) {
		return repositorio.findByNome(nome);
	}
	
}
