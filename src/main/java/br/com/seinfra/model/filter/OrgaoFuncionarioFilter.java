/*
 * 21/07/2009
 */
package br.com.seinfra.model.filter;

public class OrgaoFuncionarioFilter {

	private String orgao;
	
    private String funcionario;

	public String getOrgao() {
		return orgao;
	}

	public void setOrgao(String orgao) {
		this.orgao = orgao;
	}

	public String getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(String funcionario) {
		this.funcionario = funcionario;
	}
    
}
