package br.com.seinfra.model.enums;

public enum ORIGEM_ENTIDADE {

	ESTADUAL,
	MUNICIPAL,
	OUTROS;
	
	public String getNome() {
		return toString().replaceAll("_", " ");
	}
}
