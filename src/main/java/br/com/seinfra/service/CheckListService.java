package br.com.seinfra.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.CheckList;
import br.com.seinfra.model.CheckListDocumento;
import br.com.seinfra.model.CheckListDocumentoPendente;
import br.com.seinfra.model.CheckListSub;
import br.com.seinfra.model.CheckListTipo;
import br.com.seinfra.model.filter.CheckListFilter;
import br.com.seinfra.repository.CheckListRepository;

@Service
public class CheckListService extends GenericBaseService<CheckListRepository, CheckList	, Long>{
	
	@Autowired
	private CheckListDocumentoService checkListDocumentoService;
	
	public Page<CheckList> pequisar(CheckListFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public CheckList buscarPorDescricao(String descricao) {
		return repositorio.findByDescricao(descricao);
	}
	
	public List<CheckListTipo> checkListTipo(Long id) {
		return repositorio.checkListTipo(id);
	}
	
	public List<CheckListSub> checkListSub(Long id){
		return repositorio.checkListSub(id);
	}
	
	public CheckListSub checkListSubSelecionado(Long id) {
		return repositorio.checkListSubSelecionado(id);
	}
	
	public List<CheckListDocumento> checkListDocs(String id, String documento) {
		List<CheckListDocumento> documentos = new ArrayList<CheckListDocumento>();
		if(documento != null && !StringUtils.isEmpty(documento)) {
			for(CheckListDocumento checkListDoc : repositorio.checkListDocs(Long.valueOf(id), Long.valueOf(documento))) {
				for(CheckListDocumentoPendente checkListDocPed : checkListDocumentoService.checkListDocs(checkListDoc.getId(), Long.valueOf(documento))) {
					checkListDoc.setAtivo(checkListDocPed.getAtivo());
				}
				documentos.add(checkListDoc);
			}
		}
		return documentos;
	}
	
}
