package br.com.seinfra.model.enums;

public enum STATUS_FERIAS {

	GOZADA("GOZADA"),
	A_SER_GOZADA("A SER GOZADA"),
	EXPIRADA("EXPIRADA");
	
	private final String descricao;
	
	STATUS_FERIAS(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
