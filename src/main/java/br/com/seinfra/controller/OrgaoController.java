package br.com.seinfra.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.Orgao;
import br.com.seinfra.model.filter.OrgaoFilter;
import br.com.seinfra.service.OrgaoService;

@RestController
@RequestMapping("/asjur-api/orgao")
public class OrgaoController extends GenericBaseController<OrgaoService, Orgao, Long> {
	
	@GetMapping	
	public Page<Orgao> pequisar(OrgaoFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
