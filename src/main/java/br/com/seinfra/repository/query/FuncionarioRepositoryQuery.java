package br.com.seinfra.repository.query;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.DocumentoJuridico;
import br.com.seinfra.model.Funcionario;
import br.com.seinfra.model.PlanoFerias;
import br.com.seinfra.model.filter.FuncionarioFilter;

public interface FuncionarioRepositoryQuery {

	public Page<Funcionario> pequisar(FuncionarioFilter filtro, Pageable pageable);
	
	public Funcionario findByNome(String nome);
	
	public List<Funcionario> distribuicaoFuncionario(DocumentoJuridico documento, List<Funcionario> funcionarios);
	
	public List<Funcionario> funcionarioSemDocumento(DocumentoJuridico documento, List<Funcionario> funcionarios);
	
	public List<Funcionario> funcionariosFerias();
	
	public List<Funcionario> funcionariosAfastados();
	
	public List<PlanoFerias> relatorioPlanoFerias(Integer ano, LocalDate inicio, LocalDate fim, String ugb);
	
	public List<Funcionario> funcionariosComDocumento(DocumentoJuridico documento);
	
	public List<Funcionario> funcionariosJuridico();
	
	public Funcionario distribuicaoCompliance(String nome);
	
	public Funcionario buscarPorCoordenador();

	public List<Funcionario> buscarFuncionarioCompliance();
}
