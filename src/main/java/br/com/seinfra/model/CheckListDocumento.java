package br.com.seinfra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_chk_doc", schema = "bdscj")
public class CheckListDocumento extends GenericBaseModel<Long> {

	@ManyToOne
	@JsonBackReference("checkListDocumentos")
	@JoinColumn(name = "check_sub_id")
	private CheckListSub checkListSub;
	
	@Size(max = 2000)
	@Column(name = "ched_doc_desc")
	private String descricao;
	
	@Transient
	private Boolean ativo = Boolean.FALSE;

	public CheckListSub getCheckListSub() {
		return checkListSub;
	}

	public void setCheckListSub(CheckListSub checkListSub) {
		this.checkListSub = checkListSub;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
}

