package br.com.seinfra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_permissao", schema = "bdadm")
public class Permissao extends GenericBaseModel<Long> {

	@NotNull
	@Size(min = 3, max = 50)
	@Column(name = "descricao")
	private String descricao;

	@NotNull
	@Size(min = 3, max = 50)
	@Column(name = "role")
	private String role;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
