package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Referencia;
import br.com.seinfra.repository.query.ReferenciaRepositoryQuery;

@Repository
public interface ReferenciaRepository extends JpaRepository<Referencia, Long>, ReferenciaRepositoryQuery {

	public Referencia findByDescricao(String descricao);
}
