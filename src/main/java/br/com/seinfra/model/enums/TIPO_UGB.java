package br.com.seinfra.model.enums;

public enum TIPO_UGB {

	  APOIO,
	  COMPLIANCE,
	  COORDENACAO,
      JURIDICO;

      public String getNome() {
          return toString().replaceAll("_", " ");
      }
}
