package br.com.seinfra.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Permissao;
import br.com.seinfra.model.filter.PermissaoFilter;
import br.com.seinfra.repository.PermissaoRepository;

@Service
public class PermissaoService  extends GenericBaseService<PermissaoRepository, Permissao, Long>{
	
	public Page<Permissao> pequisar(PermissaoFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
}
