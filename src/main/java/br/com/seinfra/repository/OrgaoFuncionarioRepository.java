package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.OrgaoFuncionario;
import br.com.seinfra.repository.query.OrgaoFuncionarioRepositoryQuery;

@Repository
public interface OrgaoFuncionarioRepository extends JpaRepository<OrgaoFuncionario, Long>, OrgaoFuncionarioRepositoryQuery {
	
}
