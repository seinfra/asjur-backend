package br.com.seinfra.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.seinfra.model.Funcionario;
import br.com.seinfra.model.Orgao;
import br.com.seinfra.model.OrgaoFuncionario;
import br.com.seinfra.repository.query.OrgaoFuncionarioRepositoryQuery;

public class OrgaoFuncionarioRepositoryImpl implements OrgaoFuncionarioRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	public OrgaoFuncionario findByOrgaoAndFuncionario(String orgao, String funcionario) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<OrgaoFuncionario> criteria = builder.createQuery(OrgaoFuncionario.class);
		Root<OrgaoFuncionario> root = criteria.from(OrgaoFuncionario.class);
		Join<OrgaoFuncionario, Orgao> joinOrgao = root.join("orgao");

		List<Predicate> predicates = new ArrayList<>();
		
		predicates.add(builder.equal(root.get("funcionario"), new Funcionario(Long.valueOf(funcionario))));
		predicates.add(builder.equal(joinOrgao.get("id"), Long.valueOf(orgao)));
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));

		TypedQuery<OrgaoFuncionario> query = manager.createQuery(criteria);
		
		return query.getResultList().size() > 0 ?query.getResultList().get(0) : new OrgaoFuncionario();
	}
	
}
