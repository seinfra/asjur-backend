package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Armario;
import br.com.seinfra.repository.query.ArmarioRepositoryQuery;

@Repository
public interface ArmarioRepository extends JpaRepository<Armario, Long>, ArmarioRepositoryQuery {
	
	public Armario findByNome(String nome);

}
