package br.com.seinfra.service;

import java.io.IOException;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.CheckListSub;
import br.com.seinfra.model.DocumentoJuridico;
import br.com.seinfra.model.Funcionario;
import br.com.seinfra.model.MovimentoDocumento;
import br.com.seinfra.model.Parecer;
import br.com.seinfra.model.UnidadeAdministrativa;
import br.com.seinfra.model.Usuario;
import br.com.seinfra.model.dto.DocumentoForm;
import br.com.seinfra.model.filter.DocumentoJuridicoFilter;
import br.com.seinfra.other.Propriedades;
import br.com.seinfra.repository.DocumentoJuridicoRepository;
import net.sf.jasperreports.engine.JasperExportManager;

@Service
public class DocumentoJuridicoService extends GenericBaseService<DocumentoJuridicoRepository, DocumentoJuridico, Long>{
	
	@Autowired
	private TipoDocumentoService tipoDocumentoService;
	
	@Autowired 
	private UnidadeAdministrativaService unidadeAdministrativaService;
	
	@Autowired
	private AssuntoService assuntoService;
	
	@Autowired
	private CaixaService caixaService;
	
	@Autowired
	private PastaService pastaService;
	
	@Autowired
	private ArmarioService armarioService;
	
	@Autowired
	private ReferenciaService referenciaService;
	
	@Autowired
	private FuncionarioService funcionarioService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private OrgaoService orgaoService;
	
	@Autowired
	private CheckListService checkListService;
	
	@Autowired
    private ServletContext servletContext;
	
	@Autowired 
	private JavaMailSender mailSender;
	
	private Propriedades propertie = new Propriedades();
	
	public List<DocumentoJuridico> documentoPorFuncionarioReceber(String cpf){
		return repositorio.documentoPorFuncionarioReceber(cpf);
	}
	
	public Page<DocumentoJuridico> pequisar(DocumentoJuridicoFilter filtro, Pageable pageable) {
		filtro.setFuncionario(funcionarioService.buscarPorCpf(filtro.getCpf()));
		return repositorio.pequisar(filtro, pageable);
	}
	
	public List<DocumentoJuridico> documentoPorUsuario(String cpf){
		Funcionario funcionario = funcionarioService.buscarPorCpf(cpf);
		return repositorio.documentoPorUsuario(funcionario);
	}
	
	public Optional<DocumentoJuridico> buscarPorId(Long id){
		return repositorio.findById(id);
	}
	
	public List<DocumentoJuridico> documentoParaAnaliseCoordenador(){
		return repositorio.documentoParaAnaliseCoordenador();
	}
	
	public List<DocumentoJuridico> documentoParaCheckList(String cpf){
		return repositorio.documentoParaCheckList(cpf);
	}
	
	public List<DocumentoJuridico> documentoFinalizar(){
		return repositorio.documentoFinalizar();
	}
	
	public List<DocumentoJuridico> estatisticaAdvogado(String prazo){
		return repositorio.estatisticaAdvogado(prazo);
	}
	
	public List<DocumentoJuridico> estatisticaUgb(String prazo){
		return repositorio.estatisticaUgb(prazo);
	}
	
	public List<DocumentoJuridico> inicioDistribuicao(){
		return repositorio.inicioDistribuicao();
	}
	
	public List<DocumentoJuridico> distribuicaoEmBloco() {
		for(DocumentoJuridico entidade : inicioDistribuicao()) {
			entidade.setStatus("Aguardando checklist");
			entidade.setFuncionario(distribuicaoFuncionario(entidade));
			alterar(entidade.getId(), entidade);
		}
		return null;
	}
	
	public DocumentoJuridico distribuicaoIndividual(Long id) {
		Optional<DocumentoJuridico> entidade = repositorio.findById(id);
		DocumentoJuridico entity = entidade.get();
		
		entity.setStatus("Aguardando checklist");
		entity.setFuncionario(distribuicaoFuncionario(entity));
		return repositorio.save(entity);
	}
	
	public byte[] relatorioControlePrazos(LocalDate inicio, LocalDate fim, String ugb) throws Exception {
		List<Parecer> dados = repositorio.relatorioControPrazo(inicio, fim, ugb);
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
		
		return JasperExportManager.exportReportToPdf(propertie.verificaDados(dados, parametros , "controle-prazos"));	
	}
	
	public byte[] relatorioControleEntrega(LocalDate inicio, LocalDate fim, String id) throws Exception {
		List<Parecer> dados = repositorio.relatorioControEntrega(inicio, fim, id);
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
		parametros.put("FUNCIONARIO", new Locale("pt", "BR"));
		
		return JasperExportManager.exportReportToPdf(propertie.verificaDados(dados, parametros , "controle-entrega"));	
	}
	
	public byte[] relatorioControleJuridico(DocumentoForm documentoForm) throws Exception {
		String relatorio = "controle-juridico-advogado";
		List<DocumentoJuridico> dados = new ArrayList<>();

		if(documentoForm.getTipo().equals("COMPLEXIDADE")) {
			relatorio = "controle-juridico-complexidade";
			dados = repositorio.relatorioControJuridicoPorComplexidade(documentoForm);
		}else {
			dados = repositorio.relatorioControJuridicoPorAdvogado(documentoForm);
		}
			Map<String, Object> parametros = new HashMap<>();
		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
		
		return JasperExportManager.exportReportToPdf(propertie.verificaDados(dados, parametros , relatorio));	
	}
	
	public byte[] relatorioControleJuridicoResumo(DocumentoForm documentoForm) throws Exception {
		String relatorio = "controle-juridico-resumo-advogado";
		List<DocumentoJuridico> dados = new ArrayList<>();

		if(documentoForm.getTipo().equals("COMPLEXIDADE")) {
			relatorio = "controle-juridico-resumo-complexidade";
			dados = repositorio.relatorioControJuridicoPorComplexidadeResumo(documentoForm);
		}else {
			dados = repositorio.relatorioControJuridicoPorAdvogadoResumo(documentoForm);
		}
			Map<String, Object> parametros = new HashMap<>();
		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
		
		return JasperExportManager.exportReportToPdf(propertie.verificaDados(dados, parametros , relatorio));	
	}
	
	public byte[] relatorioProcesso(Long id) throws Exception {
	    Date dataAtual = new Date();
	    DateFormat formatador = DateFormat.getDateInstance(DateFormat.FULL, new Locale("pt", "BR"));
	    String dataExtenso = formatador.format(dataAtual);
		
		List<DocumentoJuridico> dados = repositorio.relatorioProcesso(id);
			Map<String, Object> parametros = new HashMap<>();
		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
		parametros.put("data", dataExtenso.substring(dataExtenso.indexOf(","), dataExtenso.length()));
		parametros.put("imageUrl", servletContext.getRealPath("/WEB-INF/classes/image/logo_ce_1.jpg"));
		dados.get(0).setParecer(dados.get(0).getPareceres().get(0).getId());
		
		return JasperExportManager.exportReportToPdf(propertie.verificaDados(dados, parametros , "processo"));	
	}
	
	public List<DocumentoJuridico> listarDocumento(){
		return repositorio.findAll();
	}
	
	// Editar
		public DocumentoJuridico alterarDocumento(Long codigo, DocumentoJuridico entidade) {
			if(entidade.getSubTipo() != null) {
				CheckListSub sub = checkListService.checkListSubSelecionado(entidade.getSubTipo());
				if(sub != null) {
					entidade.setPrioridade(sub.getComplexidade());
					entidade.setDataInicioPrazo(entidade.getDataEnvioRecebimento().plusDays(1).toLocalDate());
					entidade.setPrazo(calculaPrazo(entidade.getPrioridade()));
					entidade.setDataFimPrazo(entidade.getDataEnvioRecebimento().plusDays(sub.getPrazo()-2).toLocalDate());
				}
			}
			if(entidade.getDataConclusao() != null || entidade.getObservacaoDiligencia() != null) {
				Funcionario funcionario = funcionarioService.buscarPorCoordenador();
				MovimentoDocumento movimento = new MovimentoDocumento();
				movimento.setDataHoraEnvio(LocalDateTime.now());
				if(entidade.getObservacao() != null) {
					movimento.setFuncionarioOrigem(entidade.getFuncionario());
					movimento.setFuncionarioDestino(funcionario);
					movimento.setParecer(entidade.getParecerJuridico());
					movimento.setObservacao(entidade.getObservacao());
					entidade.setObservacaoDiligencia(null);
					entidade.setParecerJuridicoDiligencia(null);

				}else {
					movimento.setFuncionarioOrigem(funcionario);
					movimento.setFuncionarioDestino(entidade.getFuncionario());
					movimento.setParecer(entidade.getParecerJuridicoDiligencia());
					movimento.setObservacao(entidade.getObservacaoDiligencia());
					entidade.setObservacao(null);
					entidade.setParecerJuridico(null);
				}
				movimento.setDocumento(entidade);
				entidade.getMovimentosDocumento().add(movimento);
			}
			DocumentoJuridico entidadeSalva = buscarPorCodigo(codigo);
			BeanUtils.copyProperties(entidade, entidadeSalva, "codigo");
			return repositorio.save(entidadeSalva);
		}
	
	//----IMPORTAÇÃO SAD --------------------------------------------------
	public Funcionario distribuicaoFuncionario(DocumentoJuridico documento) {
		Integer max = 0;
		Integer maior = 0;
		Funcionario funcionario = new Funcionario();
		List<Funcionario> funcionarios = new ArrayList<Funcionario>();
		
		if(!StringUtils.isEmpty(documento.getPrioridade()) && documento.getPrazo() != null) {
			Funcionario funcionarioCompliance = this.funcionarioService.distribuicaoCompliance(documento.getInteressado());
	
			if(funcionarioCompliance == null || funcionarioCompliance.getId() == null || documento.getUgb().equals("JURIDICO")) {
				List<DocumentoJuridico> documentoPrioridade = repositorio.documentoPorFuncionario(documento, funcionarioService.funcionariosFerias(), funcionarioService.funcionariosAfastados());
				List<Funcionario> funcinariosComDocumento = new ArrayList<>();
				for(DocumentoJuridico doc : documentoPrioridade) {
					funcinariosComDocumento.add(doc.getFuncionario());
				}
				funcionarios = funcionarioService.distribuicaoFuncionario(documento, funcinariosComDocumento);
				List<Funcionario> funcionarioSemDocumento = funcionarioService.funcionarioSemDocumento(documento, funcinariosComDocumento);
				List<Funcionario> funcionarioComDocumento = funcionarioService.funcionariosComDocumento(documento);
		
				if (funcionarioSemDocumento.size() > 0) {
					funcionarios = funcionarioSemDocumento;
				}
				if(funcionarios.size() == 0) {
					funcionarios = funcionarioComDocumento;
					if(funcionarios.size() > 0) {
						max = repositorio.proximoDocumento(documento, funcionarios.get(0)).size();
						for(Funcionario func : funcionarios) {
							maior = repositorio.proximoDocumento(documento, func).size();
							if(maior < max) {
								max = maior;
								funcionario = repositorio.proximoDocumento(documento, func).get(0).getFuncionario();
							}
						}
					}
					if(funcionario.getId() != null) {
						funcionarios.clear();
						funcionarios.add(funcionario);
					}
				}
			}else {
				funcionarios.add(funcionarioService.distribuicaoCompliance(documento.getInteressado()));
			}
		}
		return funcionarios.size() > 0 ? funcionarios.get(0) : null;
	}
	
	public UnidadeAdministrativa buscarUnidadeAdministrativa(UnidadeAdministrativa unidadeAdministrativa) {
		UnidadeAdministrativa unidadeAdmin = null;
		
		if(unidadeAdministrativa != null) {
			unidadeAdmin = unidadeAdministrativaService.buscarPorNome(unidadeAdministrativa.getNome());
			if(unidadeAdmin == null) {
				unidadeAdministrativa.setId(null);
				unidadeAdministrativaService.incluir(unidadeAdministrativa);
				unidadeAdmin = unidadeAdministrativaService.buscarPorNome(unidadeAdministrativa.getNome());
			}
		}
		return unidadeAdmin;
	}
	
	public DocumentoJuridico incluirDocumento(DocumentoJuridico entidade, String distribuicao) {
		List<DocumentoJuridico> documentos = repositorio.documentoFiltro(entidade);
		
		if(documentos.size() == 0) {
			entidade.setId(null);
			Funcionario funcionarioCompliance = this.funcionarioService.distribuicaoCompliance(entidade.getInteressado());
			if(funcionarioCompliance == null || funcionarioCompliance.getId() != null) {
				entidade.setUgb("COMPLIANCE");
			}else {
				entidade.setUgb("JURIDICO");				
			}
			entidade.setPareceres(pareceres(entidade));
			entidade.setDataEnvioRecebimento(LocalDateTime.now());
			if(entidade.getPrioridade() != null) {
				entidade.setDataInicioPrazo(entidade.getDataEnvioRecebimento().plusDays(1).toLocalDate());
				entidade.setPrazo(calculaPrazo(entidade.getPrioridade()));
				entidade.setDataFimPrazo(entidade.getDataEnvioRecebimento().plusDays(entidade.getPrazo()-2).toLocalDate());
			}
			entidade.setDataDoc(entidade.getDataDoc() != null ? entidade.getDataDoc() : LocalDate.now());
			if(distribuicao.equals("S")) {
				entidade.setFuncionario(distribuicaoFuncionario(entidade));
				entidade.setStatus("Aguardando checklist");
			}
			entidade.setUnidadeAdministrativa(buscarUnidadeAdministrativa(entidade.getUnidadeAdministrativa()));
			entidade.setTipoDocumento(tipoDocumentoService.buscarTipoDocumento(entidade.getTipoDocumento()));
			entidade.setReferencia(referenciaService.buscarReferencia(entidade.getReferencia()));
			entidade.setAssunto(assuntoService.buscarAssunto(entidade.getAssunto()));
			entidade.setCaixa(caixaService.buscarCaixa(entidade.getCaixa()));
			entidade.setPasta(pastaService.buscarPasta(entidade.getPasta(), entidade.getCaixa()));
			entidade.setArmario(armarioService.buscarArmario(entidade.getArmario()));
			entidade.setOrgao(orgaoService.buscarOrgao(entidade.getOrgao()));
			
			return repositorio.save(entidade);
		}
		return null;
	}
	
	public List<Parecer> pareceres(DocumentoJuridico entidade){
		Parecer parecer = new Parecer(entidade);
		List<Parecer> pareceres = new ArrayList<>();
		pareceres.add(parecer);
		
		return pareceres;
	}
	
	@Scheduled(cron = "0 00 16 ? * MON-FRI") 
	public void importarDocumento() throws JsonParseException, JsonMappingException, IOException {
		final String uri = "http://seinfrasin5.seinfra.ce.gov.br/sad-backend/sad-api/documento/service";
	     
	    RestTemplate restTemplate = new RestTemplate();
	    String result = restTemplate.getForObject(uri, String.class);
		Gson gson = new Gson();
	
		List<DocumentoJuridico> listaDocumentos = Arrays.asList(gson.fromJson(result, DocumentoJuridico[].class));
	      for(DocumentoJuridico documento : listaDocumentos){
	       	String data = documento.getDataDocumento().substring(0, 10);
	       	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	       	LocalDate date = LocalDate.parse(data, formatter);
	       	documento.setDataDoc(date);
	       	incluirDocumento(documento, "S");
	      }
	}
	
	@Scheduled(cron = "0 00,30 12,08 ? * MON-FRI") 
	public void importarDocumentoSemDistribuicao() throws JsonParseException, JsonMappingException, IOException {
		final String uri = "http://seinfrasin5.seinfra.ce.gov.br/sad-backend/sad-api/documento/service";
	     
	    RestTemplate restTemplate = new RestTemplate();
	    String result = restTemplate.getForObject(uri, String.class);
     	Gson gson = new Gson();
	
		List<DocumentoJuridico> listaDocumentos = Arrays.asList(gson.fromJson(result, DocumentoJuridico[].class));
	      for(DocumentoJuridico documento : listaDocumentos){
	       	String data = documento.getDataDocumento().substring(0, 10);
	       	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	       	LocalDate date = LocalDate.parse(data, formatter);
	       	documento.setDataDoc(date);
	       	incluirDocumento(documento, "N");
	      }
	}
	
	@Scheduled(cron = "0 00 8 ? * MON-FRI") 
	public void sendMail() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject("Aviso Vencimento de Processo");
        message.setFrom("aline.saldanha@seinfra.ce.gov.br");
        
		Usuario usuario = new Usuario();
		List<DocumentoJuridico> documentos = repositorio.envioEmailProcesso();
		
		for(DocumentoJuridico documento : documentos) {
			Long dias = ChronoUnit.DAYS.between(LocalDate.now(), documento.getDataFimPrazo());
			if(dias <= 3L) {
				usuario = new Usuario();
				usuario = usuarioService.envioEmailUsuario(documento.getFuncionario().getCpf()).get(0);
		        message.setText("PROCESSO Nº " + documento.getNumeroProcesso() + " TERMINA EM " + dias + " DIAS!");
		        message.setTo(usuario.getEmail());
		        mailSender.send(message);
			}
		}
    }
	
	public Long calcularPrazoDias(LocalDateTime dataEnvioRecebimento, LocalDateTime dataFimPrazo) {
		Long prazo = 0L;
		if(dataEnvioRecebimento != null && dataFimPrazo != null) {
			prazo = ChronoUnit.DAYS.between(LocalDate.now(), dataFimPrazo);
		}
		return prazo;
	}
	
	public Long calculaPrazo(String prioridade) {
		Long prazo = 2L;
		if(prioridade.equals("ALTISSIMA")){
			prazo = 30L;
		    }
		else if(prioridade.equals("ALTA")){
			prazo = 15L;
	    }
	    else if(prioridade.equals("MEDIA")){
			prazo = 10L;
	    }
	    else if(prioridade.equals("BAIXA")){
			prazo = 5L;
	    }
	    return prazo;
	}
	
}
