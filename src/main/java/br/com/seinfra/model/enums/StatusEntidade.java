package br.com.seinfra.model.enums;

public enum StatusEntidade {

	ATIVO("Ativo"),
	INATIVO("Inativo");
	
	private final String descricao;
	
	StatusEntidade(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
