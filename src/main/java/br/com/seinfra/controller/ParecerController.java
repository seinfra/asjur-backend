package br.com.seinfra.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.Parecer;
import br.com.seinfra.service.ParecerService;

@RestController
@RequestMapping("/asjur-api/parecer")
public class ParecerController extends GenericBaseController<ParecerService, Parecer, Long> {
	
}