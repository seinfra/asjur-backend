/**
 *
 */
package br.com.seinfra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(schema = "bdsfn", name = "tb_crd")
public class Credor extends GenericBaseModel<Long> {

    @Column(name = "crd_nome", length = 100, nullable = false)
    private String nome;

    @Column(name = "crd_cpf_cnpj", length = 14)
    private String cpfCnpj;

    @Column(name = "crd_email", length = 200)
    private String email;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return id + "=" + nome;
    }
}
