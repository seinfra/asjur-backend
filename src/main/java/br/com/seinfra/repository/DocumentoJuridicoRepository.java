package br.com.seinfra.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.DocumentoJuridico;
import br.com.seinfra.repository.query.DocumentoJuridicoRepositoryQuery;

@Repository
public interface DocumentoJuridicoRepository extends JpaRepository<DocumentoJuridico, Long>, DocumentoJuridicoRepositoryQuery {

	public Optional<DocumentoJuridico> findById(Long id);
}
