package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Municipio;
import br.com.seinfra.repository.query.MunicipioRepositoryQuery;

@Repository
public interface MunicipioRepository extends JpaRepository<Municipio, Long>, MunicipioRepositoryQuery {

	public Municipio findByNome(String nome);
}
