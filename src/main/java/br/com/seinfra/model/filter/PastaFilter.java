/*
 * 21/07/2009
 */
package br.com.seinfra.model.filter;

public class PastaFilter {

	private Long id;
	
    private String nome;
    
    private String numero;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

}
