package br.com.seinfra.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Credor;
import br.com.seinfra.model.filter.CredorFilter;
import br.com.seinfra.repository.CredorRepository;

@Service
public class CredorService  extends GenericBaseService<CredorRepository, Credor, Long>{
	
	public Page<Credor> pequisar(CredorFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public Credor buscarPorNome(String nome) {
		return repositorio.findByNome(nome);
	}
	
}
