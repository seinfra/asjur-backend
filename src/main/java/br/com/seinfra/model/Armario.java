package br.com.seinfra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_armario", schema = "bdscj")
public class Armario extends GenericBaseModel<Long> {

	@Size(max = 100)
	@Column(name = "armario_nome")
	private String nome;

	@Size(max = 100)
	@Column(name = "armario_prateleira")
	private String prateleira;

	@ManyToOne
	@JoinColumn(name = "uadm_id")
	private UnidadeAdministrativa unidadeAdministrativa;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPrateleira() {
		return prateleira;
	}

	public void setPrateleira(String prateleira) {
		this.prateleira = prateleira;
	}

	public UnidadeAdministrativa getUnidadeAdministrativa() {
		return unidadeAdministrativa;
	}

	public void setUnidadeAdministrativa(UnidadeAdministrativa unidadeAdministrativa) {
		if(unidadeAdministrativa == null || unidadeAdministrativa.getNome() == null) {
			this.unidadeAdministrativa = null;
		}else {
			this.unidadeAdministrativa = unidadeAdministrativa;
		}    
	}

}
