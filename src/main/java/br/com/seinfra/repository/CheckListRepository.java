package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.CheckList;
import br.com.seinfra.repository.query.CheckListRepositoryQuery;

@Repository
public interface CheckListRepository extends JpaRepository<CheckList, Long>, CheckListRepositoryQuery {

	public CheckList findByDescricao(String descricao);
}
