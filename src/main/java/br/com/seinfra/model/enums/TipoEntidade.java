package br.com.seinfra.model.enums;

public enum TipoEntidade {

	PESSOA("Pessoa"),
	ORGAO("Orgão");
	
	private final String descricao;
	
	TipoEntidade(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
