package br.com.seinfra.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.Processo;
import br.com.seinfra.model.filter.ProcessoFilter;
import br.com.seinfra.service.ProcessoService;

@RestController
@RequestMapping("/asjur-api/processo")
public class ProcessoController extends GenericBaseController<ProcessoService, Processo, Long> {
	
	@GetMapping	
	public Page<Processo> pequisar(ProcessoFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}