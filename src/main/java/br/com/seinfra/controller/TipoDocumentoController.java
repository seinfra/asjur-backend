package br.com.seinfra.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.TipoDocumento;
import br.com.seinfra.model.filter.TipoDocumentoFilter;
import br.com.seinfra.service.TipoDocumentoService;

@RestController
@RequestMapping("/asjur-api/tipo-documento")
public class TipoDocumentoController extends GenericBaseController<TipoDocumentoService, TipoDocumento, Long> {
	
	@GetMapping	
	public Page<TipoDocumento> pequisar(TipoDocumentoFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
	@GetMapping("/buscarPorNome")
	public TipoDocumento buscarPorNome(String nome){
		return servico.buscarPorNome(nome);
	}
	
}