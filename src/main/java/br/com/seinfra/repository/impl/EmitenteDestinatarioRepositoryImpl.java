package br.com.seinfra.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.seinfra.model.EmitenteDestinatario;
import br.com.seinfra.model.filter.EmitenteDestinatarioFilter;
import br.com.seinfra.repository.query.EmitenteDestinatarioRepositoryQuery;

public class EmitenteDestinatarioRepositoryImpl implements EmitenteDestinatarioRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<EmitenteDestinatario> pequisar(EmitenteDestinatarioFilter filtro, Pageable pageable) {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<EmitenteDestinatario> criteria = builder.createQuery(EmitenteDestinatario.class);
		Root<EmitenteDestinatario> root = criteria.from(EmitenteDestinatario.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.asc(root.get("descricao")));

		TypedQuery<EmitenteDestinatario> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);

		return new PageImpl<>(query.getResultList(), pageable, total(filtro));
	}

	private Predicate[] criarRestricoes(EmitenteDestinatarioFilter filtro, CriteriaBuilder builder,
			Root<EmitenteDestinatario> root) {

		List<Predicate> predicates = new ArrayList<>();

		if(filtro.getId() != null) {			
			predicates.add(builder.equal(root.get("id"), filtro.getId()));
		}
		
		if (!StringUtils.isEmpty(filtro.getDescricao())) {
			predicates.add(builder.like(builder.lower(root.get("descricao")),
					"%" + filtro.getDescricao().toLowerCase() + "%"));
		}				

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;

		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);

	}

	private Long total(EmitenteDestinatarioFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<EmitenteDestinatario> root = criteria.from(EmitenteDestinatario.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	@Override
	public EmitenteDestinatario findByNome(String descricao) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<EmitenteDestinatario> criteria = builder.createQuery(EmitenteDestinatario.class);
		Root<EmitenteDestinatario> root = criteria.from(EmitenteDestinatario.class);

		criteria.where(builder.like(root.get("descricao"), "%" + descricao + "%"));
		criteria.orderBy(builder.asc(root.get("descricao")));

		TypedQuery<EmitenteDestinatario> query = manager.createQuery(criteria);
		
		return query.getResultList().size() > 0 ? query.getResultList().get(0) : new EmitenteDestinatario();
	}
	
}
