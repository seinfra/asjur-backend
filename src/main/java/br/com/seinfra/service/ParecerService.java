package br.com.seinfra.service;

import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Parecer;
import br.com.seinfra.repository.ParecerRepository;

@Service
public class ParecerService extends GenericBaseService<ParecerRepository, Parecer, Long>{
	
}
