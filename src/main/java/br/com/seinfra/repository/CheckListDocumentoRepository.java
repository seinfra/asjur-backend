package br.com.seinfra.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.CheckListDocumentoPendente;
import br.com.seinfra.model.DocumentoJuridico;
import br.com.seinfra.repository.query.CheckListDocumentoRepositoryQuery;

@Repository
public interface CheckListDocumentoRepository extends JpaRepository<CheckListDocumentoPendente, Long>, CheckListDocumentoRepositoryQuery {

	public List<CheckListDocumentoPendente> findByDocumento(DocumentoJuridico documento);
}
