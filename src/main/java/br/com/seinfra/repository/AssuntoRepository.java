package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Assunto;
import br.com.seinfra.repository.query.AssuntoRepositoryQuery;

@Repository
public interface AssuntoRepository extends JpaRepository<Assunto, Long>, AssuntoRepositoryQuery {

	public Assunto findByDescricao(String descricao);
}
