package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.Armario;
import br.com.seinfra.model.filter.ArmarioFilter;

public interface ArmarioRepositoryQuery {

	public Page<Armario> pequisar(ArmarioFilter filtro, Pageable pageable);

}
