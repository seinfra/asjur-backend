package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.Assunto;
import br.com.seinfra.model.filter.AssuntoFilter;

public interface AssuntoRepositoryQuery {

	public Page<Assunto> pequisar(AssuntoFilter filtro, Pageable pageable);

}
