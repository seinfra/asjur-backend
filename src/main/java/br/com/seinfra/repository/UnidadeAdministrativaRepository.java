package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.UnidadeAdministrativa;
import br.com.seinfra.repository.query.UnidadeAdministrativaRepositoryQuery;

@Repository
public interface UnidadeAdministrativaRepository extends JpaRepository<UnidadeAdministrativa, Long>, UnidadeAdministrativaRepositoryQuery {

	public UnidadeAdministrativa findByNome(String nome);
}
