package br.com.seinfra.service;

import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.CredorFuncionario;
import br.com.seinfra.repository.CredorFuncionarioRepository;

@Service
public class CredorFuncionarioService extends GenericBaseService<CredorFuncionarioRepository, CredorFuncionario, Long>{
	
	public CredorFuncionario buscarPorCredorFuncionario(String credor, String funcionario) {
		return repositorio.findByCredorAndFuncionario(credor, funcionario);
	}
}
