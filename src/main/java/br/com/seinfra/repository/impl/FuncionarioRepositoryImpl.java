package br.com.seinfra.repository.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.seinfra.model.Cargo;
import br.com.seinfra.model.DocumentoJuridico;
import br.com.seinfra.model.Funcionario;
import br.com.seinfra.model.Orgao;
import br.com.seinfra.model.OrgaoFuncionario;
import br.com.seinfra.model.PlanoFerias;
import br.com.seinfra.model.Requerimento;
import br.com.seinfra.model.filter.FuncionarioFilter;
import br.com.seinfra.repository.query.FuncionarioRepositoryQuery;

public class FuncionarioRepositoryImpl implements FuncionarioRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<Funcionario> pequisar(FuncionarioFilter filtro, Pageable pageable) {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Funcionario> criteria = builder.createQuery(Funcionario.class);
		Root<Funcionario> root = criteria.from(Funcionario.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.asc(root.get("nome")));

		TypedQuery<Funcionario> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);

		return new PageImpl<>(query.getResultList(), pageable, total(filtro));
	}

	private Predicate[] criarRestricoes(FuncionarioFilter filtro, CriteriaBuilder builder,
			Root<Funcionario> root) {

		List<Predicate> predicates = new ArrayList<>();

		if(filtro.getId() != null) {			
			predicates.add(builder.equal(root.get("id"), filtro.getId()));
		}
		
		if (!StringUtils.isEmpty(filtro.getNome())) {
			predicates.add(builder.like(builder.lower(root.get("nome")),
					"%" + filtro.getNome().toLowerCase() + "%"));
		}				

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;

		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);

	}

	private Long total(FuncionarioFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Funcionario> root = criteria.from(Funcionario.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	@Override
	public Funcionario findByNome(String nome) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Funcionario> criteria = builder.createQuery(Funcionario.class);
		Root<Funcionario> root = criteria.from(Funcionario.class);

		criteria.where(builder.like(root.get("nome"), "%" + nome + "%"));
		criteria.orderBy(builder.asc(root.get("nome")));

		TypedQuery<Funcionario> query = manager.createQuery(criteria);
		
		return query.getResultList().size() > 0 ? query.getResultList().get(0) : new Funcionario();
	}
	
	public List<Funcionario> distribuicaoFuncionario(DocumentoJuridico documento, List<Funcionario> funcionarios) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Funcionario> criteria = builder.createQuery(Funcionario.class);
		Root<Funcionario> root = criteria.from(Funcionario.class);
		Join<Funcionario, Cargo> cargoJoin = root.join("cargo");

		List<Predicate> predicates = new ArrayList<>();
		
		if(funcionarios.size() > 0) {
			predicates.add(root.in(funcionarios).not());
		}
		
		predicates.add(builder.equal(cargoJoin.get("nome"), "ADVOGADO"));
		if (!StringUtils.isEmpty(documento.getUgb())) {
			predicates.add(builder.equal(root.get("ugb"), documento.getUgb()));
		}
		if(funcionariosFerias().size() > 0) {			
			predicates.add(root.in(funcionariosFerias()).not());
		}
		if(funcionariosAfastados().size() > 0) {			
			predicates.add(root.in(funcionariosAfastados()).not());
		}
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		criteria.orderBy(builder.asc(root.get("nome")));

		TypedQuery<Funcionario> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<Funcionario> documentoComFuncionario() {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Funcionario> criteria = builder.createQuery(Funcionario.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
		Join<DocumentoJuridico, Funcionario> funcionarioJoin = root.join("funcionario");
		Join<Funcionario, Cargo> cargoJoin = funcionarioJoin.join("cargo");
		
		criteria.select(root.get("funcionario")).distinct(true);
		List<Predicate> predicates = new ArrayList<>();

		predicates.add(builder.equal(cargoJoin.get("nome"), "ADVOGADO"));
		if(funcionariosFerias().size() > 0) {			
			predicates.add(funcionarioJoin.in(funcionariosFerias()).not());
		}
		if(funcionariosAfastados().size() > 0) {			
			predicates.add(funcionarioJoin.in(funcionariosAfastados()).not());
		}
		
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));

		TypedQuery<Funcionario> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<Funcionario> funcionarioSemDocumento(DocumentoJuridico documento, List<Funcionario> funcionarios) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Funcionario> criteria = builder.createQuery(Funcionario.class);
		Root<Funcionario> root = criteria.from(Funcionario.class);
		Join<Funcionario, Cargo> cargoJoin = root.join("cargo");
		
		List<Predicate> predicates = new ArrayList<>();
		
		if(funcionarios.size() > 0) {
			predicates.add(root.in(funcionarios).not());
		}
		predicates.add(builder.equal(cargoJoin.get("nome"), "ADVOGADO"));
		if (!StringUtils.isEmpty(documento.getUgb())) {
			predicates.add(builder.equal(root.get("ugb"), documento.getUgb()));
		}
		if(funcionariosFerias().size() > 0) {			
			predicates.add(root.in(funcionariosFerias()).not());
		}
		if(funcionariosAfastados().size() > 0) {			
			predicates.add(root.in(funcionariosAfastados()).not());
		}
		if(documentoComFuncionario().size() > 0) {			
			predicates.add(root.in(documentoComFuncionario()).not());
		}
		
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		TypedQuery<Funcionario> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<Funcionario> funcionariosComDocumento(DocumentoJuridico documento){
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Funcionario> criteria = builder.createQuery(Funcionario.class);
		Root<Funcionario> root = criteria.from(Funcionario.class);
		Join<Funcionario, Cargo> cargoJoin = root.join("cargo");

		List<Predicate> predicates = new ArrayList<>();
		List<Funcionario> funcionarios = funcionariosPrioridade(documento.getPrioridade());
		
		if(funcionarios.size() > 0) {
			predicates.add(root.in(funcionarios).not());
		}
		predicates.add(builder.equal(cargoJoin.get("nome"), "ADVOGADO"));
		if(funcionariosFerias().size() > 0) {			
			predicates.add(root.in(funcionariosFerias()).not());
		}
		if(funcionariosAfastados().size() > 0) {			
			predicates.add(root.in(funcionariosAfastados()).not());
		}
		if (!StringUtils.isEmpty(documento.getUgb())) {
			predicates.add(builder.equal(root.get("ugb"), documento.getUgb()));
		}
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));

		TypedQuery<Funcionario> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<Funcionario> funcionariosPrioridade(String prioridade){
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);

		criteria.where(builder.equal(root.get("prioridade"), prioridade));
		criteria.orderBy(builder.desc(root.get("id")));

		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria).setMaxResults(1);
		List<Funcionario> funcionarios = new ArrayList<>();
		
		for(DocumentoJuridico documento : query.getResultList()) {
			funcionarios.add(documento.getFuncionario());
		}
		return funcionarios;
	}
	
	public List<Funcionario> funcionariosFerias(){
		
		TypedQuery<PlanoFerias> query = manager.createQuery(
			    "SELECT p "+
			    "FROM PlanoFerias p "+
			    "WHERE current_date between dataInicial and dataFinal", PlanoFerias.class);
		List<Funcionario> funcionariosFerias = new ArrayList<>();
		
		for(PlanoFerias plano : query.getResultList()) {
			funcionariosFerias.add(plano.getFuncionario());
		}
		return funcionariosFerias;
	}
	
	public List<Funcionario> funcionariosAfastados(){
		
		TypedQuery<Requerimento> query = manager.createQuery(
			    "SELECT r "+
			    "FROM Requerimento r "+
			    "WHERE current_date between dataInicial and dataFinal", Requerimento.class);
		List<Funcionario> funcionariosAfastado = new ArrayList<>();
		
		for(Requerimento requerimento : query.getResultList()) {
			funcionariosAfastado.add(requerimento.getFuncionario());
		}
		return funcionariosAfastado;
	}
	
	public List<PlanoFerias> relatorioPlanoFerias(Integer ano, LocalDate inicio, LocalDate fim, String ugb) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<PlanoFerias> criteria = builder.createQuery(PlanoFerias.class);
		Root<PlanoFerias> root = criteria.from(PlanoFerias.class);
		Join<PlanoFerias, Funcionario> joinFuncionario = root.join("funcionario");
		
		List<Predicate> predicates = new ArrayList<>();

		if(ugb != null && ugb != "") {			
			predicates.add(builder.equal(joinFuncionario.get("ugb"), ugb));
		}
		if(ano != null) {
			predicates.add(builder.equal(root.get("ano"), ano));
		}
		if(inicio != null) {
			predicates.add(builder.between(root.get("dataInicial"), inicio, fim));
		}
		if(fim != null) {
			predicates.add(builder.between(root.get("dataFinal"), inicio, fim));
		}
		
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		criteria.orderBy(builder.asc(joinFuncionario.get("ugb")));
		TypedQuery<PlanoFerias> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<Funcionario> funcionariosJuridico() {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Funcionario> criteria = builder.createQuery(Funcionario.class);
		Root<Funcionario> root = criteria.from(Funcionario.class);
		Join<Funcionario, Cargo> cargoJoin = root.join("cargo");

		List<Predicate> predicates = new ArrayList<>();
		
		predicates.add(builder.equal(cargoJoin.get("nome"), "ADVOGADO"));
		if(funcionariosFerias().size() > 0) {			
			predicates.add(root.in(funcionariosFerias()).not());
		}
		if(funcionariosAfastados().size() > 0) {			
			predicates.add(root.in(funcionariosAfastados()).not());
		}
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));

		TypedQuery<Funcionario> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public Funcionario distribuicaoCompliance(String nome) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<OrgaoFuncionario> criteria = builder.createQuery(OrgaoFuncionario.class);
		Root<OrgaoFuncionario> root = criteria.from(OrgaoFuncionario.class);
		Join<OrgaoFuncionario, Orgao> joinOrgao = root.join("orgao");
		
		criteria.where(builder.like(builder.lower(joinOrgao.get("nome")),
				"%" + nome.toLowerCase() + "%"));
		TypedQuery<OrgaoFuncionario> query = manager.createQuery(criteria);
		
		List<OrgaoFuncionario> orgaosFuncionario = query.getResultList();
		List<Funcionario> funcionarios = buscarFuncionarioCompliance();
		Funcionario funcionario = new Funcionario();
		
		if(orgaosFuncionario.size() > 0) {
			funcionario = orgaosFuncionario.get(0).getFuncionario();
			for(Funcionario funcionarioFerias : funcionariosFerias()) {
				if(funcionarioFerias == funcionario) {
					funcionarios.remove(funcionarioFerias);
					funcionario = funcionarios.get(0);
				}
				for(Funcionario funcionarioAfastado : funcionariosAfastados()) {
					if(funcionarioAfastado == funcionario) {
						funcionarios.remove(funcionarioAfastado);
						funcionario = funcionarios.get(0);
					}
				}
			}
		}
		return funcionario;
	}
	
	@Override
	public Funcionario buscarPorCoordenador() {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Funcionario> criteria = builder.createQuery(Funcionario.class);
		Root<Funcionario> root = criteria.from(Funcionario.class);
		Join<Funcionario, Cargo> joinCargo = root.join("cargo");

		criteria.where(builder.equal(joinCargo.get("nome"), "COORDENADOR"));

		TypedQuery<Funcionario> query = manager.createQuery(criteria);
		
		return query.getResultList().size() > 0 ? query.getResultList().get(0) : new Funcionario();
	}
	
	@Override
	public List<Funcionario> buscarFuncionarioCompliance() {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Funcionario> criteria = builder.createQuery(Funcionario.class);
		Root<Funcionario> root = criteria.from(Funcionario.class);

		criteria.where(builder.equal(root.get("ugb"), "COMPLIANCE"));

		TypedQuery<Funcionario> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
}
