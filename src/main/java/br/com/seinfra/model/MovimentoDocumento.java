package br.com.seinfra.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_mov", schema = "bdscj")
public class MovimentoDocumento extends GenericBaseModel<Long> {

    @Size(max = 2000)
    @Column(name = "mov_obs")
    private String observacao;
    
    @Size(max = 30)
    @Column(name = "mov_per")
    private String parecer;

    @Column(name = "mov_dt_hr")
    private LocalDateTime dataHoraEnvio;

    @Column(name = "mov_data_hora_recebimento")
    private LocalDateTime dataRecebimento;

    @ManyToOne
    @JoinColumn(name = "fnc_org_id")
    private Funcionario funcionarioOrigem;

    @ManyToOne
    @JoinColumn(name = "fnc_dst_id")
    private Funcionario funcionarioDestino;

	@JsonBackReference("movimentosDocumento")
    @ManyToOne
    @JoinColumn(name = "doc_id")
    private DocumentoJuridico documento;

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getParecer() {
		return parecer;
	}

	public void setParecer(String parecer) {
		this.parecer = parecer;
	}

	public LocalDateTime getDataHoraEnvio() {
		return dataHoraEnvio;
	}

	public void setDataHoraEnvio(LocalDateTime dataHoraEnvio) {
		this.dataHoraEnvio = dataHoraEnvio;
	}

	public LocalDateTime getDataRecebimento() {
		return dataRecebimento;
	}

	public void setDataRecebimento(LocalDateTime dataRecebimento) {
		this.dataRecebimento = dataRecebimento;
	}

	public Funcionario getFuncionarioOrigem() {
		return funcionarioOrigem;
	}

	public void setFuncionarioOrigem(Funcionario funcionarioOrigem) {
		this.funcionarioOrigem = funcionarioOrigem;
	}

	public Funcionario getFuncionarioDestino() {
		return funcionarioDestino;
	}

	public void setFuncionarioDestino(Funcionario funcionarioDestino) {
		this.funcionarioDestino = funcionarioDestino;
	}

	public DocumentoJuridico getDocumento() {
		return documento;
	}

	public void setDocumento(DocumentoJuridico documento) {
		this.documento = documento;
	}

}
