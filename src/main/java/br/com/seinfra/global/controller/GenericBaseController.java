package br.com.seinfra.global.controller;


import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.seinfra.global.model.IGenericBaseModel;
import br.com.seinfra.global.service.IGenericBaseService;

/*
* @param <S> Classe do serviço da classe a ser controlada.
* @param <E> Classe da entidade a ser manipulada.
* @param <T> Classe do tipo de dado do id da classe a ser manipulada.
*/

public class GenericBaseController<S extends IGenericBaseService<E, T>, E extends IGenericBaseModel, T> {

	@Autowired
	protected S servico;

//	@Autowired
//	private ApplicationEventPublisher publisher;

	// Novo
	@PostMapping
	public ResponseEntity<?> incluir(@Valid @RequestBody E entidade, HttpServletResponse response) {

		E novo = servico.incluir(entidade);
	//	publisher.publishEvent(new RecursoCriadoEvent(this, response, (Long) novo.getCodigo()));
		return ResponseEntity.status(HttpStatus.CREATED).body(novo);

	}

	// Editar
	@PutMapping("/{codigo}")
	public ResponseEntity<?> alterar(@PathVariable Long codigo, @Valid @RequestBody E entidade) {
		E entidadeSalva = servico.alterar(codigo, entidade);
		return ResponseEntity.ok(entidadeSalva);
	}

	// Remover
	@DeleteMapping("/{codigo}")
	public void excluir(@PathVariable T codigo) {
		servico.excluir(codigo);		
	}

	//Buscar por código
	@GetMapping("/{codigo}")
	public ResponseEntity<?> buscarPorId(@PathVariable T codigo) throws Exception {
		E entidade = servico.buscarPorCodigo(codigo);
		if (entidade == null) {
			return ResponseEntity.notFound().build();
		}
		ResponseEntity<?> response = ResponseEntity.ok(entidade);
		return response;
	}
	
	//Listar
	@GetMapping("/listar")
	public ResponseEntity<?> listar(){
		return ResponseEntity.ok(servico.listar());
	}

}

