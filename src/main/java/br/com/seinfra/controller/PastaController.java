package br.com.seinfra.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.Pasta;
import br.com.seinfra.model.filter.PastaFilter;
import br.com.seinfra.service.PastaService;

@RestController
@RequestMapping("/asjur-api/pasta")
public class PastaController extends GenericBaseController<PastaService, Pasta, Long> {
	
	@GetMapping	
	public Page<Pasta> pequisar(PastaFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
	@GetMapping("/buscarPorNome")
	public Pasta buscarPorNome(String nome){
		return servico.buscarPorNome(nome);
	}
	
	@PostMapping("/incluirPasta")
	public ResponseEntity<?> incluir(@Valid @RequestBody Pasta entidade, HttpServletResponse response) {

		Pasta novo = servico.incluirPasta(entidade);
		return ResponseEntity.status(HttpStatus.CREATED).body(novo);

	}
	
}