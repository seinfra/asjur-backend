package br.com.seinfra.model.enums;

public enum TIPO_ORGAO {

    AGÊNCIA("AGÊNCIA"),
    ASSEMBLÉIA("ASSEMBLÉIA"),
    DEFENSORIA("DEFENSORIA"),
    ENTIDADE_ESTADUAL("ENTIDADE ESTADUAL"),
    FUNDAÇÃO("FUNDAÇÃO"),
    INSTITUTO("INSTITUTO"),
    MINISTERIO("MINISTERIO"),
    POLICIA("POLICIA"),
    PREFEITURA("PREFEITURA"),
    PROCURADORIA("PROCURADORIA"),
    SECRETARIA("SECRETARIA"),
    SUPERINTENDÊNCIA("SECRETARIA"),
    UNIVERSIDADE("UNIVERSIDADE"),
    TRIBUNAL("TRIBUNAL");
	
	private final String descricao;
	
	TIPO_ORGAO(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
