package br.com.seinfra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_contato", schema = "bdscj")
public class Contato extends GenericBaseModel<Long> {

	@Size(min = 0, max = 40)
	@Column(name = "email")
	private String email;

	@Size(min = 0, max = 16)
	@Column(name = "telefone")
	private String telefone;
	
	@Size(min = 0, max = 4)
	@Column(name = "ramal")
	private String ramal;

	@ManyToOne
	@JsonBackReference("contatosFuncionario")
	@JoinColumn(name = "codigo_funcionario")
	private Funcionario funcionario;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getRamal() {
		return ramal;
	}

	public void setRamal(String ramal) {
		this.ramal = ramal;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

}
