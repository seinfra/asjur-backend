package br.com.seinfra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_chk_doc_pdt", schema = "bdscj")
public class CheckListDocumentoPendente extends GenericBaseModel<Long> {
	
	@ManyToOne
	@JoinColumn(name = "chk_id")
	private CheckList CheckList;
	
	@ManyToOne
	@JoinColumn(name = "chk_tp_id")
	private CheckListTipo checkListTipo;
	
	@ManyToOne
	@JoinColumn(name = "chk_sub_id")
	private CheckListSub checkListSub;

	@ManyToOne
	@JoinColumn(name = "check_doc_id")
	private CheckListDocumento checkListDocumento;
	
	@ManyToOne
	@JsonBackReference("documentosPendente")
	@JoinColumn(name = "doc_id")
	private DocumentoJuridico documento;
	
	@Column(name = "ched_atv")
	private Boolean ativo;
	
	@Column(name = "ched_doc_desc")
	private String descricao;
	
	
	public CheckListDocumentoPendente() {
		super();
	}

	public CheckListDocumentoPendente(Long id) {
		this.id = id;
	}

	public CheckList getCheckList() {
		return CheckList;
	}

	public void setCheckList(CheckList checkList) {
		CheckList = checkList;
	}

	public CheckListTipo getCheckListTipo() {
		return checkListTipo;
	}

	public void setCheckListTipo(CheckListTipo checkListTipo) {
		this.checkListTipo = checkListTipo;
	}

	public CheckListSub getCheckListSub() {
		return checkListSub;
	}

	public void setCheckListSub(CheckListSub checkListSub) {
		this.checkListSub = checkListSub;
	}

	public CheckListDocumento getCheckListDocumento() {
		return checkListDocumento;
	}

	public void setCheckListDocumento(CheckListDocumento checkListDocumento) {
		if(checkListDocumento == null || checkListDocumento.getId() == null) {
			this.checkListDocumento = null;
		}else {
			this.checkListDocumento = checkListDocumento;
		}
	}

	public DocumentoJuridico getDocumento() {
		return documento;
	}

	public void setDocumento(DocumentoJuridico documento) {
		this.documento = documento;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}

