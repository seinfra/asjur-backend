package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.Cargo;
import br.com.seinfra.model.filter.CargoFilter;

public interface CargoRepositoryQuery {

	public Page<Cargo> pequisar(CargoFilter filtro, Pageable pageable);
	
}
