package br.com.seinfra.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_pln_fer", schema = "bdscj")
public class PlanoFerias extends GenericBaseModel<Long>{

	@JsonBackReference("planoFerias")
	@ManyToOne
	@JoinColumn(name = "fnc_id")
	private Funcionario funcionario;
	
	@Column(name = "fer_ano_plano")
	private Integer ano;
	
	@Column(name = "fer_dat_sol")
	private LocalDate dataSolicitacao;

	@Column(name = "fer_dat_ini")
	private LocalDate dataInicial;

	@Column(name = "fer_dat_fim")
	private LocalDate dataFinal;

	@Column(name = "fer_qtd_dia")
	private Integer quantidadeDias;
	
	@Size(max = 300)
	@Column(name = "fer_obs")
	private String observacao;
	
	@Column(name = "fer_dat_sal")
	private LocalDate dataSolicitacaoSaldo;

	@Column(name = "fer_dat_ini_sal")
	private LocalDate dataInicialSaldo;

	@Column(name = "fer_dat_fim_sal")
	private LocalDate dataFinalSaldo;
	
	@Column(name = "fer_qtd_dia_sal")
	private Integer quantidadeDiasSaldo;
	
	@Size(max = 300)
	@Column(name = "fer_obs_sal")
	private String observacaoSaldo;

	@Column(name = "fer_dat_sal_res")
	private LocalDate dataSolicitacaoSaldoRestante;

	@Column(name = "fer_dat_ini_sal_res")
	private LocalDate dataInicialSaldoRestante;

	@Column(name = "fer_dat_fim_sal_res")
	private LocalDate dataFinalSaldoRestante;
	
	@Column(name = "fer_qtd_dia_sal_res")
	private Integer quantidadeDiasSaldoRestante;

	@Size(max = 300)
	@Column(name = "fer_obs_sal_res")
	private String observacaoSaldoRestante;
	
	@Column(name = "fer_stt_fer")
	private String statusFerias;
	
	@Column(name = "fer_stt_sld")
	private String statusFeriasSaldo;
	
	@Column(name = "fer_stt_res")
	private String statusFeriasSaldoRestante;
	
	@Transient
	private Boolean ferias = Boolean.FALSE;
	
	@Transient
	private Boolean feriasSaldo = Boolean.FALSE;
	
	@Transient
	private Boolean feriasSaldoRestante = Boolean.FALSE;

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public LocalDate getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(LocalDate dataInicial) {
		this.dataInicial = dataInicial;
	}

	public LocalDate getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(LocalDate dataFinal) {
		this.dataFinal = dataFinal;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getQuantidadeDias() {
		return quantidadeDias;
	}

	public void setQuantidadeDias(Integer quantidadeDias) {
		this.quantidadeDias = quantidadeDias;
	}

	public Integer getQuantidadeDiasSaldo() {
		return quantidadeDiasSaldo;
	}

	public void setQuantidadeDiasSaldo(Integer quantidadeDiasSaldo) {
		this.quantidadeDiasSaldo = quantidadeDiasSaldo;
	}

	public LocalDate getDataInicialSaldo() {
		return dataInicialSaldo;
	}

	public void setDataInicialSaldo(LocalDate dataInicialSaldo) {
		this.dataInicialSaldo = dataInicialSaldo;
	}

	public LocalDate getDataFinalSaldo() {
		return dataFinalSaldo;
	}

	public void setDataFinalSaldo(LocalDate dataFinalSaldo) {
		this.dataFinalSaldo = dataFinalSaldo;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getObservacaoSaldo() {
		return observacaoSaldo;
	}

	public void setObservacaoSaldo(String observacaoSaldo) {
		this.observacaoSaldo = observacaoSaldo;
	}

	public String getStatusFerias() {
		return statusFerias;
	}

	public void setStatusFerias(String statusFerias) {
		this.statusFerias = statusFerias;
	}

	public LocalDate getDataSolicitacao() {
		return dataSolicitacao;
	}

	public void setDataSolicitacao(LocalDate dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}

	public LocalDate getDataSolicitacaoSaldo() {
		return dataSolicitacaoSaldo;
	}

	public void setDataSolicitacaoSaldo(LocalDate dataSolicitacaoSaldo) {
		this.dataSolicitacaoSaldo = dataSolicitacaoSaldo;
	}

	public Boolean getFerias() {
		return ferias;
	}

	public void setFerias(Boolean ferias) {
		this.ferias = ferias;
	}

	public Boolean getFeriasSaldo() {
		return feriasSaldo;
	}

	public void setFeriasSaldo(Boolean feriasSaldo) {
		this.feriasSaldo = feriasSaldo;
	}

	public Boolean getFeriasSaldoRestante() {
		return feriasSaldoRestante;
	}

	public void setFeriasSaldoRestante(Boolean feriasSaldoRestante) {
		this.feriasSaldoRestante = feriasSaldoRestante;
	}

	public String getObservacaoSaldoRestante() {
		return observacaoSaldoRestante;
	}

	public void setObservacaoSaldoRestante(String observacaoSaldoRestante) {
		this.observacaoSaldoRestante = observacaoSaldoRestante;
	}

	public LocalDate getDataSolicitacaoSaldoRestante() {
		return dataSolicitacaoSaldoRestante;
	}

	public void setDataSolicitacaoSaldoRestante(LocalDate dataSolicitacaoSaldoRestante) {
		this.dataSolicitacaoSaldoRestante = dataSolicitacaoSaldoRestante;
	}

	public LocalDate getDataInicialSaldoRestante() {
		return dataInicialSaldoRestante;
	}

	public void setDataInicialSaldoRestante(LocalDate dataInicialSaldoRestante) {
		this.dataInicialSaldoRestante = dataInicialSaldoRestante;
	}

	public LocalDate getDataFinalSaldoRestante() {
		return dataFinalSaldoRestante;
	}

	public void setDataFinalSaldoRestante(LocalDate dataFinalSaldoRestante) {
		this.dataFinalSaldoRestante = dataFinalSaldoRestante;
	}

	public Integer getQuantidadeDiasSaldoRestante() {
		return quantidadeDiasSaldoRestante;
	}

	public void setQuantidadeDiasSaldoRestante(Integer quantidadeDiasSaldoRestante) {
		this.quantidadeDiasSaldoRestante = quantidadeDiasSaldoRestante;
	}

	public String getStatusFeriasSaldo() {
		return statusFeriasSaldo;
	}

	public void setStatusFeriasSaldo(String statusFeriasSaldo) {
		this.statusFeriasSaldo = statusFeriasSaldo;
	}

	public String getStatusFeriasSaldoRestante() {
		return statusFeriasSaldoRestante;
	}

	public void setStatusFeriasSaldoRestante(String statusFeriasSaldoRestante) {
		this.statusFeriasSaldoRestante = statusFeriasSaldoRestante;
	}

}
