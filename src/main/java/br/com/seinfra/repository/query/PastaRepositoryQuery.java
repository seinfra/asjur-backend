package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.Pasta;
import br.com.seinfra.model.filter.PastaFilter;

public interface PastaRepositoryQuery {

	public Page<Pasta> pequisar(PastaFilter filtro, Pageable pageable);

}
