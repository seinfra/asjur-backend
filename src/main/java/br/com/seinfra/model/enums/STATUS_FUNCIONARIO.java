package br.com.seinfra.model.enums;

public enum STATUS_FUNCIONARIO {

	ATIVO("ATIVO"),
	INATIVO("INATIVO");
	
	private final String descricao;
	
	STATUS_FUNCIONARIO(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
