package br.com.seinfra.model.enums;

public enum TIPO_PRIORIDADE {

	ALTISSIMA("ALTISSÍMA"),
	ALTA("ALTA"),
	MEDIA("MÉDIA"),
	BAIXA("BAIXA");
	
	private final String descricao;
	
	TIPO_PRIORIDADE(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
