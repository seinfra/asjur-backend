package br.com.seinfra.other;

import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class Propriedades {
	
	/* CHAVES PARAMETROS */
	public static final String LOGO_CHAVE = "logo";
	
	/* VALORES PARAMETROS */
	public static final String LOGO_VALOR = "imagens/logo.png";

	/* RELATORIOS*/
	public static final String RELATORIO = "/relatorios/";	
	public static final String JASPER = ".jasper";	
	
	
	public JasperPrint verificaDados(List<?> dados, Map<String, Object> parametros, String report) throws JRException {
		if(dados.size() == 0) {
			report = "relatorio-geral";
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(this.getClass().getResourceAsStream(RELATORIO + report + JASPER), 
																parametros, new JRBeanCollectionDataSource(dados));
		return jasperPrint;
	}
}