package br.com.seinfra.model.enums;

public enum TIPO_STATUS_DOCUMENTO {

	DOCUMENTACAO_EM_ANALISE,
	PENDENCIA_DE_DOCUMENTO,
	AGUARDANDO_EMPENHO_E_PGTO;

	public String getNomel() {
		return toString().replaceAll("_", " ");
	}
}
