package br.com.seinfra.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_usuario", schema = "bdadm")
public class Usuario extends GenericBaseModel<Long> {

	@NotNull
	@Size(min = 0, max = 100)
	@Column(name = "nome")
	private String nome;
	
	@NotNull
	@Size(min = 0, max = 50)
	@Column(name = "login")
	private String login;

	@Size(min = 0, max = 100)
	@Column(name = "email")
	private String email;

	@NotNull
	@Size(min = 0, max = 100)	
	@Column(name = "senha")
	private String senha;
	
	@NotNull
	@Size(max = 14)
	@Column(name = "cpf")
	private String cpf;

	@ManyToOne
	@JoinColumn(name = "perfil_id")
	private Perfil perfil;

	@JsonManagedReference("permissoes")
	@OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<UsuarioPermissao> permissoes = new HashSet<>();
	
	public Usuario() {
		super();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		if(perfil == null || perfil.getId() == null) {
			this.perfil = null;
		}else {
			this.perfil = perfil;
		}
	}

	public Set<UsuarioPermissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Set<UsuarioPermissao> permissoes) {
		if(permissoes != null) {
			this.permissoes.clear();
			this.permissoes.addAll(permissoes);
		}else {
			this.permissoes = permissoes;
		}
	}

}
