package br.com.seinfra.repository.query;

import br.com.seinfra.model.OrgaoFuncionario;

public interface OrgaoFuncionarioRepositoryQuery {
	
	public OrgaoFuncionario findByOrgaoAndFuncionario(String orgao, String funcionario);

}
