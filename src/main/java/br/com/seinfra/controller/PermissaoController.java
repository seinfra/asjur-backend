package br.com.seinfra.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.Permissao;
import br.com.seinfra.model.filter.PermissaoFilter;
import br.com.seinfra.service.PermissaoService;

@RestController
@RequestMapping("/asjur-api/permissao")

public class PermissaoController extends GenericBaseController<PermissaoService, Permissao, Long> {
	
	@GetMapping	
	public Page<Permissao> pequisar(PermissaoFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
}
