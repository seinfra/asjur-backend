package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Funcionario;
import br.com.seinfra.repository.query.FuncionarioRepositoryQuery;

@Repository
public interface FuncionarioRepository extends JpaRepository<Funcionario, Long>, FuncionarioRepositoryQuery {

	public Funcionario findByCpf(String cpf);
}
