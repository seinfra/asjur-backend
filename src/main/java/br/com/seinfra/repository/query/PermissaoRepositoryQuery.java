package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.Permissao;
import br.com.seinfra.model.filter.PermissaoFilter;

public interface PermissaoRepositoryQuery {

	public Page<Permissao> pequisar(PermissaoFilter filtro, Pageable pageable);
	
}
