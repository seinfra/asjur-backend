package br.com.seinfra.service;

import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.OrgaoFuncionario;
import br.com.seinfra.repository.OrgaoFuncionarioRepository;

@Service
public class OrgaoFuncionarioService extends GenericBaseService<OrgaoFuncionarioRepository, OrgaoFuncionario, Long>{
	
	public OrgaoFuncionario buscarPorOrgaoFuncionario(String orgao, String funcionario) {
		return repositorio.findByOrgaoAndFuncionario(orgao, funcionario);
	}
}
