/*
 * 21/07/2009
 */
package br.com.seinfra.model.filter;

public class ArmarioFilter {

	private Long id;
	
    private String nome;
    
    private String prateleira;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPrateleira() {
		return prateleira;
	}

	public void setPrateleira(String prateleira) {
		this.prateleira = prateleira;
	}

}
