package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Orgao;
import br.com.seinfra.repository.query.OrgaoRepositoryQuery;

@Repository
public interface OrgaoRepository extends JpaRepository<Orgao, Long>, OrgaoRepositoryQuery {

	public Orgao findByNome(String nome);
}
