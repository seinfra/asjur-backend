package br.com.seinfra.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.UnidadeAdministrativa;
import br.com.seinfra.model.filter.UnidadeAdministrativaFilter;
import br.com.seinfra.service.UnidadeAdministrativaService;

@RestController
@RequestMapping("/asjur-api/unidade-administrativa-juridico")
public class UnidadeAdministrativaJuridicoController extends GenericBaseController<UnidadeAdministrativaService, UnidadeAdministrativa, Long> {
	
	@GetMapping	
	public Page<UnidadeAdministrativa> pequisar(UnidadeAdministrativaFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
//	@GetMapping("/buscarPorNome")
//	public UnidadeAdministrativa buscarPorNome(String nome){
//		return servico.buscarPorNome(nome);
//	}
	
}
