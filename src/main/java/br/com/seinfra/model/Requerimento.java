 package br.com.seinfra.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_rqm", schema = "bdscj")
public class Requerimento extends GenericBaseModel<Long> {

	@JsonBackReference("requerimentos")
    @ManyToOne
    @JoinColumn(name = "fnc_id")
    private Funcionario funcionario;
    
    @Size(max = 500)
    @Column(name = "rqm_obs")
    private String observacao;
    
    @Column(name = "rqm_dat_ini")
	private LocalDate dataInicial;

	@Column(name = "rqm_dat_fim")
	private LocalDate dataFinal;
	
	@ManyToOne
	@JoinColumn(name = "ass_id")
	private Assunto assunto;

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

	public LocalDate getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(LocalDate dataInicial) {
		this.dataInicial = dataInicial;
	}

	public LocalDate getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(LocalDate dataFinal) {
		this.dataFinal = dataFinal;
	}

	public Assunto getAssunto() {
		return assunto;
	}

	public void setAssunto(Assunto assunto) {
		this.assunto = assunto;
	}

}
