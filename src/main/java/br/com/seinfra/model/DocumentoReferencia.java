package br.com.seinfra.model;

import br.com.seinfra.global.model.GenericBaseModel;

public class DocumentoReferencia extends GenericBaseModel<Long> {

	private Long posicao;
	private String numeroDocumento;
	private String processo;
	private String dataConclusao;
	private String data;
	private String referencia;
	private String descricao;
	private String detalhe;
	private String tramitacao;
	private Long diasTotal;

	public Long getPosicao() {
		return posicao;
	}

	public void setPosicao(Long posicao) {
		this.posicao = posicao;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getProcesso() {
		return processo;
	}

	public void setProcesso(String processo) {
		this.processo = processo;
	}

	public String getDataConclusao() {
		return dataConclusao;
	}

	public void setDataConclusao(String dataConclusao) {
		this.dataConclusao = dataConclusao;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDetalhe() {
		return detalhe;
	}

	public void setDetalhe(String detalhe) {
		this.detalhe = detalhe;
	}

	public String getTramitacao() {
		return tramitacao;
	}

	public void setTramitacao(String tramitacao) {
		this.tramitacao = tramitacao;
	}

	public Long getDiasTotal() {
		return diasTotal;
	}

	public void setDiasTotal(Long diasTotal) {
		this.diasTotal = diasTotal;
	}

}
