package br.com.seinfra.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Processo;
import br.com.seinfra.model.filter.ProcessoFilter;
import br.com.seinfra.repository.ProcessoRepository;

@Service
public class ProcessoService extends GenericBaseService<ProcessoRepository, Processo, Long>{
	
	public Page<Processo> pequisar(ProcessoFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
}
