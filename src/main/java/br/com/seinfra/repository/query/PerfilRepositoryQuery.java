package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.Perfil;
import br.com.seinfra.model.filter.PerfilFilter;

public interface PerfilRepositoryQuery {

	public Page<Perfil> pequisar(PerfilFilter filtro, Pageable pageable);
	
}
