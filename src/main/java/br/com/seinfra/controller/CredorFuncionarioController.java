package br.com.seinfra.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.CredorFuncionario;
import br.com.seinfra.model.filter.CredorFuncionarioFilter;
import br.com.seinfra.service.CredorFuncionarioService;

@RestController
@RequestMapping("/asjur-api/credor-funcionario")
public class CredorFuncionarioController extends GenericBaseController<CredorFuncionarioService, CredorFuncionario, Long> {
	
	@GetMapping
	public CredorFuncionario buscarPorCredorFuncionario(CredorFuncionarioFilter filter) {
		return servico.buscarPorCredorFuncionario(filter.getCredor(), filter.getFuncionario());
	}
}