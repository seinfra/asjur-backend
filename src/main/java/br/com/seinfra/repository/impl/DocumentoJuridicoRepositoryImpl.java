package br.com.seinfra.repository.impl;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.seinfra.model.Cargo;
import br.com.seinfra.model.DocumentoJuridico;
import br.com.seinfra.model.Funcionario;
import br.com.seinfra.model.Parecer;
import br.com.seinfra.model.dto.DocumentoForm;
import br.com.seinfra.model.filter.DocumentoJuridicoFilter;
import br.com.seinfra.repository.query.DocumentoJuridicoRepositoryQuery;

public class DocumentoJuridicoRepositoryImpl implements DocumentoJuridicoRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<DocumentoJuridico> pequisar(DocumentoJuridicoFilter filtro, Pageable pageable) {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.desc(root.get("id")));

		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);

		return new PageImpl<>(query.getResultList(), pageable, total(filtro));
	}

	private Predicate[] criarRestricoes(DocumentoJuridicoFilter filtro, CriteriaBuilder builder,
			Root<DocumentoJuridico> root) {

		List<Predicate> predicates = new ArrayList<>();

		if(filtro.getId() != null) {			
			predicates.add(builder.equal(root.get("id"), filtro.getId()));
		}
		
		if(filtro.getFuncionario() != null) {
			if(filtro.getFuncionario().getCargo().getNome().equals("ADVOGADO")) {
				predicates.add(builder.equal(root.get("funcionario"), filtro.getFuncionario()));
			}
		}
		
		if (!StringUtils.isEmpty(filtro.getNumeroDocumento())) {
			predicates.add(builder.like(builder.lower(root.get("numeroDocumento")),
					"%" + filtro.getNumeroDocumento().toLowerCase() + "%"));
		}	
		
		if (!StringUtils.isEmpty(filtro.getNumeroProcesso())) {
			predicates.add(builder.like(builder.lower(root.get("numeroProcesso")),
					"%" + filtro.getNumeroProcesso().toLowerCase() + "%"));
		}			

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}

	private Long total(DocumentoJuridicoFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	
	public List<DocumentoJuridico> documentoPorUsuario(Funcionario funcionario) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
		
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if(funcionario != null) {
			if(funcionario.getCargo().getNome().equals("ADVOGADO")) {
				predicates.add(builder.equal(root.get("funcionario"), funcionario));
			}
		}
		predicates.add(builder.or(builder.notEqual(root.get("status"), "Processo finalizado"), root.get("status").isNull()));
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		criteria.orderBy(builder.desc(root.get("dataDoc")));

		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		List<DocumentoJuridico> documentos = new ArrayList<>();
		
		for(DocumentoJuridico documento : query.getResultList()) {
			if(documento.getDataEnvioRecebimento() != null && documento.getDataFimPrazo() != null) {
				documento.setPrazo(ChronoUnit.DAYS.between(LocalDate.now(), documento.getDataFimPrazo()));
				documentos.add(documento);
			}
		}
		return documentos;
	}
	
	public List<DocumentoJuridico> documentoPorFuncionario(DocumentoJuridico documento, List<Funcionario> funcionarios, List<Funcionario> funcionariosAfastados) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
		Join<DocumentoJuridico, Funcionario> funcionarioJoin = root.join("funcionario");
		Join<Funcionario, Cargo> cargoJoin = funcionarioJoin.join("cargo");
		
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if(funcionarios.size() > 0) {
			predicates.add(funcionarioJoin.in(funcionarios).not());
		}
		if(funcionariosAfastados.size() > 0) {
			predicates.add(funcionarioJoin.in(funcionariosAfastados).not());
		}
		if(documento.getOrgao() != null && documento.getOrgao().getId() != null) {
			predicates.add(builder.equal(root.get("orgao"), documento.getOrgao()));
		}
			predicates.add(builder.equal(root.get("ugb"), documento.getUgb()));
			predicates.add(builder.equal(root.get("prioridade"), documento.getPrioridade()));
			predicates.add(builder.equal(cargoJoin.get("nome"), "ADVOGADO"));
			predicates.add(root.get("dataConclusao").isNull());
		
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		criteria.orderBy(builder.desc(root.get("id")));

		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<DocumentoJuridico> proximoDocumento(DocumentoJuridico documento, Funcionario funcionario) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
		Join<DocumentoJuridico, Funcionario> funcionarioJoin = root.join("funcionario");
		Join<Funcionario, Cargo> cargoJoin = funcionarioJoin.join("cargo");
		
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if(documento.getOrgao() != null && documento.getOrgao().getId() != null) {
			predicates.add(builder.equal(root.get("orgao"), documento.getOrgao()));
		}
			predicates.add(builder.equal(root.get("ugb"), documento.getUgb()));
			predicates.add(builder.equal(root.get("funcionario"), funcionario));
			predicates.add(builder.equal(root.get("prioridade"), documento.getPrioridade()));
			predicates.add(builder.equal(cargoJoin.get("nome"), "ADVOGADO"));
			predicates.add(root.get("dataConclusao").isNull());
		
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		criteria.orderBy(builder.desc(root.get("id")));

		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<DocumentoJuridico> documentoParaAnaliseCoordenador() {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
		
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		predicates.add(root.get("dataConclusao").isNotNull());
		predicates.add(root.get("dataConclusaoReal").isNull());
	
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		criteria.orderBy(builder.desc(root.get("dataConclusao")));

		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<DocumentoJuridico> inicioDistribuicao() {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
		
		criteria.where(root.get("funcionario").isNull());
		criteria.orderBy(builder.desc(root.get("id")));

		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<DocumentoJuridico> documentoParaCheckList(String cpf) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
		Join<DocumentoJuridico, Funcionario> funcionarioJoin = root.join("funcionario");
		
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		predicates.add(root.get("dataConclusao").isNull());
		predicates.add(root.get("dataConclusaoReal").isNull());
		predicates.add(builder.equal(funcionarioJoin.get("cpf"), cpf));
		predicates.add(builder.or(builder.equal(root.get("status"), "Aguardando checklist"), builder.equal(root.get("status"), "Aguardando checklist/Pendência"), root.get("status").isNull()));
	
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		criteria.orderBy(builder.desc(root.get("dataConclusao")));

		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<DocumentoJuridico> documentoFinalizar() {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
		
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		predicates.add(root.get("dataConclusao").isNotNull());
		predicates.add(root.get("dataConclusaoReal").isNotNull());
		predicates.add(builder.or(builder.notEqual(root.get("status"), "Processo finalizado"), root.get("status").isNull()));
	
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		criteria.orderBy(builder.desc(root.get("dataConclusao")));

		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<DocumentoJuridico> documentoPorFuncionarioReceber(String cpf) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
		Join<DocumentoJuridico, Funcionario> funcionarioJoin = root.join("funcionario");

		criteria.where(builder.equal(funcionarioJoin.get("cpf"), cpf),	
				                     root.get("dataRecebimentoAdvogado").isNull());
		criteria.orderBy(builder.desc(root.get("dataDoc")));

		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<Parecer> relatorioControPrazo(LocalDate inicio, LocalDate fim, String ugb) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Parecer> criteria = builder.createQuery(Parecer.class);
		Root<Parecer> root = criteria.from(Parecer.class);
		Join<Parecer, DocumentoJuridico> joinDocumento = root.join("documento");
		
		List<Predicate> predicates = new ArrayList<>();

		if(ugb != null && ugb != "") {			
			predicates.add(builder.equal(joinDocumento.get("ugb"), ugb));
		}
		if(inicio != null) {
			predicates.add(builder.between(joinDocumento.get("dataInicioPrazo"), inicio, fim));
		}
		if(fim != null) {
			predicates.add(builder.between(joinDocumento.get("dataFimPrazo"), inicio, fim));
		}
		
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		criteria.orderBy(builder.desc(joinDocumento.get("dataInicioPrazo")));
		TypedQuery<Parecer> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<Parecer> relatorioControEntrega(LocalDate inicio, LocalDate fim, String id) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Parecer> criteria = builder.createQuery(Parecer.class);
		Root<Parecer> root = criteria.from(Parecer.class);
		Join<Parecer, DocumentoJuridico> joinDocumento = root.join("documento");
		
		List<Predicate> predicates = new ArrayList<>();

		if(id != null && id != "") {			
			predicates.add(builder.equal(joinDocumento.get("funcionario"), new Funcionario(Long.valueOf(id))));
		}
		if(inicio != null) {
			predicates.add(builder.between(joinDocumento.get("dataEnvioRecebimento"), inicio, fim));
		}
		
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		criteria.orderBy(builder.desc(joinDocumento.get("dataEnvioRecebimento")));
		TypedQuery<Parecer> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<DocumentoJuridico> relatorioControJuridicoPorAdvogado(DocumentoForm documentoForm) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
		Join<DocumentoJuridico, Funcionario> joinFuncionario = root.join("funcionario");
		
		Subquery<Long> subAtraso = criteria.subquery(Long.class);
		Root<DocumentoJuridico> rootAtraso = subAtraso.from(DocumentoJuridico.class);

		subAtraso.select(builder.count(rootAtraso.get("prazo")));
		subAtraso.where(builder.equal(rootAtraso.get("funcionario"), root.get("funcionario")),
				        builder.equal(rootAtraso.get("prioridade"), root.get("prioridade")), 
				        builder.lessThan(rootAtraso.get("dataFimPrazo"), rootAtraso.get("dataConclusaoReal")));
		
		Subquery<Long> subPrazo = criteria.subquery(Long.class);
		Root<DocumentoJuridico> rootPrazo = subPrazo.from(DocumentoJuridico.class);

		subPrazo.select(builder.count(rootPrazo.get("prazo")));
		subPrazo.where(builder.equal(rootPrazo.get("funcionario"), root.get("funcionario")),
				        builder.equal(rootPrazo.get("prioridade"), root.get("prioridade")), 
				        builder.greaterThanOrEqualTo(rootPrazo.get("dataFimPrazo"), rootPrazo.get("dataConclusaoReal")));
		
		criteria.multiselect(builder.count(root.get("id")), root.get("funcionario"), root.get("prioridade"), 
				subPrazo.getSelection(), subAtraso.getSelection()).distinct(true);

		List<Predicate> predicates = new ArrayList<>();

		if(documentoForm.getUgb() != null && documentoForm.getUgb() != "") {			
			predicates.add(builder.equal(root.get("ugb"), documentoForm.getUgb()));
		}
		if(documentoForm.getAno() != null && documentoForm.getAno() != "") {
			predicates.add(builder.equal(builder.function("YEAR", Integer.class, root.get("dataDoc")), Integer.valueOf(documentoForm.getAno())));
		}
		if(documentoForm.getMes() != null && documentoForm.getMes() != "") {
			predicates.add(builder.equal(builder.function("MONTH", Integer.class, root.get("dataDoc")), Integer.valueOf(documentoForm.getMes())));
		}
		if(documentoForm.getPrioridade() != null && documentoForm.getPrioridade() != "") {
			predicates.add(builder.equal(root.get("prioridade"), documentoForm.getPrioridade()));
		}		
		if(documentoForm.getId() != null && documentoForm.getId() != "") {
			predicates.add(builder.equal(joinFuncionario.get("id"), Long.valueOf(documentoForm.getId())));
		}
		predicates.add(root.get("dataConclusaoReal").isNotNull());
		
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		criteria.groupBy(root.get("funcionario"), root.get("prioridade"));
		criteria.orderBy(builder.asc(root.get("prioridade")));
		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<DocumentoJuridico> relatorioControJuridicoPorComplexidade(DocumentoForm documentoForm) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
		Join<DocumentoJuridico, Funcionario> joinFuncionario = root.join("funcionario");
		
		criteria.multiselect(builder.count(root.get("id")), root.get("prioridade"),
				             builder.function("MONTH", Integer.class, root.get("dataDoc"))).distinct(true);
		
		CriteriaBuilder builderPrazo = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteriaPrazo = builderPrazo.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> rootPrazo = criteriaPrazo.from(DocumentoJuridico.class);
		Join<DocumentoJuridico, Funcionario> joinFuncionarioPrazo = rootPrazo.join("funcionario");

		criteriaPrazo.multiselect(builderPrazo.count(rootPrazo.get("id")), rootPrazo.get("prioridade"),
				builderPrazo.function("MONTH", Integer.class, rootPrazo.get("dataDoc"))).distinct(true);
		
		CriteriaBuilder builderAtraso = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteriaAtraso = builderAtraso.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> rootAtraso = criteriaAtraso.from(DocumentoJuridico.class);
		Join<DocumentoJuridico, Funcionario> joinFuncionarioAtraso = rootAtraso.join("funcionario");

		criteriaAtraso.multiselect(builderAtraso.count(rootAtraso.get("id")), rootAtraso.get("prioridade"),
				builderAtraso.function("MONTH", Integer.class, rootAtraso.get("dataDoc"))).distinct(true);
		
		List<Predicate> predicates = new ArrayList<>();
		List<Predicate> predicates2 = new ArrayList<>();
		List<Predicate> predicates3 = new ArrayList<>();

		if(documentoForm.getUgb() != null && documentoForm.getUgb() != "") {			
			predicates.add(builder.equal(root.get("ugb"), documentoForm.getUgb()));
			predicates2.add(builderAtraso.equal(rootAtraso.get("ugb"), documentoForm.getUgb()));
			predicates3.add(builderPrazo.equal(rootPrazo.get("ugb"), documentoForm.getUgb()));
		}
		if(documentoForm.getAno() != null && documentoForm.getAno() != "") {
			predicates.add(builder.equal(builder.function("YEAR", Integer.class, root.get("dataDoc")), Integer.valueOf(documentoForm.getAno())));
			predicates2.add(builderAtraso.equal(builderAtraso.function("YEAR", Integer.class, rootAtraso.get("dataDoc")), Integer.valueOf(documentoForm.getAno())));
			predicates3.add(builderPrazo.equal(builderPrazo.function("YEAR", Integer.class, rootPrazo.get("dataDoc")), Integer.valueOf(documentoForm.getAno())));
		}
		if(documentoForm.getMes() != null && documentoForm.getMes() != "") {
			predicates.add(builder.equal(builder.function("MONTH", Integer.class, root.get("dataDoc")), Integer.valueOf(documentoForm.getMes())));
			predicates2.add(builderAtraso.equal(builderAtraso.function("MONTH", Integer.class, rootAtraso.get("dataDoc")), Integer.valueOf(documentoForm.getMes())));
			predicates3.add(builderPrazo.equal(builderPrazo.function("MONTH", Integer.class, rootPrazo.get("dataDoc")), Integer.valueOf(documentoForm.getMes())));
		}
		if(documentoForm.getPrioridade() != null && documentoForm.getPrioridade() != "") {
			predicates.add(builder.equal(root.get("prioridade"), documentoForm.getPrioridade()));
			predicates2.add(builderAtraso.equal(rootAtraso.get("prioridade"), documentoForm.getPrioridade()));
			predicates3.add(builderPrazo.equal(rootPrazo.get("prioridade"), documentoForm.getPrioridade()));
		}		
		if(documentoForm.getId() != null && documentoForm.getId() != "") {
			predicates.add(builder.equal(joinFuncionario.get("id"), Long.valueOf(documentoForm.getId())));
			predicates2.add(builderAtraso.equal(joinFuncionarioAtraso.get("id"), Long.valueOf(documentoForm.getId())));
			predicates3.add(builderPrazo.equal(joinFuncionarioPrazo.get("id"), Long.valueOf(documentoForm.getId())));
		}
		predicates.add(root.get("dataConclusaoReal").isNotNull());
		predicates2.add(rootAtraso.get("dataConclusaoReal").isNotNull());
		predicates2.add(builderAtraso.lessThan(rootAtraso.get("dataFimPrazo"), rootAtraso.get("dataConclusaoReal")));
		predicates3.add(rootPrazo.get("dataConclusaoReal").isNotNull());
		predicates3.add(builderPrazo.greaterThanOrEqualTo(rootPrazo.get("dataFimPrazo"), rootPrazo.get("dataConclusaoReal")));
		
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		criteria.groupBy(root.get("prioridade"), builder.function("MONTH", Integer.class, root.get("dataDoc")));
		criteria.orderBy(builder.asc(root.get("prioridade")), builder.asc(builder.function("MONTH", Integer.class, root.get("dataDoc"))));
		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		
		criteriaAtraso.where(predicates2.toArray(new Predicate[predicates2.size()]));
		criteriaAtraso.groupBy(rootAtraso.get("prioridade"), builderAtraso.function("MONTH", Integer.class, rootAtraso.get("dataDoc")));
		criteriaAtraso.orderBy(builderAtraso.asc(rootAtraso.get("prioridade")), builderAtraso.asc(builderAtraso.function("MONTH", Integer.class, rootAtraso.get("dataDoc"))));
		TypedQuery<DocumentoJuridico> queryAtraso = manager.createQuery(criteriaAtraso);
		
		criteriaPrazo.where(predicates3.toArray(new Predicate[predicates3.size()]));
		criteriaPrazo.groupBy(rootPrazo.get("prioridade"), builderPrazo.function("MONTH", Integer.class, rootPrazo.get("dataDoc")));
		criteriaPrazo.orderBy(builderPrazo.asc(rootPrazo.get("prioridade")), builderPrazo.asc(builderPrazo.function("MONTH", Integer.class, rootPrazo.get("dataDoc"))));
		TypedQuery<DocumentoJuridico> queryPrazo = manager.createQuery(criteriaPrazo);
		
		List<DocumentoJuridico> listaDocumentos = new ArrayList<>();
		
		for(DocumentoJuridico  documento : query.getResultList()) {
			for(DocumentoJuridico documentoPrazo : queryPrazo.getResultList()) {
				if(documento.getPrioridade().equals(documentoPrazo.getPrioridade()) && documento.getMes().equals(documentoPrazo.getMes())) {
					documento.setPrazo(documentoPrazo.getId());
				}
			}
			for(DocumentoJuridico documentoAtraso : queryAtraso.getResultList()) {
				if(documento.getPrioridade().equals(documentoAtraso.getPrioridade()) && documento.getMes().equals(documentoAtraso.getMes())) {
					documento.setAtraso(documentoAtraso.getId());
				}
			}
			listaDocumentos.add(documento);
		}
		return listaDocumentos;
	}
	
	public List<DocumentoJuridico> relatorioControJuridicoPorAdvogadoResumo(DocumentoForm documentoForm) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
		
		Subquery<Long> subAtraso = criteria.subquery(Long.class);
		Root<DocumentoJuridico> rootAtraso = subAtraso.from(DocumentoJuridico.class);

		subAtraso.select(builder.count(rootAtraso.get("prazo")));
		subAtraso.where(builder.equal(rootAtraso.get("funcionario"), root.get("funcionario")),
				        builder.lessThan(rootAtraso.get("dataFimPrazo"), rootAtraso.get("dataConclusaoReal")));
		
		Subquery<Long> subPrazo = criteria.subquery(Long.class);
		Root<DocumentoJuridico> rootPrazo = subPrazo.from(DocumentoJuridico.class);

		subPrazo.select(builder.count(rootPrazo.get("prazo")));
		subPrazo.where(builder.equal(rootPrazo.get("funcionario"), root.get("funcionario")),
				        builder.greaterThanOrEqualTo(rootPrazo.get("dataFimPrazo"), rootPrazo.get("dataConclusaoReal")));
		
		criteria.multiselect(builder.count(root.get("id")), root.get("funcionario"), 
				subPrazo.getSelection(), subAtraso.getSelection()).distinct(true);

		List<Predicate> predicates = new ArrayList<>();

		if(documentoForm.getUgb() != null && documentoForm.getUgb() != "") {			
			predicates.add(builder.equal(root.get("ugb"), documentoForm.getUgb()));
		}
		if(documentoForm.getAno() != null && documentoForm.getAno() != "") {
			predicates.add(builder.equal(builder.function("YEAR", Integer.class, root.get("dataDoc")), Integer.valueOf(documentoForm.getAno())));
		}
		predicates.add(root.get("dataConclusaoReal").isNotNull());
		
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		criteria.groupBy(root.get("funcionario"));
		criteria.orderBy(builder.asc(root.get("funcionario")));
		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<DocumentoJuridico> relatorioControJuridicoPorComplexidadeResumo(DocumentoForm documentoForm) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
		
		criteria.multiselect(builder.count(root.get("id")),
	             builder.function("MONTH", Integer.class, root.get("dataDoc"))).distinct(true);
		
		CriteriaBuilder builderPrazo = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteriaPrazo = builderPrazo.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> rootPrazo = criteriaPrazo.from(DocumentoJuridico.class);

		criteriaPrazo.multiselect(builderPrazo.count(rootPrazo.get("prazo")),
				builderPrazo.function("MONTH", Integer.class, rootPrazo.get("dataDoc"))).distinct(true);
		
		
		CriteriaBuilder builderAtraso = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteriaAtraso = builderAtraso.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> rootAtraso = criteriaAtraso.from(DocumentoJuridico.class);

		criteriaAtraso.multiselect(builderAtraso.count(rootAtraso.get("prazo")),
				builderAtraso.function("MONTH", Integer.class, rootAtraso.get("dataDoc"))).distinct(true);
		
		List<Predicate> predicates = new ArrayList<>();
		List<Predicate> predicates2 = new ArrayList<>();
		List<Predicate> predicates3 = new ArrayList<>();

		if(documentoForm.getUgb() != null && documentoForm.getUgb() != "") {			
			predicates.add(builder.equal(root.get("ugb"), documentoForm.getUgb()));
			predicates2.add(builderAtraso.equal(rootAtraso.get("ugb"), documentoForm.getUgb()));
			predicates3.add(builderPrazo.equal(rootPrazo.get("ugb"), documentoForm.getUgb()));
		}
		if(documentoForm.getAno() != null && documentoForm.getAno() != "") {
			predicates.add(builder.equal(builder.function("YEAR", Integer.class, root.get("dataDoc")), Integer.valueOf(documentoForm.getAno())));
			predicates2.add(builderAtraso.equal(builderAtraso.function("YEAR", Integer.class, rootAtraso.get("dataDoc")), Integer.valueOf(documentoForm.getAno())));
			predicates3.add(builderPrazo.equal(builderPrazo.function("YEAR", Integer.class, rootPrazo.get("dataDoc")), Integer.valueOf(documentoForm.getAno())));
		}
		
		predicates.add(root.get("dataConclusaoReal").isNotNull());
		predicates2.add(rootAtraso.get("dataConclusaoReal").isNotNull());
		predicates2.add(builderAtraso.lessThan(rootAtraso.get("dataFimPrazo"), rootAtraso.get("dataConclusaoReal")));
		predicates3.add(rootPrazo.get("dataConclusaoReal").isNotNull());
		predicates3.add(builderPrazo.greaterThanOrEqualTo(rootPrazo.get("dataFimPrazo"), rootPrazo.get("dataConclusaoReal")));
		
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		criteria.groupBy(builder.function("MONTH", Integer.class, root.get("dataDoc")));
		criteria.orderBy(builder.asc(builder.function("MONTH", Integer.class, root.get("dataDoc"))));
		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		
		criteriaAtraso.where(predicates2.toArray(new Predicate[predicates2.size()]));
		criteriaAtraso.groupBy(builderAtraso.function("MONTH", Integer.class, rootAtraso.get("dataDoc")));
		criteriaAtraso.orderBy(builderAtraso.asc(builderAtraso.function("MONTH", Integer.class, rootAtraso.get("dataDoc"))));
		TypedQuery<DocumentoJuridico> queryAtraso = manager.createQuery(criteriaAtraso);
		
		criteriaPrazo.where(predicates3.toArray(new Predicate[predicates3.size()]));
		criteriaPrazo.groupBy(builderPrazo.function("MONTH", Integer.class, rootPrazo.get("dataDoc")));
		criteriaPrazo.orderBy(builderPrazo.asc(builderPrazo.function("MONTH", Integer.class, rootPrazo.get("dataDoc"))));
		TypedQuery<DocumentoJuridico> queryPrazo = manager.createQuery(criteriaPrazo);
		
		List<DocumentoJuridico> listaDocumentos = new ArrayList<>();
		
		for(DocumentoJuridico  documento : query.getResultList()) {
			for(DocumentoJuridico documentoPrazo : queryPrazo.getResultList()) {
				if(documento.getMes() == documentoPrazo.getMes()) {
					documento.setPrazo(documentoPrazo.getId());
				}
			}
			for(DocumentoJuridico documentoAtraso : queryAtraso.getResultList()) {
				if(documento.getMes() == documentoAtraso.getMes()) {
					documento.setAtraso(documentoAtraso.getId());
				}
			}
			listaDocumentos.add(documento);
		}
		return listaDocumentos;
	}
	
	public List<DocumentoJuridico> documentoFiltro(DocumentoJuridico documento) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);

		List<Predicate> predicates = new ArrayList<>();

		if (!StringUtils.isEmpty(documento.getNumeroDocumento())) {
			predicates.add(builder.equal(root.get("numeroDocumento"), documento.getNumeroDocumento()));
		}
		if (!StringUtils.isEmpty(documento.getNumeroProcesso())) {
			predicates.add(builder.equal(root.get("numeroProcesso"), documento.getNumeroProcesso()));
		}
		
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));

		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<DocumentoJuridico> estatisticaAdvogado(String prazo) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
	
		criteria.multiselect(builder.count(root.get("id")), root.get("funcionario"));

		if(prazo.equals("ATRASO")) {
			criteria.where(builder.greaterThanOrEqualTo(root.get("dataFimPrazo"), root.get("dataConclusaoReal")));
		}else if(prazo.equals("PRAZO")) {
			criteria.where(builder.lessThanOrEqualTo(root.get("dataFimPrazo"), root.get("dataConclusaoReal")));			
		}
		criteria.groupBy(root.get("funcionario"));

		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		List<DocumentoJuridico> documentos = new ArrayList<>();
		
		for(DocumentoJuridico doc : query.getResultList()) {
			doc.getFuncionario().setNome(doc.getFuncionario().getNome().split(" ")[0]);
			documentos.add(doc);
		}
		return documentos;
	}
	
	public List<DocumentoJuridico> estatisticaUgb(String prazo) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
	
		criteria.multiselect(builder.count(root.get("id")), root.get("ugb"));
		if(prazo.equals("ATRASO")) {
			criteria.where(builder.greaterThanOrEqualTo(root.get("dataFimPrazo"), root.get("dataConclusaoReal")));
		}else if(prazo.equals("PRAZO")) {
			criteria.where(builder.lessThanOrEqualTo(root.get("dataFimPrazo"), root.get("dataConclusaoReal")));			
		}
		criteria.groupBy(root.get("ugb"));
		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<DocumentoJuridico> relatorioProcesso(Long id) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
	
		criteria.where(builder.equal(root.get("id"), id));
		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<DocumentoJuridico> envioEmailProcesso() {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<DocumentoJuridico> criteria = builder.createQuery(DocumentoJuridico.class);
		Root<DocumentoJuridico> root = criteria.from(DocumentoJuridico.class);
		
		List<Predicate> predicates = new ArrayList<>();

		predicates.add(builder.or(builder.notEqual(root.get("status"), "Processo finalizado"), root.get("status").isNull()));
		predicates.add(root.get("dataRecebimentoAdvogado").isNotNull());
	
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		TypedQuery<DocumentoJuridico> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
}

