package br.com.seinfra.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Orgao;
import br.com.seinfra.model.filter.OrgaoFilter;
import br.com.seinfra.repository.OrgaoRepository;

@Service
public class OrgaoService  extends GenericBaseService<OrgaoRepository, Orgao, Long>{
	
	public Page<Orgao> pequisar(OrgaoFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public Orgao buscarPorNome(String nome) {
		return repositorio.findByNome(nome);
	}
	
	public Orgao buscarOrgao(Orgao orgao) {
		
		Orgao org = null;
		if(orgao != null) {
			org = buscarPorNome(orgao.getNome());
			if(org == null) {
				orgao.setId(null);
				incluir(orgao);
				org = buscarPorNome(orgao.getNome());
			}
		}
		return org;
	}
	
}
