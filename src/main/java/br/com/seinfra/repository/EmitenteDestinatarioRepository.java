package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.EmitenteDestinatario;
import br.com.seinfra.repository.query.EmitenteDestinatarioRepositoryQuery;

@Repository
public interface EmitenteDestinatarioRepository extends JpaRepository<EmitenteDestinatario, Long>, EmitenteDestinatarioRepositoryQuery {

	public EmitenteDestinatario findByDescricao(String descricao);
}
