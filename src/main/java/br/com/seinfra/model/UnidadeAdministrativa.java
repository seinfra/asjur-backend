package br.com.seinfra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.seinfra.global.model.GenericBaseModel;
import br.com.seinfra.model.enums.NivelHierarquico;

@Entity
@Table( name="tb_uadm", schema = "bdger")
public class UnidadeAdministrativa extends GenericBaseModel<Long>{
	
	@NotNull
	@Size(min = 2, max = 100)
    @Column(name = "uadm_nome", unique = true)
    private String nome;

	@Size(min = 2, max = 20)
    @Column(name = "uadm_sig", unique = true)
    private String sigla;

    @Column(name = "uadm_des", nullable = false, length = 300)
    private String descricao;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "uadm_spr", foreignKey = @ForeignKey(name = "fk_uadm_uadm"))
    @Fetch(FetchMode.JOIN)
    private UnidadeAdministrativa superior;

    @Column(name = "uadm_nvl_hrq")
    @Enumerated(EnumType.STRING)
    private NivelHierarquico nivel;

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public NivelHierarquico getNivel() {
        return nivel;
    }

    public void setNivel(NivelHierarquico nivel) {
        this.nivel = nivel;
    }

    public UnidadeAdministrativa getSuperior() {
        return superior;
    }

    public void setSuperior(UnidadeAdministrativa superior) {
        this.superior = superior;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
