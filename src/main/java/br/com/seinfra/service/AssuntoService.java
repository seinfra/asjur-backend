package br.com.seinfra.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Assunto;
import br.com.seinfra.model.filter.AssuntoFilter;
import br.com.seinfra.repository.AssuntoRepository;

@Service
public class AssuntoService extends GenericBaseService<AssuntoRepository, Assunto	, Long>{
	
	public Page<Assunto> pequisar(AssuntoFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public Assunto buscarPorDescricao(String descricao) {
		return repositorio.findByDescricao(descricao);
	}
	
	public Assunto buscarAssunto(Assunto assunto) {
		
		Assunto ass = null;
		if(assunto != null) {
			ass = buscarPorDescricao(assunto.getDescricao());
			if(ass == null) {
				assunto.setId(null);
				incluir(assunto);
				ass = buscarPorDescricao(assunto.getDescricao());
			}
		}
		return ass;
	}
}
