package br.com.seinfra.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.Assunto;
import br.com.seinfra.model.filter.AssuntoFilter;
import br.com.seinfra.service.AssuntoService;

@RestController
@RequestMapping("/asjur-api/assunto")
public class AssuntoController extends GenericBaseController<AssuntoService, Assunto, Long> {
	
	@GetMapping	
	public Page<Assunto> pequisar(AssuntoFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
	@GetMapping("/buscarPorDescricao")
	public Assunto buscarPorDescricao(String descricao){
		return servico.buscarPorDescricao(descricao);
	}
	
}