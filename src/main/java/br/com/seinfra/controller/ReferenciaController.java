package br.com.seinfra.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.Referencia;
import br.com.seinfra.model.filter.ReferenciaFilter;
import br.com.seinfra.service.ReferenciaService;

@RestController
@RequestMapping("/asjur-api/referencia")
public class ReferenciaController extends GenericBaseController<ReferenciaService, Referencia, Long> {
	
	@GetMapping	
	public Page<Referencia> pequisar(ReferenciaFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
	@GetMapping("/buscarPorDescricao")
	public Referencia buscarPorDescricao(String descricao){
		return servico.buscarPorDescricao(descricao);
	}
	
}