package br.com.seinfra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(schema = "bdger", name = "TB_MUN")
public class Municipio extends GenericBaseModel<Long> {

    @Column(name = "MUN_NOME", length = 100, nullable = false, unique = false)
    private String nome;

    @Column(name = "MUN_COD_IBGE", length = 7)
    private String codigoIBGE;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigoIBGE() {
        return codigoIBGE;
    }

    public void setCodigoIBGE(String codigoIBGE) {
        this.codigoIBGE = codigoIBGE;
    }
}
