package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.Processo;
import br.com.seinfra.model.filter.ProcessoFilter;

public interface ProcessoRepositoryQuery {

	public Page<Processo> pequisar(ProcessoFilter filtro, Pageable pageable);

}
