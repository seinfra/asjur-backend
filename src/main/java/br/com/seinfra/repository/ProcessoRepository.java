package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Processo;
import br.com.seinfra.repository.query.ProcessoRepositoryQuery;

@Repository
public interface ProcessoRepository extends JpaRepository<Processo, Long>, ProcessoRepositoryQuery {

}
