package br.com.seinfra.model.dto;

public class DocumentoForm {
	
	private String tipo;
	
	private String ano;
	
	private String mes;
	
	private String ugb;
	
	private String prioridade;
	
	private String id;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getUgb() {
		return ugb;
	}

	public void setUgb(String ugb) {
		this.ugb = ugb;
	}

	public String getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(String prioridade) {
		this.prioridade = prioridade;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	} 

	
}
