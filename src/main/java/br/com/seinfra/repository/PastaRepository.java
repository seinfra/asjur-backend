package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Pasta;
import br.com.seinfra.repository.query.PastaRepositoryQuery;

@Repository
public interface PastaRepository extends JpaRepository<Pasta, Long>, PastaRepositoryQuery {

	public Pasta findByNome(String nome);
}
