/**
 *
 */
package br.com.seinfra.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_fnc", schema = "bdscj")
public class Funcionario extends GenericBaseModel<Long> {

	@NotNull
	@Size(max = 100)
	@Column(name = "fnc_nome")
	private String nome;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "end_id", foreignKey = @ForeignKey(name = "fk_fnc_end"))
	private Endereco endereco;

	@Column(name = "fnc_dat_nasc")
	private LocalDate dataNascimento;
	
	@Column(name = "fnc_mat")
	private String matricula;

	@NotNull
	@Size(max = 14)
	@Column(name = "fnc_cpf")
	private String cpf;
	
	@Size(max = 20)
	@Column(name = "fnc_rg")
	private String rg;

	@Column(name = "fnc_data_entrada")
	private LocalDate dataEntrada;
	
	@Column(name = "fnc_data_saida")
	private LocalDate dataSaida;
	
    @ManyToOne
    @JoinColumn(name = "cargo_id")
    private Cargo cargo;
    
    @NotNull
    @Column(name = "ugb")
	private String ugb;
    
	@JsonManagedReference("credoresFuncionario")
	@OneToMany(mappedBy = "funcionario", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CredorFuncionario> credoresFuncionario = new ArrayList<>();
    
	@JsonManagedReference("orgaosFuncionario")
	@OneToMany(mappedBy = "funcionario", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<OrgaoFuncionario> orgaosFuncionario = new ArrayList<>();
    
	@JsonManagedReference("contatosFuncionario")
	@OneToMany(mappedBy = "funcionario", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Contato> contatos = new ArrayList<>();
	
	@JsonManagedReference("contatosPessoaisFuncionario")
	@OneToMany(mappedBy = "funcionario", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ContatoPessoal> contatosPessoais = new ArrayList<>();

	@JsonManagedReference("planoFerias")
	@OneToMany(mappedBy = "funcionario", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<PlanoFerias> planoFerias = new ArrayList<>();

	@JsonManagedReference("requerimentos")
	@OneToMany(mappedBy = "funcionario", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Requerimento> requerimentos = new ArrayList<>();
	
	@Transient
	private List<Orgao> orgaos = new ArrayList<>();
	
	@Transient
	private List<Credor> credores = new ArrayList<>();
	
	@Transient
	private PlanoFerias planoFer;
	
	private String status;

	public Funcionario() {
		super();
	}
	
	public Funcionario(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
    	if (endereco == null || endereco.getLogradouro() == null) {
			this.endereco = null;
		} else {
			this.endereco = endereco;
		}
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public List<OrgaoFuncionario> getOrgaosFuncionario() {
		return orgaosFuncionario;
	}

	public void setOrgaosFuncionario(List<OrgaoFuncionario> orgaosFuncionario) {
		if (orgaosFuncionario != null) {
			this.orgaosFuncionario.clear();
			this.orgaosFuncionario.addAll(orgaosFuncionario);
		} else {
			this.orgaosFuncionario = orgaosFuncionario;
		}
	}

	public List<Contato> getContatos() {
		return contatos;
	}

	public void setContatos(List<Contato> contatos) {
		if (contatos != null) {
			this.contatos.clear();
			this.contatos.addAll(contatos);
		} else {
			this.contatos = contatos;
		}
	}

	public List<ContatoPessoal> getContatosPessoais() {
		return contatosPessoais;
	}

	public void setContatosPessoais(List<ContatoPessoal> contatosPessoais) {
		if (contatosPessoais != null) {
			this.contatosPessoais.clear();
			this.contatosPessoais.addAll(contatosPessoais);
		} else {
			this.contatosPessoais = contatosPessoais;
		}
	}

	public void setPlanoFerias(List<PlanoFerias> planoFerias) {
		if (planoFerias != null) {
			this.planoFerias.clear();
			this.planoFerias.addAll(planoFerias);
		} else {
			this.planoFerias = planoFerias;
		}
	}

	public LocalDate getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(LocalDate dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public LocalDate getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(LocalDate dataSaida) {
		this.dataSaida = dataSaida;
	}

	public String getUgb() {
		return ugb;
	}

	public void setUgb(String ugb) {
		this.ugb = ugb;
	}

	public List<CredorFuncionario> getCredoresFuncionario() {
		return credoresFuncionario;
	}

	public void setCredoresFuncionario(List<CredorFuncionario> credoresFuncionario) {
		if (credoresFuncionario != null) {
			this.credoresFuncionario.clear();
			this.credoresFuncionario.addAll(credoresFuncionario);
		} else {
			this.credoresFuncionario = credoresFuncionario;
		}
	}

	public List<Credor> getCredores() {
		return credores;
	}

	public void setCredores(List<Credor> credores) {
		this.credores = credores;
	}

	public void setRequerimentos(List<Requerimento> requerimentos) {
		if (requerimentos != null) {
			this.requerimentos.clear();
			this.requerimentos.addAll(requerimentos);
		} else {
			this.requerimentos = requerimentos;
		}
	}

	public List<Requerimento> getRequerimentos() {
		return requerimentos;
	}

	public List<Orgao> getOrgaos() {
		return orgaos;
	}

	public void setOrgaos(List<Orgao> orgaos) {
		if (orgaos != null) {
			this.orgaos.clear();
			this.orgaos.addAll(orgaos);
		} else {
			this.orgaos = orgaos;
		}
	}

	public PlanoFerias getPlanoFer() {
		return planoFer;
	}

	public void setPlanoFer(PlanoFerias planoFer) {
		this.planoFer = planoFer;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<PlanoFerias> getPlanoFerias() {
		return planoFerias;
	}
	
	@Override
	public String toString() {
		return nome;
	}

}
