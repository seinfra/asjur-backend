package br.com.seinfra.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.Caixa;
import br.com.seinfra.model.filter.CaixaFilter;
import br.com.seinfra.service.CaixaService;

@RestController
@RequestMapping("/asjur-api/caixa")
public class CaixaController extends GenericBaseController<CaixaService, Caixa, Long> {
	
	@GetMapping	
	public Page<Caixa> pequisar(CaixaFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
	@GetMapping("/buscarPorNome")
	public Caixa buscarPorNome(String nome){
		return servico.buscarPorNome(nome);
	}
	
}