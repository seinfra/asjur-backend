package br.com.seinfra.controller;

import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.Funcionario;
import br.com.seinfra.model.filter.FuncionarioFilter;
import br.com.seinfra.service.FuncionarioService;

@RestController
@RequestMapping("/asjur-api/funcionario")
public class FuncionarioController extends GenericBaseController<FuncionarioService, Funcionario, Long> {
	
	@GetMapping	
	public Page<Funcionario> pequisar(FuncionarioFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
	@GetMapping("/calcularDiaFerias")
	public Long calcularDiasFerias(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dataEntrada, 
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dataFim) {
		return servico.calcularDiasFerias(dataEntrada, dataFim);
	}
	
	@GetMapping("/calcularDiaSaldoFerias")
	public Long calcularDiaSaldoFerias(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dataEntrada, 
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dataFim,
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dataEntradaSaldo, 
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dataFimSaldo) {
		return servico.calcularDiasSaldoFerias(dataEntrada, dataFim, dataEntradaSaldo, dataFimSaldo);
	}
	
	@GetMapping("/calcularDiaRestanteFerias")
	public Long calcularDiasFeriasRestante(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dataEntrada, 
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dataFim,
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dataEntradaSaldo, 
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dataFimSaldo,
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dataEntradaSaldoRestante, 
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dataFimSaldoRestante) {
		return servico.calcularDiasRestanteFerias(dataEntrada, dataFim, dataEntradaSaldo, dataFimSaldo, dataEntradaSaldoRestante, dataFimSaldoRestante);
	}
	
	@GetMapping("/funcionario/{cpf}")
	public Funcionario buscarPorCpf(@PathVariable String cpf) {
		return servico.buscarPorCpf(cpf);
	}
	
	@GetMapping("/funcionario-juridico")
	public List<Funcionario> buscarFuncionarioJuridico() {
		return servico.funcionariosJuridico();
	}
	
	@PostMapping("/incluir-funcionario")
	public ResponseEntity<?> incluir(@Valid @RequestBody Funcionario entidade, HttpServletResponse response) {

		Funcionario novo = servico.incluirFuncionario(null, entidade);
		return ResponseEntity.status(HttpStatus.CREATED).body(novo);
	}
	
	@PutMapping("/alterar-funcionario/{codigo}")
	public ResponseEntity<?> alterarFuncionario(@PathVariable Long codigo, @Valid @RequestBody Funcionario entidade) {
		Funcionario entidadeSalva = servico.incluirFuncionario(codigo, entidade);
		return ResponseEntity.ok(entidadeSalva);
	}
	
}
