package br.com.seinfra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.seinfra.global.model.GenericBaseModel;
import br.com.seinfra.model.enums.TIPO_FUNCIONARIO;

@Entity
@Table(name = "tb_tfnc", schema = "bdscj")
public class TipoFuncionario extends GenericBaseModel<Long> {

	@Size(max = 60)
    @Column(name = "tfnc_nome", unique = true)
    private String nome;

    @Column(name = "tfnc_atv")
    private Boolean ativo;

    @Column(name = "tfnc_folha")
    private Boolean folha;

    @Column(name = "tfnc_rh")
    private Boolean rh;

    @Column(name = "tfnc_tipo", nullable = false)
    @Enumerated(EnumType.STRING)
    private TIPO_FUNCIONARIO tipo;

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Boolean getFolha() {
        return folha;
    }

    public void setFolha(Boolean folha) {
        this.folha = folha;
    }

    public Boolean getRh() {
        return rh;
    }

    public void setRh(Boolean rh) {
        this.rh = rh;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TIPO_FUNCIONARIO getTipo() {
        return tipo;
    }

    public void setTipo(TIPO_FUNCIONARIO tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return nome;
    }
}
