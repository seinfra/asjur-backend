package br.com.seinfra.model.enums;

public enum TIPO_FUNCIONARIO {
    SERVIDOR,
    TERCEIRIZADO,
    SERVIDOR_CEDIDO,
    ESTAGIARIO;
	
	 public String getNome() {
         return toString().replaceAll("_", " ");
     }
}
