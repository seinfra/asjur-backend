package br.com.seinfra.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Caixa;
import br.com.seinfra.model.Pasta;
import br.com.seinfra.model.filter.PastaFilter;
import br.com.seinfra.repository.PastaRepository;

@Service
public class PastaService extends GenericBaseService<PastaRepository, Pasta, Long>{
	
	@Autowired
	private CaixaService caixaSerivce;
	
	public Page<Pasta> pequisar(PastaFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public Pasta buscarPorNome(String nome) {
		return repositorio.findByNome(nome);
	}
	
	public Pasta incluirPasta(Pasta entidade) {
		entidade.setCaixa(caixaSerivce.buscarPorNome(entidade.getCaixa().getNome()));
		return repositorio.save(entidade);
	}
	
	public Pasta buscarPasta(Pasta pasta, Caixa caixa) {
		
		Pasta past =  null;
		 
		if(pasta != null) {
			past = buscarPorNome(pasta.getNome());
			if(past == null) {
				pasta.setId(null);
				pasta.setCaixa(caixa);
				incluir(pasta);
				past = buscarPorNome(pasta.getNome());
			}

		}
		return past;
	}
}
