package br.com.seinfra.service;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Perfil;
import br.com.seinfra.model.filter.PerfilFilter;
import br.com.seinfra.repository.PerfilRepository;

@Service
public class PerfilService  extends GenericBaseService<PerfilRepository, Perfil, Long>{
	
	public Page<Perfil> pequisar(PerfilFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public Perfil alterar(Long codigo, Perfil entidade) {
		Perfil entidadeSalva = buscarPorCodigo(codigo);
		
		entidadeSalva.getPermissoes().clear();
		entidadeSalva.getPermissoes().addAll(entidade.getPermissoes());		
		
		BeanUtils.copyProperties(entidade, entidadeSalva, "codigo", "permissoes");		
		return repositorio.save(entidadeSalva);
	}
	
}
