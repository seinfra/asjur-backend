package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.TipoDocumento;
import br.com.seinfra.repository.query.TipoDocumentoRepositoryQuery;

@Repository
public interface TipoDocumentoRepository extends JpaRepository<TipoDocumento, Long>, TipoDocumentoRepositoryQuery {

	public TipoDocumento findByDescricao(String descricao);
}
