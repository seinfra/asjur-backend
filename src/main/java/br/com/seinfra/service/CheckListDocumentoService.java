package br.com.seinfra.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.CheckListDocumentoPendente;
import br.com.seinfra.model.DocumentoJuridico;
import br.com.seinfra.repository.CheckListDocumentoRepository;

@Service
public class CheckListDocumentoService extends GenericBaseService<CheckListDocumentoRepository, CheckListDocumentoPendente, Long>{
	
	public List<CheckListDocumentoPendente> findByDocumento(DocumentoJuridico documento) {
		List<CheckListDocumentoPendente> pendentes = repositorio.findByDocumento(documento);
		
		if(pendentes.size() > 0) {
			for(CheckListDocumentoPendente pend : pendentes) {
				repositorio.deleteById(pend.getId());
			}
		}
		return repositorio.findByDocumento(documento);
	}
	
	public List<CheckListDocumentoPendente> checkListDocs(Long id, Long documento) {
		return repositorio.checkListDocs(id, documento);
	}
}
