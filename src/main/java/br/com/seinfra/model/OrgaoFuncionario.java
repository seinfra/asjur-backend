package br.com.seinfra.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_org_fnc", schema = "bdscj")
public class OrgaoFuncionario extends GenericBaseModel<Long> {

	@ManyToOne
	@JoinColumn(name = "codigo_orgao")
	private Orgao orgao;
	
	@ManyToOne
	@JsonBackReference("orgaosFuncionario")
	@JoinColumn(name = "codigo_funcionario")
	private Funcionario funcionario;

	public Orgao getOrgao() {
		return orgao;
	}

	public void setOrgao(Orgao orgao) {
		if (orgao == null || orgao.getNome() == null) {
			this.orgao = null;
		} else {
			this.orgao = orgao;
		}
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		if (funcionario == null || funcionario.getCpf() == null) {
			this.funcionario = null;
		} else {
			this.funcionario = funcionario;
		}
	}

}
