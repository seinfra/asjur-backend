package br.com.seinfra.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.Credor;
import br.com.seinfra.model.filter.CredorFilter;
import br.com.seinfra.service.CredorService;

@RestController
@RequestMapping("/asjur-api/credor")
public class CredorController extends GenericBaseController<CredorService, Credor, Long> {
	
	@GetMapping	
	public Page<Credor> pequisar(CredorFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
