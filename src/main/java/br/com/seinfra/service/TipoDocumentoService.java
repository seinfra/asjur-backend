package br.com.seinfra.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.TipoDocumento;
import br.com.seinfra.model.filter.TipoDocumentoFilter;
import br.com.seinfra.repository.TipoDocumentoRepository;

@Service
public class TipoDocumentoService extends GenericBaseService<TipoDocumentoRepository, TipoDocumento, Long>{
	
	public Page<TipoDocumento> pequisar(TipoDocumentoFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public TipoDocumento buscarPorNome(String nome){
		return repositorio.findByDescricao(nome);
	}
	
	public TipoDocumento buscarTipoDocumento(TipoDocumento tipoDocumento) {
		
		TipoDocumento tipoDoc = null;
		
		if(tipoDocumento != null) {
			tipoDoc = buscarPorNome(tipoDocumento.getDescricao());
			if(tipoDoc == null) {
				tipoDocumento.setId(null);
				incluir(tipoDocumento);
				tipoDoc = buscarPorNome(tipoDocumento.getDescricao());
			}
		}
		
		return tipoDoc;
	}
}
