package br.com.seinfra.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.seinfra.model.TipoDocumento;
import br.com.seinfra.model.filter.TipoDocumentoFilter;
import br.com.seinfra.repository.query.TipoDocumentoRepositoryQuery;

public class TipoDocumentoRepositoryImpl implements TipoDocumentoRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<TipoDocumento> pequisar(TipoDocumentoFilter filtro, Pageable pageable) {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<TipoDocumento> criteria = builder.createQuery(TipoDocumento.class);
		Root<TipoDocumento> root = criteria.from(TipoDocumento.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.asc(root.get("descricao")));

		TypedQuery<TipoDocumento> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);

		return new PageImpl<>(query.getResultList(), pageable, total(filtro));
	}

	private Predicate[] criarRestricoes(TipoDocumentoFilter filtro, CriteriaBuilder builder,
			Root<TipoDocumento> root) {

		List<Predicate> predicates = new ArrayList<>();

		if(filtro.getId() != null) {			
			predicates.add(builder.equal(root.get("id"), filtro.getId()));
		}
		
		if (!StringUtils.isEmpty(filtro.getDescricao())) {
			predicates.add(builder.like(builder.lower(root.get("descricao")),
					"%" + filtro.getDescricao().toLowerCase() + "%"));
		}				

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;

		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);

	}

	private Long total(TipoDocumentoFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<TipoDocumento> root = criteria.from(TipoDocumento.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	
}

