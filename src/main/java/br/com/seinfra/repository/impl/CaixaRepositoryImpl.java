package br.com.seinfra.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.seinfra.model.Caixa;
import br.com.seinfra.model.filter.CaixaFilter;
import br.com.seinfra.repository.query.CaixaRepositoryQuery;

public class CaixaRepositoryImpl implements CaixaRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<Caixa> pequisar(CaixaFilter filtro, Pageable pageable) {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Caixa> criteria = builder.createQuery(Caixa.class);
		Root<Caixa> root = criteria.from(Caixa.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.asc(root.get("nome")));

		TypedQuery<Caixa> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);

		return new PageImpl<>(query.getResultList(), pageable, total(filtro));
	}

	private Predicate[] criarRestricoes(CaixaFilter filtro, CriteriaBuilder builder,
			Root<Caixa> root) {

		List<Predicate> predicates = new ArrayList<>();

		if(filtro.getId() != null) {			
			predicates.add(builder.equal(root.get("id"), filtro.getId()));
		}
		
		if(filtro.getNumero() != null) {			
			predicates.add(builder.equal(root.get("numero"), filtro.getNumero()));
		}
		
		if (!StringUtils.isEmpty(filtro.getNome())) {
			predicates.add(builder.like(builder.lower(root.get("nome")),
					"%" + filtro.getNome().toLowerCase() + "%"));
		}				

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;

		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);

	}

	private Long total(CaixaFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Caixa> root = criteria.from(Caixa.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	
}

