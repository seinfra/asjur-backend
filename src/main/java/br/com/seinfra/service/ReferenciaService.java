package br.com.seinfra.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Referencia;
import br.com.seinfra.model.filter.ReferenciaFilter;
import br.com.seinfra.repository.ReferenciaRepository;

@Service
public class ReferenciaService extends GenericBaseService<ReferenciaRepository, Referencia, Long>{
	
	public Page<Referencia> pequisar(ReferenciaFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public Referencia buscarPorDescricao(String descricao) {
		return repositorio.findByDescricao(descricao);
	}
	
	public Referencia buscarReferencia(Referencia referencia) {
		
		Referencia ref = null;
		if(referencia != null) {
			ref = buscarPorDescricao(referencia.getDescricao());
			if(ref == null) {
				referencia.setId(null);
				incluir(referencia);
				ref = buscarPorDescricao(referencia.getDescricao());
			}
		}
		return ref;
	}
}
