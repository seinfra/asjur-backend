package br.com.seinfra.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.EmitenteDestinatario;
import br.com.seinfra.model.filter.EmitenteDestinatarioFilter;
import br.com.seinfra.repository.EmitenteDestinatarioRepository;

@Service
public class EmitenteDestinatarioService  extends GenericBaseService<EmitenteDestinatarioRepository, EmitenteDestinatario, Long>{
	
	public Page<EmitenteDestinatario> pequisar(EmitenteDestinatarioFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public EmitenteDestinatario buscarPorDescricao(String descricao){
		return repositorio.findByDescricao(descricao);
	}
	
	public EmitenteDestinatario buscarEmitenteDestinatario(EmitenteDestinatario emitenteDestinatario) {
		
		EmitenteDestinatario emitDest = null;
		
		if(emitenteDestinatario != null) {
			emitDest = buscarPorDescricao(emitenteDestinatario.getDescricao());
			if(emitDest == null) {
				emitenteDestinatario.setId(null);
				incluir(emitenteDestinatario);
				emitDest = buscarPorDescricao(emitenteDestinatario.getDescricao());
			}
		}
		return emitDest;
	}
}
