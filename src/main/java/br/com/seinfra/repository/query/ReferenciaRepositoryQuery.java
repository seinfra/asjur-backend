package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.Referencia;
import br.com.seinfra.model.filter.ReferenciaFilter;

public interface ReferenciaRepositoryQuery {

	public Page<Referencia> pequisar(ReferenciaFilter filtro, Pageable pageable);

}
