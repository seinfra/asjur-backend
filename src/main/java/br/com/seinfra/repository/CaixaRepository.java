package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Caixa;
import br.com.seinfra.repository.query.CaixaRepositoryQuery;

@Repository
public interface CaixaRepository extends JpaRepository<Caixa, Long>, CaixaRepositoryQuery {

	public Caixa findByNome(String nome);
}
