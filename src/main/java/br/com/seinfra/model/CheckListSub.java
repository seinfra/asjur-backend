package br.com.seinfra.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_chk_sub", schema = "bdscj")
public class CheckListSub extends GenericBaseModel<Long> {

    @Column(name = "ches_desc")
    private String descricao;
	
	@Column(name = "ches_comp")
	private String complexidade;
	
	@Column(name = "ches_prz")
	private Integer prazo;
	
	@ManyToOne
	@JsonBackReference("checkListSubs")
	@JoinColumn(name = "chk_tp_id")
	private CheckListTipo checklistTipo;
	
	@JsonManagedReference("checkListDocumentos")
	@OneToMany(mappedBy = "checkListSub", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CheckListDocumento> checkListDocumentos = new ArrayList<CheckListDocumento>();

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getComplexidade() {
		return complexidade;
	}

	public void setComplexidade(String complexidade) {
		this.complexidade = complexidade;
	}

	public Integer getPrazo() {
		return prazo;
	}

	public void setPrazo(Integer prazo) {
		this.prazo = prazo;
	}

	public CheckListTipo getChecklistTipo() {
		return checklistTipo;
	}

	public void setChecklistTipo(CheckListTipo checklistTipo) {
		this.checklistTipo = checklistTipo;
	}

	public List<CheckListDocumento> getCheckListDocumentos() {
		return checkListDocumentos;
	}

	public void setCheckListDocumentos(List<CheckListDocumento> checkListDocumentos) {
		if (checkListDocumentos != null) {
			this.checkListDocumentos.clear();
			this.checkListDocumentos.addAll(checkListDocumentos);
		} else {
			this.checkListDocumentos = checkListDocumentos;
		}
	}

	
}