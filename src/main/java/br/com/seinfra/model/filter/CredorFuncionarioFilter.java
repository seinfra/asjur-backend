/*
 * 21/07/2009
 */
package br.com.seinfra.model.filter;

public class CredorFuncionarioFilter {

	private String credor;
	
    private String funcionario;

	public String getCredor() {
		return credor;
	}

	public void setCredor(String credor) {
		this.credor = credor;
	}

	public String getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(String funcionario) {
		this.funcionario = funcionario;
	}
    
}
