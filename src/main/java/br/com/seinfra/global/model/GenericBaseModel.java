package br.com.seinfra.global.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;

@MappedSuperclass
public class GenericBaseModel<T extends Serializable> implements IGenericBaseModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected T id;

	@JsonIgnore
	@Column(name = "data_cadastro")
	protected LocalDate dataCadastro = LocalDate.now();

	@JsonIgnore
	@Column(name = "data_alteracao")
	protected LocalDate dataAlteracao;

	public T getId() {
		return id;
	}

	public void setId(T id) {
		this.id = id;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;		
	}

	public LocalDate getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(LocalDate dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

}
