package br.com.seinfra.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.Usuario;
import br.com.seinfra.model.filter.UsuarioFilter;
import br.com.seinfra.service.UsuarioService;

@RestController
@RequestMapping("/asjur-api/usuario")
public class UsuarioController extends GenericBaseController<UsuarioService, Usuario, Long> {
	
	@GetMapping	
	public Page<Usuario> pequisar(UsuarioFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
}