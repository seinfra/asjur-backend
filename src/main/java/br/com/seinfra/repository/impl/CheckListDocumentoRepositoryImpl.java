package br.com.seinfra.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.seinfra.model.CheckListDocumento;
import br.com.seinfra.model.CheckListDocumentoPendente;
import br.com.seinfra.model.DocumentoJuridico;
import br.com.seinfra.repository.query.CheckListDocumentoRepositoryQuery;

public class CheckListDocumentoRepositoryImpl implements CheckListDocumentoRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	public List<CheckListDocumentoPendente> checkListDocs(Long id, Long documento) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<CheckListDocumentoPendente> criteria = builder.createQuery(CheckListDocumentoPendente.class);
		Root<CheckListDocumentoPendente> root = criteria.from(CheckListDocumentoPendente.class);
		Join<CheckListDocumentoPendente, CheckListDocumento> joinCheckListDoc = root.join("checkListDocumento");
		Join<CheckListDocumentoPendente, DocumentoJuridico> joinDocumento = root.join("documento");
		
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if(id != null) {
			predicates.add(builder.equal(joinCheckListDoc.get("id"), id));
		}
		if(documento != null) {
			predicates.add(builder.equal(joinDocumento.get("id"), documento));
		}
		
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));
		TypedQuery<CheckListDocumentoPendente> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}

}

