package br.com.seinfra.model.dto;

import java.math.BigDecimal;

import br.com.seinfra.model.TipoDocumento;

public class DocumentoJson {

	private String categoriaDocumento;

	private String numeroDocumento;

	private String numeroProcesso;

	private String interessado;

	private String demanda = "N";

	private String demandaAtendida;

	private BigDecimal valorDemanda;

	private String dataDocumento;
	
	private String dataInicioPrazo;
	
	private String dataFimPrazo;

	private String dataEnvioRecebimento;
	
	private String dataRecebimentoAdvogado;

	private String descricaoAssunto;

	private String detalhe;

	private String observacao;

	private String urlArquivo;

	private String dataConclusao;

	private String dataConclusaoReal;

	private Boolean prazoLegal;
	
	private Boolean redistribuicao;
	
	private Long prazo;

	private String prioridade;
	
	private String ugb;

	private Boolean arquivar;
	
	private TipoDocumento tipoDocumento;

	public String getCategoriaDocumento() {
		return categoriaDocumento;
	}

	public void setCategoriaDocumento(String categoriaDocumento) {
		this.categoriaDocumento = categoriaDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	public String getInteressado() {
		return interessado;
	}

	public void setInteressado(String interessado) {
		this.interessado = interessado;
	}

	public String getDemanda() {
		return demanda;
	}

	public void setDemanda(String demanda) {
		this.demanda = demanda;
	}

	public String getDemandaAtendida() {
		return demandaAtendida;
	}

	public void setDemandaAtendida(String demandaAtendida) {
		this.demandaAtendida = demandaAtendida;
	}

	public BigDecimal getValorDemanda() {
		return valorDemanda;
	}

	public void setValorDemanda(BigDecimal valorDemanda) {
		this.valorDemanda = valorDemanda;
	}

	public String getDataDocumento() {
		return dataDocumento;
	}

	public void setDataDocumento(String dataDocumento) {
		this.dataDocumento = dataDocumento;
	}

	public String getDataInicioPrazo() {
		return dataInicioPrazo;
	}

	public void setDataInicioPrazo(String dataInicioPrazo) {
		this.dataInicioPrazo = dataInicioPrazo;
	}

	public String getDataFimPrazo() {
		return dataFimPrazo;
	}

	public void setDataFimPrazo(String dataFimPrazo) {
		this.dataFimPrazo = dataFimPrazo;
	}

	public String getDataEnvioRecebimento() {
		return dataEnvioRecebimento;
	}

	public void setDataEnvioRecebimento(String dataEnvioRecebimento) {
		this.dataEnvioRecebimento = dataEnvioRecebimento;
	}

	public String getDataRecebimentoAdvogado() {
		return dataRecebimentoAdvogado;
	}

	public void setDataRecebimentoAdvogado(String dataRecebimentoAdvogado) {
		this.dataRecebimentoAdvogado = dataRecebimentoAdvogado;
	}

	public String getDescricaoAssunto() {
		return descricaoAssunto;
	}

	public void setDescricaoAssunto(String descricaoAssunto) {
		this.descricaoAssunto = descricaoAssunto;
	}

	public String getDetalhe() {
		return detalhe;
	}

	public void setDetalhe(String detalhe) {
		this.detalhe = detalhe;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getUrlArquivo() {
		return urlArquivo;
	}

	public void setUrlArquivo(String urlArquivo) {
		this.urlArquivo = urlArquivo;
	}

	public String getDataConclusao() {
		return dataConclusao;
	}

	public void setDataConclusao(String dataConclusao) {
		this.dataConclusao = dataConclusao;
	}

	public String getDataConclusaoReal() {
		return dataConclusaoReal;
	}

	public void setDataConclusaoReal(String dataConclusaoReal) {
		this.dataConclusaoReal = dataConclusaoReal;
	}

	public Boolean getPrazoLegal() {
		return prazoLegal;
	}

	public void setPrazoLegal(Boolean prazoLegal) {
		this.prazoLegal = prazoLegal;
	}

	public Boolean getRedistribuicao() {
		return redistribuicao;
	}

	public void setRedistribuicao(Boolean redistribuicao) {
		this.redistribuicao = redistribuicao;
	}

	public Long getPrazo() {
		return prazo;
	}

	public void setPrazo(Long prazo) {
		this.prazo = prazo;
	}

	public String getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(String prioridade) {
		this.prioridade = prioridade;
	}

	public String getUgb() {
		return ugb;
	}

	public void setUgb(String ugb) {
		this.ugb = ugb;
	}

	public Boolean getArquivar() {
		return arquivar;
	}

	public void setArquivar(Boolean arquivar) {
		this.arquivar = arquivar;
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

}
	