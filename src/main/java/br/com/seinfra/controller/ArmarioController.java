package br.com.seinfra.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.Armario;
import br.com.seinfra.model.filter.ArmarioFilter;
import br.com.seinfra.service.ArmarioService;

@RestController
@RequestMapping("/asjur-api/armario")
public class ArmarioController extends GenericBaseController<ArmarioService, Armario, Long> {
	
	@GetMapping	
	public Page<Armario> pequisar(ArmarioFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
	@GetMapping("/buscarPorNome")
	public Armario buscarPorNome(String nome){
		return servico.buscarPorNome(nome);
	}
	
}