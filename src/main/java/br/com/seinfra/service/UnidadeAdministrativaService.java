package br.com.seinfra.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.UnidadeAdministrativa;
import br.com.seinfra.model.filter.UnidadeAdministrativaFilter;
import br.com.seinfra.repository.UnidadeAdministrativaRepository;

@Service
public class UnidadeAdministrativaService extends GenericBaseService<UnidadeAdministrativaRepository, UnidadeAdministrativa, Long>{
	
	public Page<UnidadeAdministrativa> pequisar(UnidadeAdministrativaFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public UnidadeAdministrativa buscarPorNome(String nome){
		return repositorio.findByNome(nome);
	}
}
