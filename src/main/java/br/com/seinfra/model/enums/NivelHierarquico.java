package br.com.seinfra.model.enums;

public enum NivelHierarquico {

    COORDENADORIA("COORDENADORIA"),
    CELULA("CELULA"),
    PREFEITURA("PREFEITURA"),
    VINCULADA("VINCULADA"),
    EMPRESA("EMPRESA"),
    ORGAO("ORGAO");
	
	private final String descricao;
	
	NivelHierarquico(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
