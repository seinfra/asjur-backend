package br.com.seinfra.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Cargo;
import br.com.seinfra.model.filter.CargoFilter;
import br.com.seinfra.repository.CargoRepository;

@Service
public class CargoService  extends GenericBaseService<CargoRepository, Cargo, Long>{
	
	public Page<Cargo> pequisar(CargoFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public Cargo buscarPorNome(String nome) {
		return repositorio.findByNome(nome);
	}
	
}
