package br.com.seinfra.grafico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.model.DocumentoJuridico;
import br.com.seinfra.service.DocumentoJuridicoService;

@RestController
@RequestMapping("/asjur-api/graficos")
public class GraficoController {
	
	@Autowired
	private DocumentoJuridicoService documentoService;

	@RequestMapping("/processo/por-mes")
	public List<DocumentoJuridico> graficoPorMes() {
		return documentoService.listarDocumento();
	}
	
	@RequestMapping("/processo/por-ugb")
	public List<DocumentoJuridico> graficoPorUgb() {
		return documentoService.listarDocumento();
	}
	
	@GetMapping("/estatistica-documento-ugb")	
	public List<DocumentoJuridico> estatisticaUgb(@RequestParam String prazo) {
		return documentoService.estatisticaUgb(prazo);
	}
	
	@GetMapping("/estatistica-documento-advogado")	
	public List<DocumentoJuridico> estatisticaAdvogado(@RequestParam String prazo) {
		return documentoService.estatisticaAdvogado(prazo);
	}
}
