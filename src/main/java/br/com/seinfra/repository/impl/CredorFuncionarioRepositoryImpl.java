package br.com.seinfra.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import br.com.seinfra.model.Credor;
import br.com.seinfra.model.CredorFuncionario;
import br.com.seinfra.model.Funcionario;
import br.com.seinfra.repository.query.CredorFuncionarioRepositoryQuery;

public class CredorFuncionarioRepositoryImpl implements CredorFuncionarioRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	public CredorFuncionario findByCredorAndFuncionario(String orgao, String funcionario) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<CredorFuncionario> criteria = builder.createQuery(CredorFuncionario.class);
		Root<CredorFuncionario> root = criteria.from(CredorFuncionario.class);
		Join<CredorFuncionario, Credor> joinCredor = root.join("credor");

		List<Predicate> predicates = new ArrayList<>();
		
		predicates.add(builder.equal(root.get("funcionario"), new Funcionario(Long.valueOf(funcionario))));
		predicates.add(builder.equal(joinCredor.get("id"), Long.valueOf(orgao)));
		criteria.where(predicates.toArray(new Predicate[predicates.size()]));

		TypedQuery<CredorFuncionario> query = manager.createQuery(criteria);
		
		return query.getResultList().size() > 0 ?query.getResultList().get(0) : new CredorFuncionario();
	}
	
}
