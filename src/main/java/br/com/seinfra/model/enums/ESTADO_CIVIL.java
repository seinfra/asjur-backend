package br.com.seinfra.model.enums;

public enum ESTADO_CIVIL {
	CASADO,
	DIVORCIADO,
	SOLTEIRO, 
	SEPARADO_JUDICIALMENTE, 
	SEPARADO_DE_FATO, 
	UNIÃO_ESTÁVEL, 
	VIÚVO;

	public String getNomel() {
		return toString().replaceAll("_", " ");
	}
}
