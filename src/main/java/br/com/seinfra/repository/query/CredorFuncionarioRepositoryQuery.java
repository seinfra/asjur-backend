package br.com.seinfra.repository.query;

import br.com.seinfra.model.CredorFuncionario;

public interface CredorFuncionarioRepositoryQuery {
	
	public CredorFuncionario findByCredorAndFuncionario(String credor, String funcionario);

}
