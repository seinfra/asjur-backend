package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Perfil;
import br.com.seinfra.repository.query.PerfilRepositoryQuery;

@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Long>, PerfilRepositoryQuery {

}
