package br.com.seinfra.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Usuario;
import br.com.seinfra.model.filter.UsuarioFilter;
import br.com.seinfra.other.CriptografaSenha;
import br.com.seinfra.repository.UsuarioRepository;

@Service
public class UsuarioService extends GenericBaseService<UsuarioRepository, Usuario, Long>{	
	
	public Page<Usuario> pequisar(UsuarioFilter usuarioFilter, Pageable pageable) {
		return repositorio.pequisar(usuarioFilter, pageable);
	}
	
	@Override
	public Usuario incluir(Usuario entidade) {
		CriptografaSenha geradorSenha = new CriptografaSenha();
		entidade.setSenha(geradorSenha.codificarSenha(entidade.getSenha()));
		return super.incluir(entidade);
	}
	
	@Override
	public Usuario alterar(Long codigo, Usuario entidade) {
		
		Usuario usuario = buscarPorCodigo(codigo);
		
		if(usuario.getSenha().equals(entidade.getSenha())) {
			return super.alterar(codigo, entidade);
		}
		
		CriptografaSenha geradorSenha = new CriptografaSenha();
		entidade.setSenha(geradorSenha.codificarSenha(entidade.getSenha()));
		return super.alterar(codigo, entidade);
	}
	
	public Optional<Usuario> buscarPorEmail(String email){
		Optional<Usuario> usuarioOptional = repositorio.findByEmail(email);
		return usuarioOptional;
	}
	
	public Optional<Usuario> buscarPorLogin(String login){
		Optional<Usuario> usuarioOptional = repositorio.findByLogin(login);
		return usuarioOptional;
	}
	
	public List<Usuario> envioEmailUsuario(String cpf) {
		return repositorio.envioEmailUsuario(cpf);
	}

}
