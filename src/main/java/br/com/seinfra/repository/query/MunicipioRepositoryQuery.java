package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.Municipio;
import br.com.seinfra.model.filter.MunicipioFilter;

public interface MunicipioRepositoryQuery {

	public Page<Municipio> pequisar(MunicipioFilter filtro, Pageable pageable);
	
}
