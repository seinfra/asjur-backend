package br.com.seinfra.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.DocumentoJuridico;
import br.com.seinfra.model.filter.DocumentoJuridicoFilter;
import br.com.seinfra.service.DocumentoJuridicoService;

@RestController
@RequestMapping("/asjur-api/documento-juridico")
public class DocumentoJuridicoController extends GenericBaseController<DocumentoJuridicoService, DocumentoJuridico, Long> {
	
	@GetMapping	
	public Page<DocumentoJuridico> pequisar(DocumentoJuridicoFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
	@GetMapping("/juridico/{codigo}")
	public Optional<DocumentoJuridico> buscarPorCodigo(@PathVariable Long id) {
		return servico.buscarPorId(id);
	}
	
	@GetMapping("/usuario/{cpf}")
	public List<DocumentoJuridico> documentoPorUsuario(@PathVariable String cpf) {
		return servico.documentoPorUsuario(cpf);
	}
	
	@GetMapping("/funcionario/{cpf}")
	public List<DocumentoJuridico> documentoPorFuncionarioReceber(@PathVariable String cpf) {
		return servico.documentoPorFuncionarioReceber(cpf);
	}
	
	@PostMapping("/incluirDocumento")
	public ResponseEntity<?> incluir(@Valid @RequestBody DocumentoJuridico entidade, HttpServletResponse response) {

		DocumentoJuridico novo = servico.incluirDocumento(entidade, "S");
		return ResponseEntity.status(HttpStatus.CREATED).body(novo);
	}
	
	@GetMapping("/analise-documento")	
	public List<DocumentoJuridico> documentoParaAnaliseCoordenador() {
		return servico.documentoParaAnaliseCoordenador();
	}
	
	@GetMapping("/checklist-documento/{cpf}")	
	public List<DocumentoJuridico> documentoParaCheckList(@PathVariable String cpf) {
		return servico.documentoParaCheckList(cpf);
	}
	
	@GetMapping("/inicio-distribuicao-documento")	
	public List<DocumentoJuridico> inicioDistribuicao() {
		return servico.inicioDistribuicao();
	}
	
	@GetMapping("/distribuicao-bloco-documento")	
	public List<DocumentoJuridico> distribuicaoEmBloco() {
		return servico.distribuicaoEmBloco();
	}
	
	@GetMapping("/distribuicao-individual-documento/{id}")	
	public DocumentoJuridico distribuicaoEmBloco(@PathVariable Long id) {
		return servico.distribuicaoIndividual(id);
	}
	
	@GetMapping("/finalizar-documento")	
	public List<DocumentoJuridico> documentoFinalizar() {
		return servico.documentoFinalizar();
	}
	
	@PutMapping("/alterar-documento/{codigo}")
	public ResponseEntity<?> alterarDocumento(@PathVariable Long codigo, @Valid @RequestBody DocumentoJuridico entidade) {
		DocumentoJuridico entidadeSalva = servico.alterarDocumento(codigo, entidade);
		return ResponseEntity.ok(entidadeSalva);
	}
	
	@RequestMapping("/calcular-prazo")
	public Long calcularPrazoDias(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime dataEnvioRecebimento, 
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime dataFimPrazo) {
		return servico.calcularPrazoDias(dataEnvioRecebimento, dataFimPrazo);
	}
	
	@RequestMapping("/relatorioProcesso/{id}")
	public ResponseEntity<byte[]> relatorioProcesso(@PathVariable Long id) throws Exception{
		byte[] relatorio = servico.relatorioProcesso(id);

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
				.body(relatorio);
	}
	
}