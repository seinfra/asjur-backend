package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.CredorFuncionario;
import br.com.seinfra.repository.query.CredorFuncionarioRepositoryQuery;

@Repository
public interface CredorFuncionarioRepository extends JpaRepository<CredorFuncionario, Long>, CredorFuncionarioRepositoryQuery {
	
}
