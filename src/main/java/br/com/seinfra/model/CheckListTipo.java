package br.com.seinfra.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_chk_tp", schema = "bdscj")
public class CheckListTipo extends GenericBaseModel<Long> {

    @Column(name = "chec_desc")
    private String descricao;
	
	@ManyToOne
	@JsonBackReference("checkListTipos")
	@JoinColumn(name = "chk_id")
	private CheckList checklist;
	
	@JsonManagedReference("checkListSubs")
	@OneToMany(mappedBy = "checklistTipo", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CheckListSub> checkListSubs = new ArrayList<CheckListSub>();


	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public CheckList getChecklist() {
		return checklist;
	}

	public void setChecklist(CheckList checklist) {
		this.checklist = checklist;
	}

	public List<CheckListSub> getCheckListSubs() {
		return checkListSubs;
	}

	public void setCheckListSubs(List<CheckListSub> checkListSubs) {
		if (checkListSubs != null) {
			this.checkListSubs.clear();
			this.checkListSubs.addAll(checkListSubs);
		} else {
			this.checkListSubs = checkListSubs;
		}
	}

}