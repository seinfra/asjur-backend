package br.com.seinfra.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_chk", schema = "bdscj")
public class CheckList extends GenericBaseModel<Long> {

    @Column(name = "chec_desc")
    private String descricao;
	
	@JsonManagedReference("checkListTipos")
	@OneToMany(mappedBy = "checklist", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CheckListTipo> checkListTipos = new ArrayList<CheckListTipo>();


	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<CheckListTipo> getCheckListTipos() {
		return checkListTipos;
	}

	public void setCheckListTipos(List<CheckListTipo> checkListTipos) {
		if (checkListTipos != null) {
			this.checkListTipos.clear();
			this.checkListTipos.addAll(checkListTipos);
		} else {
			this.checkListTipos = checkListTipos;
		}
	}

}