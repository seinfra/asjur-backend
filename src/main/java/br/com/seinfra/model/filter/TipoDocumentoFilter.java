/*
 * 21/07/2009
 */
package br.com.seinfra.model.filter;

public class TipoDocumentoFilter {

	private Long id;
	
    private String descricao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
