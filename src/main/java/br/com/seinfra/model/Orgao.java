package br.com.seinfra.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.seinfra.global.model.GenericBaseModel;
import br.com.seinfra.model.enums.TIPO_ORGAO;

@Entity
@Table(name = "tb_org", schema = "bdger")
public class Orgao extends GenericBaseModel<Long> {

    @Column(name = "org_nom", unique = true, nullable = false)
    private String nome;
    
    @Column(name = "org_sig", unique = true, nullable = false)
    private String sigla;
    
    @Column(name = "org_cnpj", length = 14, unique = true)
    private String cnpj;

    @Column(name = "org_tip")
    @Enumerated(EnumType.STRING)
    private TIPO_ORGAO tipo;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "end_id")
    private Endereco endereco;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "org_sup_id")
    private Orgao superior;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
    	if (endereco == null || endereco.getLogradouro() == null) {
			this.endereco = null;
		} else {
			this.endereco = endereco;
		}
    }

    public void setTipo(TIPO_ORGAO tipo) {
        this.tipo = tipo;
    }

    public TIPO_ORGAO getTipo() {
        return tipo;
    }

    public void setSuperior(Orgao superior) {
        this.superior = superior;
    }

    public Orgao getSuperior() {
        return superior;
    }

}
