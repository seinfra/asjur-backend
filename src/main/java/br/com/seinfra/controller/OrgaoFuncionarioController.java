package br.com.seinfra.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.OrgaoFuncionario;
import br.com.seinfra.model.filter.OrgaoFuncionarioFilter;
import br.com.seinfra.service.OrgaoFuncionarioService;

@RestController
@RequestMapping("/asjur-api/orgao-funcionario")
public class OrgaoFuncionarioController extends GenericBaseController<OrgaoFuncionarioService, OrgaoFuncionario, Long> {
	
	@GetMapping
	public OrgaoFuncionario buscarPorOrgaoFuncionario(OrgaoFuncionarioFilter filter) {
		return servico.buscarPorOrgaoFuncionario(filter.getOrgao(), filter.getFuncionario());
	}
}