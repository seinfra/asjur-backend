package br.com.seinfra.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.EmitenteDestinatario;
import br.com.seinfra.model.filter.EmitenteDestinatarioFilter;
import br.com.seinfra.service.EmitenteDestinatarioService;

@RestController
@RequestMapping("/asjur-api/emitente-destinatario")
public class EmitenteDestinatarioController extends GenericBaseController<EmitenteDestinatarioService, EmitenteDestinatario, Long> {
	
	@GetMapping	
	public Page<EmitenteDestinatario> pequisar(EmitenteDestinatarioFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
