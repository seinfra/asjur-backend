package br.com.seinfra.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_crd_fnc", schema = "bdscj")
public class CredorFuncionario extends GenericBaseModel<Long> {

	@ManyToOne
	@JoinColumn(name = "codigo_crd")
	private Credor credor;
	
	@ManyToOne
	@JsonBackReference("credoresFuncionario")
	@JoinColumn(name = "codigo_fnc")
	private Funcionario funcionario;

	public Credor getCredor() {
		return credor;
	}

	public void setCredor(Credor credor) {
		if (credor == null || credor.getNome() == null) {
			this.credor = null;
		} else {
			this.credor = credor;
		}
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		if (funcionario == null || funcionario.getCpf() == null) {
			this.funcionario = null;
		} else {
			this.funcionario = funcionario;
		}
	}

}
