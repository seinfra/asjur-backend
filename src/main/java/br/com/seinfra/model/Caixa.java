package br.com.seinfra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_cxa", schema = "bdscj")
public class Caixa extends GenericBaseModel<Long> {

	@Size(max = 100)
    @Column(name = "cxa_nome")
    private String nome;

    @Column(name = "cxa_num")
    private Integer numero;
	
    @Column(name = "cxa_ano")
    private Integer ano;

    @ManyToOne
    @JoinColumn(name = "uadm_id")
    private UnidadeAdministrativa unidadeAdministrativa;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public UnidadeAdministrativa getUnidadeAdministrativa() {
        return unidadeAdministrativa;
    }

    public void setUnidadeAdministrativa(UnidadeAdministrativa unidadeAdministrativa) {
    	if(unidadeAdministrativa == null || unidadeAdministrativa.getNome() == null) {
			this.unidadeAdministrativa = null;
		}else {
			this.unidadeAdministrativa = unidadeAdministrativa;
		}    
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

	@Override
	public String toString() {
		return new StringBuilder().append(nome).append(" - ").append(numero).toString();
	}
    
}
