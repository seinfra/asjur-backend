/**
 *
 */
package br.com.seinfra.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.seinfra.global.model.GenericBaseModel;
import br.com.seinfra.model.enums.TIPO_STATUS;

@Entity
@Table(name = "tb_processo", schema = "bdscj")
public class Processo extends GenericBaseModel<Long> {

    @ManyToOne
    @JoinColumn(name = "doc_id")
    private DocumentoJuridico documento;

    @Size(max = 500)
    @Column(name = "decricao")
    private String descricao;

    @Size(max = 30)
    @Column(name = "analisador")
    private String analisador;

    @Column(name = "valor")
    private BigDecimal valor;

    @Column(name = "observacao")
    private String observacao;

    @Column(name = "arquivar")
    private Boolean arquivar;

    @Column(name = "entrada")
    private Date entrada;
    
    @Column(name = "saida")
    private Date saida;

    @Column(name = "previsao_saida")
    private Date previsaoSaida;
    
    @Size(max = 40)
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private TIPO_STATUS status;

    public String getNumeroDocumentoProcesso() {
        String numero = "";
        if (documento != null) {
            numero = documento.getNumeroProcesso() == null ? documento.getTipoDocumento().getDescricao() + "-"
                    + documento.getNumeroDocumento() : documento.getNumeroProcesso();
        }
        return numero;
    }

    public DocumentoJuridico getDocumento() {
        return documento;
    }

    public void setDocumento(DocumentoJuridico documento) {
        this.documento = documento;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getAnalisador() {
        return analisador;
    }

    public void setAnalisador(String analisador) {
        this.analisador = analisador;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Boolean getArquivar() {
        return arquivar;
    }

    public void setArquivar(Boolean arquivar) {
        this.arquivar = arquivar;
    }

    public Date getEntrada() {
        return entrada;
    }

    public void setEntrada(Date entrada) {
        this.entrada = entrada;
    }

    public Date getSaida() {
        return saida;
    }

    public void setSaida(Date saida) {
        this.saida = saida;
    }

    public TIPO_STATUS getStatus() {
        return status;
    }

    public void setStatus(TIPO_STATUS status) {
        this.status = status;
    }

    public Date getPrevisaoSaida() {
        return previsaoSaida;
    }

    public void setPrevisaoSaida(Date previsaoSaida) {
        this.previsaoSaida = previsaoSaida;
    }

}
