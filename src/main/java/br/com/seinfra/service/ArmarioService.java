package br.com.seinfra.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Armario;
import br.com.seinfra.model.filter.ArmarioFilter;
import br.com.seinfra.repository.ArmarioRepository;

@Service
public class ArmarioService extends GenericBaseService<ArmarioRepository, Armario, Long>{
	
	public Page<Armario> pequisar(ArmarioFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public Armario buscarPorNome(String nome) {
		return repositorio.findByNome(nome);
	}
	
	public Armario buscarArmario(Armario armario) {
		
		Armario arm = null;
		
		if(armario != null) {
			arm = buscarPorNome(armario.getNome());
			if(arm == null) {
				armario.setId(null);
				incluir(armario);
				arm = buscarPorNome(armario.getNome());
			}
		}
		return arm;
	}
}
