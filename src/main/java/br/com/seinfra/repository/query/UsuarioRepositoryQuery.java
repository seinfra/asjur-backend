package br.com.seinfra.repository.query;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.Usuario;
import br.com.seinfra.model.filter.UsuarioFilter;

public interface UsuarioRepositoryQuery {

	public Page<Usuario> pequisar(UsuarioFilter usuarioFilter, Pageable pageable);
	
	public List<Usuario> envioEmailUsuario(String cpf);
	
}
