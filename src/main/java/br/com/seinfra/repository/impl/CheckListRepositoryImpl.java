package br.com.seinfra.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.seinfra.model.CheckList;
import br.com.seinfra.model.CheckListDocumento;
import br.com.seinfra.model.CheckListSub;
import br.com.seinfra.model.CheckListTipo;
import br.com.seinfra.model.filter.CheckListFilter;
import br.com.seinfra.repository.query.CheckListRepositoryQuery;

public class CheckListRepositoryImpl implements CheckListRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<CheckList> pequisar(CheckListFilter filtro, Pageable pageable) {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<CheckList> criteria = builder.createQuery(CheckList.class);
		Root<CheckList> root = criteria.from(CheckList.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.asc(root.get("descricao")));

		TypedQuery<CheckList> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);

		return new PageImpl<>(query.getResultList(), pageable, total(filtro));
	}

	private Predicate[] criarRestricoes(CheckListFilter filtro, CriteriaBuilder builder,
			Root<CheckList> root) {

		List<Predicate> predicates = new ArrayList<>();

		if(filtro.getId() != null) {			
			predicates.add(builder.equal(root.get("id"), filtro.getId()));
		}
		
		if (!StringUtils.isEmpty(filtro.getDescricao())) {
			predicates.add(builder.like(builder.lower(root.get("descricao")),
					"%" + filtro.getDescricao().toLowerCase() + "%"));
		}				

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;

		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);

	}

	private Long total(CheckListFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<CheckList> root = criteria.from(CheckList.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	
	public List<CheckListTipo> checkListTipo(Long id) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<CheckListTipo> criteria = builder.createQuery(CheckListTipo.class);
		Root<CheckListTipo> root = criteria.from(CheckListTipo.class);
		Join<CheckListTipo, CheckList> joinCheckList = root.join("checklist");

		if(id != null) {
			criteria.where(builder.equal(joinCheckList.get("id"), id));
		}
		TypedQuery<CheckListTipo> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public List<CheckListSub> checkListSub(Long id) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<CheckListSub> criteria = builder.createQuery(CheckListSub.class);
		Root<CheckListSub> root = criteria.from(CheckListSub.class);
		Join<CheckListSub, CheckListTipo> joinCheckListTipo = root.join("checklistTipo");

		if(id != null) {
			criteria.where(builder.equal(joinCheckListTipo.get("id"), id));
		}
		TypedQuery<CheckListSub> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	public CheckListSub checkListSubSelecionado(Long id) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<CheckListSub> criteria = builder.createQuery(CheckListSub.class);
		Root<CheckListSub> root = criteria.from(CheckListSub.class);

		if(id != null) {
			criteria.where(builder.equal(root.get("id"), id));
		}
		TypedQuery<CheckListSub> query = manager.createQuery(criteria);
		
		return query.getResultList().size() > 0 ? query.getResultList().get(0) : new CheckListSub();
	}
	
	public List<CheckListDocumento> checkListDocs(Long id, Long documento) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<CheckListDocumento> criteria = builder.createQuery(CheckListDocumento.class);
		Root<CheckListDocumento> root = criteria.from(CheckListDocumento.class);
		Join<CheckListDocumento, CheckListSub> joinCheckListSub = root.join("checkListSub");

		if(id != null) {
			criteria.where(builder.equal(joinCheckListSub.get("id"), Long.valueOf(id)));
		}
		TypedQuery<CheckListDocumento> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
}

