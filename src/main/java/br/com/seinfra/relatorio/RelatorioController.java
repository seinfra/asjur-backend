package br.com.seinfra.relatorio;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.model.dto.DocumentoForm;
import br.com.seinfra.service.DocumentoJuridicoService;
import br.com.seinfra.service.FuncionarioService;

@RestController
@RequestMapping("/asjur-api/relatorios")
public class RelatorioController {
	
	@Autowired
	private FuncionarioService funcionarioService; 
	
	@Autowired
	private DocumentoJuridicoService documentoService;

	@RequestMapping("/planoFerias")
	public ResponseEntity<byte[]>  relatorioPlanoFerias(@RequestParam String ano, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate inicio, 
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fim, @RequestParam String ugb) throws Exception{
		byte[] relatorio = funcionarioService.relatorioPlanoFerias(ano, inicio, fim, ugb);

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
				.body(relatorio);
	}
	
	@RequestMapping("/controleJuridico")
	public ResponseEntity<byte[]>  relatorioControleJuridico(DocumentoForm documentoForm) throws Exception{
		byte[] relatorio = documentoService.relatorioControleJuridico(documentoForm);

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
				.body(relatorio);
	}
	
	@RequestMapping("/controleJuridicoResumo")
	public ResponseEntity<byte[]>  relatorioControleJuridicoResumo(DocumentoForm documentoForm) throws Exception{
		byte[] relatorio = documentoService.relatorioControleJuridicoResumo(documentoForm);

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
				.body(relatorio);
	}
	
	@RequestMapping("/controlePrazos")
	public ResponseEntity<byte[]>  relatorioControlePrazos(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate inicio, 
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fim, @RequestParam String ugb) throws Exception{
		byte[] relatorio = documentoService.relatorioControlePrazos(inicio, fim, ugb);

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
				.body(relatorio);
	}
	
	@RequestMapping("/controleEntrega")
	public ResponseEntity<byte[]>  relatorioControleEntrega(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate inicio, 
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate fim, @RequestParam String id) throws Exception{
		byte[] relatorio = documentoService.relatorioControleEntrega(inicio, fim, id);

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
				.body(relatorio);
	}
}
