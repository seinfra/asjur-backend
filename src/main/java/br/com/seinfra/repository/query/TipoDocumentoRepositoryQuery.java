package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.TipoDocumento;
import br.com.seinfra.model.filter.TipoDocumentoFilter;

public interface TipoDocumentoRepositoryQuery {

	public Page<TipoDocumento> pequisar(TipoDocumentoFilter filtro, Pageable pageable);

}
