package br.com.seinfra.model;

import br.com.seinfra.global.model.GenericBaseModel;

public class DocumentosPendentes extends GenericBaseModel<Long> {

	private String numeroDocumento;
	private String processo;
	private String assunto;
	private String destino;
	private String interessado;
	private String dataMovimento;
	private String dataCadastroDocumento;
	private String dataConclusaoPrevista;
	private Long diastotalProcesso;
	private Long diasRestante;

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getProcesso() {
		return processo;
	}

	public void setProcesso(String processo) {
		this.processo = processo;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getInteressado() {
		return interessado;
	}

	public void setInteressado(String interessado) {
		this.interessado = interessado;
	}

	public String getDataMovimento() {
		return dataMovimento;
	}

	public void setDataMovimento(String dataMovimento) {
		this.dataMovimento = dataMovimento;
	}

	public String getDataCadastroDocumento() {
		return dataCadastroDocumento;
	}

	public void setDataCadastroDocumento(String dataCadastroDocumento) {
		this.dataCadastroDocumento = dataCadastroDocumento;
	}

	public String getDataConclusaoPrevista() {
		return dataConclusaoPrevista;
	}

	public void setDataConclusaoPrevista(String dataConclusaoPrevista) {
		this.dataConclusaoPrevista = dataConclusaoPrevista;
	}

	public Long getDiastotalProcesso() {
		return diastotalProcesso;
	}

	public void setDiastotalProcesso(Long diastotalProcesso) {
		this.diastotalProcesso = diastotalProcesso;
	}

	public Long getDiasRestante() {
		return diasRestante;
	}

	public void setDiasRestante(Long diasRestante) {
		this.diasRestante = diasRestante;
	}

}