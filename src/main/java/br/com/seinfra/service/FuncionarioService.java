package br.com.seinfra.service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.DocumentoJuridico;
import br.com.seinfra.model.Funcionario;
import br.com.seinfra.model.Orgao;
import br.com.seinfra.model.OrgaoFuncionario;
import br.com.seinfra.model.PlanoFerias;
import br.com.seinfra.model.filter.FuncionarioFilter;
import br.com.seinfra.other.Propriedades;
import br.com.seinfra.repository.FuncionarioRepository;
import net.sf.jasperreports.engine.JasperExportManager;

@Service
public class FuncionarioService  extends GenericBaseService<FuncionarioRepository, Funcionario, Long>{
	
	@Autowired
	private OrgaoService orgaoService;
	
	private Propriedades propertie = new Propriedades();
	
	public Page<Funcionario> pequisar(FuncionarioFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public Funcionario buscarPorNome(String nome){
		return repositorio.findByNome(nome);
	}
	
	public Funcionario buscarPorCpf(String cpf){
		return repositorio.findByCpf(cpf);
	}
	
	public Long calcularDiasFerias(LocalDate dataEntrada, LocalDate dataFim){
		Long dias = ChronoUnit.DAYS.between(dataEntrada, dataFim);
		
		return dias + 1;
	}
	
	public Long calcularDiasSaldoFerias(LocalDate dataEntrada, LocalDate dataFim, 
			LocalDate dataEntradaSaldo, LocalDate dataFimSaldo){
		Long dias = ChronoUnit.DAYS.between(dataEntrada, dataFim);
		Long diasSaldo = ChronoUnit.DAYS.between(dataEntradaSaldo, dataFimSaldo);
		
		return 30 - (dias + diasSaldo + 1);
	}
	
	public Long calcularDiasRestanteFerias(LocalDate dataEntrada, LocalDate dataFim, 
			LocalDate dataEntradaSaldo, LocalDate dataFimSaldo, LocalDate dataEntradaSaldoRestante, LocalDate dataFimSaldoRestante){
		Long dias = ChronoUnit.DAYS.between(dataEntrada, dataFim);
		Long diasSaldo = ChronoUnit.DAYS.between(dataEntradaSaldo, dataFimSaldo);
		Long diasSaldoRestante = ChronoUnit.DAYS.between(dataEntradaSaldoRestante, dataFimSaldoRestante);
		
		return 30 - (dias + diasSaldo + diasSaldoRestante + 3);
	}
	
	public Funcionario buscarPorCoordenador() {
		return repositorio.buscarPorCoordenador();
	}
	
	public List<Funcionario> distribuicaoFuncionario(DocumentoJuridico documento, List<Funcionario> funcionarios){
		return repositorio.distribuicaoFuncionario(documento, funcionarios);
	}
	
	public List<Funcionario> funcionarioSemDocumento(DocumentoJuridico documento, List<Funcionario> funcionarios){
		return repositorio.funcionarioSemDocumento(documento, funcionarios);
	}
	
	public List<Funcionario> funcionariosFerias(){
		return repositorio.funcionariosFerias();
	}
	
	public List<Funcionario> funcionariosAfastados() {
		return repositorio.funcionariosAfastados();
	}
	
	public List<Funcionario> funcionariosComDocumento(DocumentoJuridico documento) {
		return repositorio.funcionariosComDocumento(documento);
	}
	
	public List<Funcionario> funcionariosJuridico() {
		return repositorio.funcionariosJuridico();
	}
	
	public Funcionario distribuicaoCompliance(String nome) {
		if(nome != null) {
			return repositorio.distribuicaoCompliance(nome);
		}else {
			return null;
		}
	}
	
	public Funcionario incluirFuncionario(Long codigo, Funcionario entidade) {
		entidade.getOrgaosFuncionario().clear();

		for(Orgao orgao : entidade.getOrgaos()) {
			OrgaoFuncionario orgaoFuncionario = new OrgaoFuncionario();
			orgaoFuncionario.setOrgao(orgaoService.buscarPorCodigo(orgao.getId()));
			orgaoFuncionario.setFuncionario(entidade);
			entidade.getOrgaosFuncionario().add(orgaoFuncionario);
		}
		if(codigo != null) {
			Funcionario entidadeSalva = buscarPorCodigo(codigo);
			BeanUtils.copyProperties(entidade, entidadeSalva, "codigo");
		}
		return repositorio.save(entidade);
	}
	
	public byte[] relatorioPlanoFerias(String ano, LocalDate inicio, LocalDate fim, String ugb) throws Exception {
		
		List<PlanoFerias> dados = repositorio.relatorioPlanoFerias(ano != "" ? Integer.valueOf(ano) : null, inicio, fim, ugb);
		for(PlanoFerias plano : dados) {
			if(plano.getDataFinal() != null && plano.getDataFinal().isBefore(LocalDate.now())) {
				plano.setFerias(Boolean.TRUE);
			}
			if(plano.getDataFinalSaldo() != null && plano.getDataFinalSaldo().isBefore(LocalDate.now())) {
				plano.setFeriasSaldo(Boolean.TRUE);
			}
			if(plano.getDataFinalSaldoRestante() != null && plano.getDataFinalSaldoRestante().isBefore(LocalDate.now())) {
				plano.setFeriasSaldoRestante(Boolean.TRUE);
			}
		}
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
		
		return JasperExportManager.exportReportToPdf(propertie.verificaDados(dados, parametros , "plano-ferias"));
	}
}
