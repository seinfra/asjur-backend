package br.com.seinfra.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.seinfra.global.service.GenericBaseService;
import br.com.seinfra.model.Caixa;
import br.com.seinfra.model.filter.CaixaFilter;
import br.com.seinfra.repository.CaixaRepository;

@Service
public class CaixaService extends GenericBaseService<CaixaRepository, Caixa, Long>{
	
	public Page<Caixa> pequisar(CaixaFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public Caixa buscarPorNome(String nome) {
		return repositorio.findByNome(nome);
	}
	
	public Caixa buscarCaixa(Caixa caixa) {
		
		Caixa cai = null;
		
		if(caixa != null) {
			cai = buscarPorNome(caixa.getNome());
			if(cai == null) {
				caixa.setId(null);
				incluir(caixa);
				cai = buscarPorNome(caixa.getNome());
			}
		}
		return cai;
	}
}
