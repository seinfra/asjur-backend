package br.com.seinfra.model.enums;

public enum SEXO_FUNCIONARIO {
	MASCULINO,
	FEMININO;
	public String getNome() {
		return toString().replaceAll("_", " ");
	}
}
