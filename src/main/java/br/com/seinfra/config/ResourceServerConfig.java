package br.com.seinfra.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.expression.OAuth2MethodSecurityExpressionHandler;

@Profile("oauth-security")
@Configuration
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/asjur-api/cargo/**").permitAll()
				.antMatchers("/asjur-api/caixa/**").permitAll()
				.antMatchers("/asjur-api/pasta/**").permitAll()
				.antMatchers("/asjur-api/orgao/**").permitAll()
				.antMatchers("/asjur-api/orgao-funcionario/**").permitAll()
				.antMatchers("/asjur-api/perfil/**").permitAll()
				.antMatchers("/asjur-api/usuario/**").permitAll()
				.antMatchers("/asjur-api/armario/**").permitAll()
				.antMatchers("/asjur-api/assunto/**").permitAll()
				.antMatchers("/asjur-api/parecer/**").permitAll()
				.antMatchers("/asjur-api/permissao/**").permitAll()
				.antMatchers("/asjur-api/checklist/**").permitAll()
				.antMatchers("/asjur-api/referencia/**").permitAll()
				.antMatchers("/asjur-api/municipio/**").permitAll()
				.antMatchers("/asjur-api/credor/**").permitAll()
				.antMatchers("/asjur-api/credor-funcionario/**").permitAll()
				.antMatchers("/asjur-api/funcionario/**").permitAll()
				.antMatchers("/asjur-api/tipo-documento/**").permitAll()
				.antMatchers("/asjur-api/documento-juridico/**").permitAll()
				.antMatchers("/asjur-api/checklist-documento/**").permitAll()
				.antMatchers("/asjur-api/unidade-administrativa-juridico/**").permitAll()
				.antMatchers("/asjur-api/tokens/**").permitAll()
				.antMatchers("/asjur-api/graficos/**").permitAll()
				.antMatchers("/asjur-api/relatorios/**").permitAll()
				.anyRequest().authenticated()
				.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			.csrf().disable();
	}
	
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.stateless(true);
	}
	
	@Bean
	public MethodSecurityExpressionHandler createExpressionHandler() {
		return new OAuth2MethodSecurityExpressionHandler();
	}
}
