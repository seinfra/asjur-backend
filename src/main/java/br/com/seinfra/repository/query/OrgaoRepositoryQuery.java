package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.Orgao;
import br.com.seinfra.model.filter.OrgaoFilter;

public interface OrgaoRepositoryQuery {

	public Page<Orgao> pequisar(OrgaoFilter filtro, Pageable pageable);
	
}
