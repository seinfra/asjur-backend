package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.Caixa;
import br.com.seinfra.model.filter.CaixaFilter;

public interface CaixaRepositoryQuery {

	public Page<Caixa> pequisar(CaixaFilter filtro, Pageable pageable);

}
