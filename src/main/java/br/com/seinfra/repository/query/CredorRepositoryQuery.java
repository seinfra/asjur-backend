package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.Credor;
import br.com.seinfra.model.filter.CredorFilter;

public interface CredorRepositoryQuery {

	public Page<Credor> pequisar(CredorFilter filtro, Pageable pageable);
	
}
