/*
 * 21/07/2009
 */
package br.com.seinfra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_referencia", schema = "bdscj")
public class Referencia extends GenericBaseModel<Long> {

	@Size(max = 60)
    @Column(name = "ref_desc", unique = true)
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
