package br.com.seinfra.model.filter;

import br.com.seinfra.model.DocumentoJuridico;

public class ParecerFilter {

	private Long id;
	private DocumentoJuridico documento;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DocumentoJuridico getDocumento() {
		return documento;
	}

	public void setDocumento(DocumentoJuridico documento) {
		this.documento = documento;
	}

}
