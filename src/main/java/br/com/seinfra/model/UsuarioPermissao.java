package br.com.seinfra.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_usuario_permissao", schema = "bdadm")
public class UsuarioPermissao extends GenericBaseModel<Long> {

	@ManyToOne
	@JoinColumn(name = "usuario_id")
	@JsonBackReference("permissoes")
	private Usuario usuario;

	@ManyToOne
	@JoinColumn(name = "permissao_id")
	private Permissao permissao;
	
	public UsuarioPermissao() {
		super();
	}

	public UsuarioPermissao(Usuario usuario, Permissao permissao) {
		this.usuario = usuario;
		this.permissao = permissao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Permissao getPermissao() {
		return permissao;
	}

	public void setPermissao(Permissao permissao) {
		this.permissao = permissao;
	}

}
