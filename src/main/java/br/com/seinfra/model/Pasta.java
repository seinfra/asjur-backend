package br.com.seinfra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_pst", schema = "bdscj")
public class Pasta extends GenericBaseModel<Long> {

    @Column(name = "pst_nome", nullable = false, length = 60)
    private String nome;

    @Column(name = "pst_num", length = 5)
    private Integer numero;

    @ManyToOne
    @JoinColumn(name = "caixa_id")
    private Caixa caixa;

    @Column(name = "pst_ano")
    private Integer ano;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Caixa getCaixa() {
        return caixa;
    }

    public void setCaixa(Caixa caixa) {
		this.caixa = caixa;
    }

}
