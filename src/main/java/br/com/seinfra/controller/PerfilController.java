package br.com.seinfra.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.Perfil;
import br.com.seinfra.model.filter.PerfilFilter;
import br.com.seinfra.service.PerfilService;

@RestController
@RequestMapping("/asjur-api/perfil")
public class PerfilController extends GenericBaseController<PerfilService, Perfil, Long> {
	
	@GetMapping	
	public Page<Perfil> pequisar(PerfilFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
