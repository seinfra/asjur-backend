package br.com.seinfra.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.Cargo;
import br.com.seinfra.model.filter.CargoFilter;
import br.com.seinfra.service.CargoService;

@RestController
@RequestMapping("/asjur-api/cargo")
public class CargoController extends GenericBaseController<CargoService, Cargo, Long> {
	
	@GetMapping	
	public Page<Cargo> pequisar(CargoFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
