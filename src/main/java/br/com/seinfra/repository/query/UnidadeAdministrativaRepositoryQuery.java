package br.com.seinfra.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.UnidadeAdministrativa;
import br.com.seinfra.model.filter.UnidadeAdministrativaFilter;

public interface UnidadeAdministrativaRepositoryQuery {

	public Page<UnidadeAdministrativa> pequisar(UnidadeAdministrativaFilter filtro, Pageable pageable);
	
}
