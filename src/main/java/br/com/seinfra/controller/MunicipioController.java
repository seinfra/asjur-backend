package br.com.seinfra.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.Municipio;
import br.com.seinfra.model.filter.MunicipioFilter;
import br.com.seinfra.service.MunicipioService;

@RestController
@RequestMapping("/asjur-api/municipio")
public class MunicipioController extends GenericBaseController<MunicipioService, Municipio, Long> {
	
	@GetMapping	
	public Page<Municipio> pequisar(MunicipioFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
