package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Parecer;
import br.com.seinfra.repository.query.ParecerRepositoryQuery;

@Repository
public interface ParecerRepository extends JpaRepository<Parecer, Long>, ParecerRepositoryQuery {

}
