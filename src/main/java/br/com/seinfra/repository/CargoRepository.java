package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Cargo;
import br.com.seinfra.repository.query.CargoRepositoryQuery;

@Repository
public interface CargoRepository extends JpaRepository<Cargo, Long>, CargoRepositoryQuery {

	public Cargo findByNome(String nome);
}
