package br.com.seinfra.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "TB_TIP_DOC", schema = "bdscj")
public class TipoDocumento extends GenericBaseModel<Long> {

	@Size(max = 100)
    @Column(name = "tip_doc_nome", unique = true)
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
