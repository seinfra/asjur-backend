package br.com.seinfra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.seinfra.model.Credor;
import br.com.seinfra.repository.query.CredorRepositoryQuery;

@Repository
public interface CredorRepository extends JpaRepository<Credor, Long>, CredorRepositoryQuery {

	public Credor findByNome(String nome);
}
