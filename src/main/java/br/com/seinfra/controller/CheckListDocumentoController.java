package br.com.seinfra.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.seinfra.global.controller.GenericBaseController;
import br.com.seinfra.model.CheckListDocumentoPendente;
import br.com.seinfra.model.DocumentoJuridico;
import br.com.seinfra.service.CheckListDocumentoService;

@RestController
@RequestMapping("/asjur-api/checklist-documento")
public class CheckListDocumentoController extends GenericBaseController<CheckListDocumentoService, CheckListDocumentoPendente, Long> {
	
	@GetMapping("/checklist")
	public CheckListDocumentoPendente buscarPorDocumento(@RequestParam String id, @RequestParam String documento){
		List<CheckListDocumentoPendente> checkPendente = servico.checkListDocs(Long.valueOf(id), Long.valueOf(documento));
		return checkPendente.size() > 0 ? checkPendente.get(0) : new CheckListDocumentoPendente();
	}
	
	@GetMapping("/documento/{id}")
	public List<CheckListDocumentoPendente> findByDocumento(@PathVariable Long id){
		return servico.findByDocumento(new DocumentoJuridico(id));
	}
	
}