package br.com.seinfra.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "tb_doc", schema = "bdscj")
public class DocumentoJuridico extends GenericBaseModel<Long> {

	@Size(max = 1)
	@Column(name = "doc_cat")
	private String categoriaDocumento;

	@Size(max = 20)
	@Column(name = "doc_nr")
	private String numeroDocumento;

	@ManyToOne
	@JoinColumn(name = "tipo_doc_id")
	private TipoDocumento tipoDocumento;

	@ManyToOne
	@JoinColumn(name = "referencia_id")
	private Referencia referencia;

	@Size(max = 15)
	@Column(name = "doc_processo")
	private String numeroProcesso;

	@Size(max = 200)
	@Column(name = "doc_interessado")
	private String interessado;
	
	@ManyToOne
	@JoinColumn(name = "funcionario_id")
	private Funcionario funcionario;

	@ManyToOne
	@JoinColumn(name = "documento_pai_id")
	private DocumentoJuridico documento;

	@Column(name = "doc_data")
	private LocalDate dataDoc;
	
	@Column(name = "doc_data_ini_prazo")
	private LocalDate dataInicioPrazo;
	
	@Column(name = "doc_data_fim_prazo")
	private LocalDate dataFimPrazo;

	@Column(name = "doc_dataEnvioRec")
	private LocalDateTime dataEnvioRecebimento;
	
	@Column(name = "doc_data_rec_adg")
	private LocalDateTime dataRecebimentoAdvogado;

	@ManyToOne
	@JoinColumn(name = "assunto_id")
	private Assunto assunto;

	@Size(max = 2000)
	@Column(name = "doc_dsc_assunto")
	private String descricaoAssunto;

	@Size(max = 2000)
	@Column(name = "doc_detalhe")
	private String detalhe;
	
	@Size(max = 30)
	@Column(name = "doc_diligencia")
	private String diligencia;

	@ManyToOne
	@JoinColumn(name = "armario_id")
	private Armario armario;

	@ManyToOne
	@JoinColumn(name = "caixa_id")
	private Caixa caixa;

	@ManyToOne
	@JoinColumn(name = "pasta_id")
	private Pasta pasta;
	
    @Size(max = 30)
    @Column(name = "par_jur")
    private String parecerJuridico;
    
    @Size(max = 30)
    @Column(name = "par_jur_dlg")
    private String parecerJuridicoDiligencia;

	@Size(max = 2000)
	@Column(name = "doc_obs")
	private String observacao;
	
	@Size(max = 255)
	@Column(name = "doc_obs_dil")
	private String observacaoDiligencia;

	@Column(name = "doc_dataconclusao")
	private LocalDate dataConclusao;

	@Column(name = "doc_dtconclusaoreal")
	private LocalDate dataConclusaoReal;

	@Column(name = "doc_red")	
	private Boolean redistribuicao;
	
	@Column(name = "doc_prv")	
	private Boolean prevento;
	
	@Column(name = "doc_stts")	
	private String status;
	
	@Column(name = "doc_chk")	
	private String checkList;
	
	@Column(name = "prazo")
	private Long prazo;

	@Column(name = "doc_prioridade")
	private String prioridade;
	
	@NotNull
    @Column(name = "ugb")
	private String ugb;

    @ManyToOne
    @JoinColumn(name = "orgao_id")
    private Orgao orgao;
    
	@ManyToOne
	@JoinColumn(name = "uadm_id")
	private UnidadeAdministrativa unidadeAdministrativa;
	
	@JsonManagedReference("pareceres")
	@OneToMany(mappedBy = "documento", cascade = CascadeType.ALL)
	private List<Parecer> pareceres = new ArrayList<>();
	
	@JsonManagedReference("documentosPendente")
	@OneToMany(mappedBy = "documento", cascade = CascadeType.ALL)
	private List<CheckListDocumentoPendente> documentosPendente = new ArrayList<>();
	
	@JsonManagedReference("movimentosDocumento")
	@OneToMany(mappedBy = "documento", cascade = CascadeType.ALL)
	private List<MovimentoDocumento> movimentosDocumento = new ArrayList<>();
	
    @Transient
    private Long parecer;
    
    @Transient
    private Long subTipo;
	    
	@Transient
	private String dataDocumento;
	
	@Transient
	private Long atraso;
	
	@Transient
	private Integer mes;
	
	public DocumentoJuridico() {
		super();
	}
	
	public DocumentoJuridico(Long id) {
		this.id = id;
	}
	
	public DocumentoJuridico(Long id, String ugb) {
		super();
		this.id = id;
		this.ugb = ugb;
	}

	public DocumentoJuridico(Long id, Funcionario funcionario) {
		super();
		this.id = id;
		this.funcionario = funcionario;
	}
	
	public DocumentoJuridico(Long id, Integer mes) {
		super();
		this.id = id;
		this.mes = mes;
	}
	
	public DocumentoJuridico(Long id, Funcionario funcionario, String prioridade, Long prazo, Long atraso) {
		super();
		this.id = id;
		this.funcionario = funcionario;
		this.prioridade = prioridade;
		this.prazo = prazo;
		this.atraso = atraso;
	}
	
	public DocumentoJuridico(Long id, String prioridade, Integer mes) {
		super();
		this.id = id;
		this.prioridade = prioridade;
		this.mes = mes;
	}
	
	public DocumentoJuridico(Long id, Funcionario funcionario, Long prazo, Long atraso) {
		super();
		this.id = id;
		this.funcionario = funcionario;
		this.prazo = prazo;
		this.atraso = atraso;
	}
	
	public DocumentoJuridico(Long id, Integer mes, Long prazo, Long atraso) {
		super();
		this.id = id;
		this.mes = mes;
		this.prazo = prazo;
		this.atraso = atraso;
	}

	public String getCategoriaDocumento() {
		return categoriaDocumento;
	}

	public void setCategoriaDocumento(String categoriaDocumento) {
		this.categoriaDocumento = categoriaDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Referencia getReferencia() {
		return referencia;
	}

	public void setReferencia(Referencia referencia) {
		this.referencia = referencia;
	}

	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	public String getInteressado() {
		return interessado;
	}

	public void setInteressado(String interessado) {
		this.interessado = interessado;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		if(funcionario == null || funcionario.getId() == null) {
			this.funcionario = null;
		}else {
			this.funcionario = funcionario;
		}	
	}

	public DocumentoJuridico getDocumento() {
		return documento;
	}

	public void setDocumento(DocumentoJuridico documento) {
		this.documento = documento;
	}

	public String getDataDocumento() {
		return dataDocumento;
	}

	public void setDataDocumento(String dataDocumento) {
		this.dataDocumento = dataDocumento;
	}

	public LocalDate getDataDoc() {
		return dataDoc;
	}

	public void setDataDoc(LocalDate dataDoc) {
		this.dataDoc = dataDoc;
	}

	public LocalDate getDataInicioPrazo() {
		return dataInicioPrazo;
	}

	public void setDataInicioPrazo(LocalDate dataInicioPrazo) {
		this.dataInicioPrazo = dataInicioPrazo;
	}

	public LocalDate getDataFimPrazo() {
		return dataFimPrazo;
	}

	public void setDataFimPrazo(LocalDate dataFimPrazo) {
		this.dataFimPrazo = dataFimPrazo;
	}

	public LocalDateTime getDataEnvioRecebimento() {
		return dataEnvioRecebimento;
	}

	public void setDataEnvioRecebimento(LocalDateTime dataEnvioRecebimento) {
		this.dataEnvioRecebimento = dataEnvioRecebimento;
	}

	public LocalDateTime getDataRecebimentoAdvogado() {
		return dataRecebimentoAdvogado;
	}

	public void setDataRecebimentoAdvogado(LocalDateTime dataRecebimentoAdvogado) {
		this.dataRecebimentoAdvogado = dataRecebimentoAdvogado;
	}

	public Assunto getAssunto() {
		return assunto;
	}

	public void setAssunto(Assunto assunto) {
		this.assunto = assunto;
	}

	public String getDescricaoAssunto() {
		return descricaoAssunto;
	}

	public void setDescricaoAssunto(String descricaoAssunto) {
		this.descricaoAssunto = descricaoAssunto;
	}

	public String getDetalhe() {
		return detalhe;
	}

	public void setDetalhe(String detalhe) {
		this.detalhe = detalhe;
	}

	public String getDiligencia() {
		return diligencia;
	}

	public void setDiligencia(String diligencia) {
		this.diligencia = diligencia;
	}

	public void setUnidadeAdministrativa(UnidadeAdministrativa unidadeAdministrativa) {
		if(unidadeAdministrativa == null || unidadeAdministrativa.getId() == null) {
			this.unidadeAdministrativa = null;
		}else {
			this.unidadeAdministrativa = unidadeAdministrativa;
		}	
	}
	
	public UnidadeAdministrativa getUnidadeAdministrativa() {
		return unidadeAdministrativa;
	}

	public String getParecerJuridicoDiligencia() {
		return parecerJuridicoDiligencia;
	}

	public void setParecerJuridicoDiligencia(String parecerJuridicoDiligencia) {
		this.parecerJuridicoDiligencia = parecerJuridicoDiligencia;
	}


	public Armario getArmario() {
		return armario;
	}

	public void setArmario(Armario armario) {
		if(armario == null || armario.getId() == null) {
			this.armario = null;
		}else {
			this.armario = armario;
		}
	}

	public Caixa getCaixa() {
		return caixa;
	}

	public void setCaixa(Caixa caixa) {
		if(caixa == null || caixa.getId() == null) {
			this.caixa = null;
		}else {
			this.caixa = caixa;
		}
	}

	public Pasta getPasta() {
		return pasta;
	}

	public void setPasta(Pasta pasta) {
		if(pasta == null || pasta.getId() == null) {
			this.pasta = null;
		}else {
			this.pasta = pasta;
		}
	}

	public String getParecerJuridico() {
		return parecerJuridico;
	}

	public void setParecerJuridico(String parecerJuridico) {
		this.parecerJuridico = parecerJuridico;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getObservacaoDiligencia() {
		return observacaoDiligencia;
	}

	public void setObservacaoDiligencia(String observacaoDiligencia) {
		this.observacaoDiligencia = observacaoDiligencia;
	}

	public LocalDate getDataConclusao() {
		return dataConclusao;
	}

	public void setDataConclusao(LocalDate dataConclusao) {
		this.dataConclusao = dataConclusao;
	}

	public LocalDate getDataConclusaoReal() {
		return dataConclusaoReal;
	}

	public void setDataConclusaoReal(LocalDate dataConclusaoReal) {
		this.dataConclusaoReal = dataConclusaoReal;
	}

	public Boolean getRedistribuicao() {
		return redistribuicao;
	}

	public void setRedistribuicao(Boolean redistribuicao) {
		this.redistribuicao = redistribuicao;
	}

	public Boolean getPrevento() {
		return prevento;
	}

	public void setPrevento(Boolean prevento) {
		this.prevento = prevento;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCheckList() {
		return checkList;
	}

	public void setCheckList(String checkList) {
		this.checkList = checkList;
	}

	public Long getPrazo() {
		return prazo;
	}

	public void setPrazo(Long prazo) {
		this.prazo = prazo;
	}

	public String getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(String prioridade) {
		this.prioridade = prioridade;
	}

	public String getUgb() {
		return ugb;
	}

	public void setUgb(String ugb) {
		this.ugb = ugb;
	}

	public Orgao getOrgao() {
		return orgao;
	}

	public void setOrgao(Orgao orgao) {
		if(orgao == null || orgao.getId() == null) {
			this.orgao = null;
		}else {
			this.orgao = orgao;
		}	
	}

	public Long getParecer() {
		return parecer;
	}

	public void setParecer(Long parecer) {
		this.parecer = parecer;
	}

	public Long getSubTipo() {
		return subTipo;
	}

	public void setSubTipo(Long subTipo) {
		this.subTipo = subTipo;
	}

	public List<Parecer> getPareceres() {
		return pareceres;
	}

	public void setPareceres(List<Parecer> pareceres) {
		this.pareceres = pareceres;
	}

	public List<CheckListDocumentoPendente> getDocumentosPendente() {
		return documentosPendente;
	}

	public void setDocumentosPendente(List<CheckListDocumentoPendente> documentosPendente) {
		this.documentosPendente = documentosPendente;
	}

	public List<MovimentoDocumento> getMovimentosDocumento() {
		return movimentosDocumento;
	}

	public void setMovimentosDocumento(List<MovimentoDocumento> movimentosDocumento) {
		this.movimentosDocumento = movimentosDocumento;
	}

	public Long getAtraso() {
		return atraso;
	}

	public void setAtraso(Long atraso) {
		this.atraso = atraso;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

}
	