package br.com.seinfra.global.service;

import br.com.seinfra.global.model.IGenericBaseModel;

public interface IGenericBaseService<E extends IGenericBaseModel, T> {

	E incluir(E entidade);
	E alterar(Long id, E entidade);
	void excluir(T id);
	E buscarPorCodigo(T id);
	Object listar();
		
}
