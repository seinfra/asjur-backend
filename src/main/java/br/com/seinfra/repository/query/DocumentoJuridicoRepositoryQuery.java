package br.com.seinfra.repository.query;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.seinfra.model.DocumentoJuridico;
import br.com.seinfra.model.Funcionario;
import br.com.seinfra.model.Parecer;
import br.com.seinfra.model.dto.DocumentoForm;
import br.com.seinfra.model.filter.DocumentoJuridicoFilter;

public interface DocumentoJuridicoRepositoryQuery {

	public Page<DocumentoJuridico> pequisar(DocumentoJuridicoFilter filtro, Pageable pageable);
	
	public List<DocumentoJuridico> documentoPorUsuario(Funcionario funcionario);
	
	public List<DocumentoJuridico> documentoPorFuncionario(DocumentoJuridico documento, List<Funcionario> funcionarios, List<Funcionario> funcionariosAfastados);
	
	public List<DocumentoJuridico> documentoPorFuncionarioReceber(String cpf);
	
	public List<Parecer> relatorioControPrazo(LocalDate inicio, LocalDate fim, String ugb);
	
	public List<Parecer> relatorioControEntrega(LocalDate inicio, LocalDate fim, String id);
	
	public List<DocumentoJuridico> relatorioControJuridicoPorComplexidade(DocumentoForm documentoForm);
	
	public List<DocumentoJuridico> relatorioControJuridicoPorAdvogado(DocumentoForm documentoForm);
	
	public List<DocumentoJuridico> relatorioControJuridicoPorComplexidadeResumo(DocumentoForm documentoForm);
	
	public List<DocumentoJuridico> relatorioControJuridicoPorAdvogadoResumo(DocumentoForm documentoForm);
	
	public List<DocumentoJuridico> documentoParaAnaliseCoordenador();
	
	public List<DocumentoJuridico> documentoFiltro(DocumentoJuridico documento);
	
	public List<DocumentoJuridico> documentoParaCheckList(String cpf);
	
	public List<DocumentoJuridico> documentoFinalizar();
	
	public List<DocumentoJuridico> estatisticaAdvogado(String prazo);
	
	public List<DocumentoJuridico> estatisticaUgb(String prazo);
	
	public List<DocumentoJuridico> inicioDistribuicao();
	
	public List<DocumentoJuridico> relatorioProcesso(Long id);
	
	public List<DocumentoJuridico> envioEmailProcesso();
	
	public List<DocumentoJuridico> proximoDocumento(DocumentoJuridico documento, Funcionario funcionario);
	
}
