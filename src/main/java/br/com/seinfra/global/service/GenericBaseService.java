package br.com.seinfra.global.service;

import java.io.Serializable;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.seinfra.global.model.IGenericBaseModel;

/*
* @param <R> Classe do repositorio da classe a ser manipulada.
* @param <E> Classe da entidade a ser manipulada.
* @param <T> Classe do tipo de dado do id da classe a ser manipulada.
*/

public abstract class GenericBaseService<R extends JpaRepository<E, T>, E extends IGenericBaseModel, T extends Serializable>
		implements IGenericBaseService<E, T> {

	@Autowired
	public R repositorio;

	// Novo
	public E incluir(E entidade) {
		return repositorio.save(entidade);
	}

	// Editar
	public E alterar(T codigo, E entidade) {
		E entidadeSalva = buscarPorCodigo(codigo);
		BeanUtils.copyProperties(entidade, entidadeSalva, "codigo");
		return repositorio.save(entidadeSalva);
	}

	// remover
	public void excluir(T codigo) {
		repositorio.deleteById(codigo);
	}

	// Buscar por código
	public E buscarPorCodigo(T codigo) {
		Optional<E> entidade = repositorio.findById(codigo);
		if (entidade.isPresent()) {
			return entidade.get();
		}
		return null;
	}

	// Listar
	public Object listar() {
		return repositorio.findAll();
	}
}
