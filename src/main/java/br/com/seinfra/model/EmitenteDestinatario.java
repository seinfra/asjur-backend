package br.com.seinfra.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.seinfra.global.model.GenericBaseModel;

@Entity
@Table(name = "TB_EMDT", schema = "bdscj")
public class EmitenteDestinatario extends GenericBaseModel<Long> {

	@JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "end_id")
    private Endereco endereco;

	@JsonIgnore
    @ManyToOne
    @JoinColumn(name = "uadm_id")
    private UnidadeAdministrativa unidadeAdministrativa;

    @Size(max = 200)
    @Column(name = "EMDT_DSC", unique = true)
    private String descricao;

    @Size(max = 200)
    @Column(name = "EMDT_SIGLA", unique = true)
    private String sigla;

    @Size(max = 1)
    @Column(name = "EMDT_TIP_PESSOA")
    private String tipoPessoa;

    @Size(max = 20)
    @Column(name = "EMDT_RG")
    private String rg;

    @Size(max = 20)
    @Column(name = "EMDT_CNPJ_CPF", unique = true)
    private String cnpjCpf;

    @Size(max = 20)
    @Column(name = "EMDT_CGF")
    private String cgf;

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public UnidadeAdministrativa getUnidadeAdministrativa() {
        return unidadeAdministrativa;
    }

    public void setUnidadeAdministrativa(UnidadeAdministrativa unidadeAdministrativa) {
		if(unidadeAdministrativa == null || unidadeAdministrativa.getNome() == null) {
			this.unidadeAdministrativa = null;
		}else {
			this.unidadeAdministrativa = unidadeAdministrativa;
		}
	}

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCnpjCpf() {
        return cnpjCpf;
    }

    public void setCnpjCpf(String cnpjCpf) {
        this.cnpjCpf = cnpjCpf;
    }

    public String getCgf() {
        return cgf;
    }

    public void setCgf(String cgf) {
        this.cgf = cgf;
    }

}