package br.com.seinfra.model.enums;

public enum TIPO_STATUS {

	  EM_PRIMEIRA_ANALISE,
      EM_SEGUNDA_ANALISE,
      RETORNO_A_ORIGEM_POR_PENDENECIAS,
      RESOLVIDO;

      public String getNome() {
          return toString().replaceAll("_", " ");
      }
}
