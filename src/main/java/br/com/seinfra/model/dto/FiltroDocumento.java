package br.com.seinfra.model.dto;

import java.util.Date;

import br.com.seinfra.model.Assunto;
import br.com.seinfra.model.Caixa;
import br.com.seinfra.model.DocumentoJuridico;
import br.com.seinfra.model.MovimentoDocumento;
import br.com.seinfra.model.Pasta;
import br.com.seinfra.model.Referencia;
import br.com.seinfra.model.TipoDocumento;
import br.com.seinfra.model.UnidadeAdministrativa;

public class FiltroDocumento {

    public static final String EXPEDIDO = "E";
    public static final String RECEBIDO = "R";

    private Long id;
    
    private String categoriaDocumento;

    private String numeroDocumento;

    private String numeroProcesso;

    private Date dataInicialRecebimento;

    private Date dataFinalRecebimento;

    private Date dataInicialDocumento;

    private Date dataFinalDocumento;

    private Date dataInicialTramite;

    private Date dataFinalTramite;

    private String interessado;

    private String descricaoAssunto;

    private String observacao;

    private TipoDocumento tipoDocumento;
    
    private String tipoMovimento;

    private Referencia referencia;

    private Assunto assunto;

    private UnidadeAdministrativa unidadeAdministrativa;

    private DocumentoJuridico documento;

    private MovimentoDocumento movimentoDocumento;

    private String demanda;

    private Caixa caixa;

    private Pasta pasta;

    private String demandaAtendida;

    private Boolean arquivar;

    private transient Boolean indicador = false;

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNumeroProcesso() {
        return numeroProcesso;
    }

    public void setNumeroProcesso(String numeroProcesso) {
        this.numeroProcesso = numeroProcesso;
    }

    public Date getDataInicialRecebimento() {
        return dataInicialRecebimento;
    }

    public void setDataInicialRecebimento(Date dataInicialRecebimento) {
        this.dataInicialRecebimento = dataInicialRecebimento;
    }

    public Date getDataFinalRecebimento() {
        return dataFinalRecebimento;
    }

    public void setDataFinalRecebimento(Date dataFinalRecebimento) {
        this.dataFinalRecebimento = dataFinalRecebimento;
    }

    public Date getDataInicialDocumento() {
        return dataInicialDocumento;
    }

    public void setDataInicialDocumento(Date dataInicialDocumento) {
        this.dataInicialDocumento = dataInicialDocumento;
    }

    public Date getDataFinalDocumento() {
        return dataFinalDocumento;
    }

    public void setDataFinalDocumento(Date dataFinalDocumento) {
        this.dataFinalDocumento = dataFinalDocumento;
    }

    public String getInteressado() {
        return interessado;
    }

    public void setInteressado(String interessado) {
        this.interessado = interessado;
    }

    public String getDescricaoAssunto() {
        return descricaoAssunto;
    }

    public void setDescricaoAssunto(String descricaoAssunto) {
        this.descricaoAssunto = descricaoAssunto;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

	public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Referencia getReferencia() {
		return referencia;
	}

	public void setReferencia(Referencia referencia) {
		this.referencia = referencia;
	}

	public Assunto getAssunto() {
        return assunto;
    }

    public void setAssunto(Assunto assunto) {
        this.assunto = assunto;
    }

    public UnidadeAdministrativa getUnidadeAdministrativa() {
        return unidadeAdministrativa;
    }

    public void setUnidadeAdministrativa(UnidadeAdministrativa unidadeAdministrativa) {
        this.unidadeAdministrativa = unidadeAdministrativa;
    }

    public String getCategoriaDocumento() {
        return categoriaDocumento;
    }

    public void setCategoriaDocumento(String categoriaDocumento) {
        this.categoriaDocumento = categoriaDocumento;
    }

    public DocumentoJuridico getDocumento() {
        return documento;
    }

    public void setDocumento(DocumentoJuridico documento) {
        this.documento = documento;
    }

    public Date getDataInicialTramite() {
        return dataInicialTramite;
    }

    public void setDataInicialTramite(Date dataInicialTramite) {
        this.dataInicialTramite = dataInicialTramite;
    }

    public Date getDataFinalTramite() {
        return dataFinalTramite;
    }

    public void setDataFinalTramite(Date dataFinalTramite) {
        this.dataFinalTramite = dataFinalTramite;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MovimentoDocumento getMovimentoDocumento() {
        return movimentoDocumento;
    }

    public void setMovimentoDocumento(MovimentoDocumento movimentoDocumento) {
        this.movimentoDocumento = movimentoDocumento;
    }

    public String getDemanda() {
        return demanda;
    }

    public void setDemanda(String demanda) {
        this.demanda = demanda;
    }

    public String getDemandaAtendida() {
        return demandaAtendida;
    }

    public void setDemandaAtendida(String demandaAtendida) {
        this.demandaAtendida = demandaAtendida;
    }

    public Caixa getCaixa() {
        return caixa;
    }

    public void setCaixa(Caixa caixa) {
        this.caixa = caixa;
    }

    public Pasta getPasta() {
        return pasta;
    }

    public void setPasta(Pasta pasta) {
        this.pasta = pasta;
    }

    public void setTipoMovimento(String tipoMovimento) {
        this.tipoMovimento = tipoMovimento;
    }

    public String getTipoMovimento() {
        return tipoMovimento;
    }

    public boolean isExpedido() {
        return EXPEDIDO.equals(categoriaDocumento);
    }

    public boolean isRecebido() {
        return RECEBIDO.equals(categoriaDocumento);
    }

    public Boolean getArquivar() {
        return arquivar;
    }

    public void setArquivar(Boolean arquivar) {
        this.arquivar = arquivar;
    }

	public Boolean getIndicador() {
		return indicador;
	}

	public void setIndicador(Boolean indicador) {
		this.indicador = indicador;
	}

}
